package com.example.xyrestoadminapp.admin.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.xyrestoadminapp.admin.repository.AdminRepository

class AdminViewModelFactory(private val repository: AdminRepository): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AdminViewModel(repository) as T
    }
}