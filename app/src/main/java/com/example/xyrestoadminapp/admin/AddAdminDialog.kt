package com.example.xyrestoadminapp.admin

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.xyrestoadminapp.R
import com.example.xyrestoadminapp.admin.adapter.UserListAdapter
import com.example.xyrestoadminapp.base.constant.Constant.USER_ROLE
import com.example.xyrestoadminapp.base.fragment.BaseDialogFragment
import com.example.xyrestoadminapp.base.fragment.ConfirmationDialogFragment.Companion.KEY_DATA
import com.example.xyrestoadminapp.base.fragment.DialogResponse
import com.example.xyrestoadminapp.base.fragment.IResponseCommunicator
import com.example.xyrestoadminapp.base.model.User
import com.example.xyrestoadminapp.base.utils.ResponseWrapper
import com.example.xyrestoadminapp.base.utils.Utils.getScreenDimension
import com.example.xyrestoadminapp.base.widget.decoration.SpacingItemDecoration
import com.example.xyrestoadminapp.databinding.FragmentDialogAddAdminBinding

class AddAdminDialog: BaseDialogFragment() {

    private var _viewBinding: FragmentDialogAddAdminBinding? = null
    private val viewBinding: FragmentDialogAddAdminBinding get() = _viewBinding!!
    private lateinit var iResponseCommunicator: IResponseCommunicator
    private val rvAdapter: UserListAdapter by lazy { UserListAdapter(::onItemClicked) }
    private var userId: Int? = null
    private val iAdminListCommunicator: IAdminListCommunicator by lazy { parentFragment as IAdminListCommunicator }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        dialog?.window?.setBackgroundDrawable(ContextCompat.getDrawable(requireContext(), android.R.color.transparent))
        _viewBinding = FragmentDialogAddAdminBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun initView() {
        with(viewBinding){
            val dimens = getScreenDimension(requireContext(), true)
            val density = resources.displayMetrics.density
            if (dimens.first >= 600 && dimens.second >= 600){
                vBackground.layoutParams = ConstraintLayout.LayoutParams((590*density).toInt(), (400*density).toInt()).apply {
                    topToTop = R.id.cl_container
                    bottomToBottom = R.id.cl_container
                    startToStart = R.id.cl_container
                    endToEnd = R.id.cl_container
                }
            } else {
                vBackground.layoutParams = ConstraintLayout.LayoutParams((320*density).toInt(), (300*density).toInt()).apply {
                    topToTop = R.id.cl_container
                    bottomToBottom = R.id.cl_container
                    startToStart = R.id.cl_container
                    endToEnd = R.id.cl_container
                }
            }
            rvEmailList.run {
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                adapter = rvAdapter.apply { addItemDecoration(SpacingItemDecoration(leftMargin = 5, topMargin = 5)) }
            }
            etEmail.addTextChangedListener(object : TextWatcher{
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                    //
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    //
                }

                override fun afterTextChanged(s: Editable?) {
                    viewBinding.tvStatus.visibility = View.GONE
                    if (s.toString() == ""){
                        viewBinding.cvContainer.visibility = View.GONE
                        viewBinding.progressBar.visibility = View.GONE
                        rvAdapter.clearData()
                    } else{
                        viewBinding.cvContainer.visibility = View.VISIBLE
                        viewBinding.progressBar.visibility = View.VISIBLE
                        iAdminListCommunicator.getUsers(USER_ROLE, s.toString())
                    }
                }

            })
            btnSave.setOnClickListener {
                if (this@AddAdminDialog::iResponseCommunicator.isInitialized){
                    userId?.let { userId ->
                        iResponseCommunicator.onResponse(
                            DialogResponse.YES,
                            Bundle().apply { putInt(KEY_DATA, userId) }
                        )
                    } ?: run {
                        Toast.makeText(context, "PLEASE SELECT EMAIL FROM AVAILABLE EMAIL", Toast.LENGTH_LONG).show()
                    }

                }
            }
            root.setOnClickListener { dismiss() }
            btnCancel.setOnClickListener { dismissWindow() }
        }
        initObserver()
    }

    override fun getDimension(): Pair<Int, Int> {
        return Pair(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.MATCH_PARENT)
    }

    private fun initObserver(){
        iAdminListCommunicator.addObserverOnUserLiveData(viewLifecycleOwner, {
            when(it.status){
                ResponseWrapper.Status.LOADING -> {

                }
                ResponseWrapper.Status.SUCCESS -> {
                    it.body?.data?.let { users ->
                        rvAdapter.setItemList(users)
                        if (users.isEmpty()){
                            viewBinding.tvStatus.visibility = View.VISIBLE
                            viewBinding.tvStatus.text = resources.getString(R.string.empty)
                        }
                    }
                }
                ResponseWrapper.Status.ERROR -> {
                    if (it.body?.errors?.toString()?.contains("cancelled") == false){
                        viewBinding.tvStatus.visibility = View.VISIBLE
                        viewBinding.tvStatus.text = resources.getString(R.string.error)
                        Toast.makeText(context, "ERROR: "+it.body.errors, Toast.LENGTH_LONG).show()
                    }
                }
            }
            viewBinding.progressBar.visibility = View.GONE
        })
    }

    private fun dismissWindow(){
        if (this@AddAdminDialog::iResponseCommunicator.isInitialized){
            iResponseCommunicator.onResponse(
                DialogResponse.NO,
                null
            )
        }
    }

    private fun onItemClicked(user: User){
        userId = user.id
        viewBinding.etEmail.setText(user.email)
        viewBinding.cvContainer.visibility = View.GONE
    }

    fun setResponseCommunicator(iCommunicator: IResponseCommunicator){
        iResponseCommunicator = iCommunicator
    }
}