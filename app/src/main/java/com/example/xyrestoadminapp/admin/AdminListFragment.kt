package com.example.xyrestoadminapp.admin

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.xyrestoadminapp.R
import com.example.xyrestoadminapp.admin.adapter.AdminListAdapter
import com.example.xyrestoadminapp.admin.model.ChangeRoleRequest
import com.example.xyrestoadminapp.admin.network.IAdminApiService
import com.example.xyrestoadminapp.admin.repository.AdminRepository
import com.example.xyrestoadminapp.admin.viewmodel.AdminViewModel
import com.example.xyrestoadminapp.admin.viewmodel.AdminViewModelFactory
import com.example.xyrestoadminapp.base.constant.Constant.ADMIN_ROLE
import com.example.xyrestoadminapp.base.constant.Constant.USER_ROLE
import com.example.xyrestoadminapp.base.fragment.ConfirmationDialogFragment
import com.example.xyrestoadminapp.base.fragment.ConfirmationDialogFragment.Companion.KEY_DATA
import com.example.xyrestoadminapp.base.fragment.DialogResponse
import com.example.xyrestoadminapp.base.fragment.IResponseCommunicator
import com.example.xyrestoadminapp.base.model.BaseApiResponse
import com.example.xyrestoadminapp.base.model.User
import com.example.xyrestoadminapp.base.storage.network.ApiClient
import com.example.xyrestoadminapp.base.utils.ResponseWrapper
import com.example.xyrestoadminapp.base.utils.Utils
import com.example.xyrestoadminapp.base.utils.Utils.orDefaultBool
import com.example.xyrestoadminapp.base.widget.decoration.SpacingItemDecoration
import com.example.xyrestoadminapp.databinding.FragmentAdminListBinding
import com.example.xyrestoadminapp.home.IRetryCallBack
import com.example.xyrestoadminapp.home.LoadingCommunicator
import com.example.xyrestoadminapp.home.LoadingMode

class AdminListFragment : Fragment(), IAdminListCommunicator {

    private var _viewBinding: FragmentAdminListBinding? = null
    private val viewBinding: FragmentAdminListBinding get() = _viewBinding!!
    private val adminListAdapter: AdminListAdapter by lazy { AdminListAdapter(::onDeleteAdminClicked) }
    private val viewModel: AdminViewModel by viewModels { AdminViewModelFactory(
        AdminRepository(ApiClient.getRetrofitInstance(requireContext()).create(IAdminApiService::class.java))
    ) }
    private lateinit var loadingCommunicator: LoadingCommunicator
    private val retryCallbackList = mutableListOf<Pair<Int, IRetryCallBack>>()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        loadingCommunicator = activity as LoadingCommunicator
        loadingCommunicator.startLoading(1, LoadingMode.PROGRESS_BAR_WITH_BACKGROUND)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _viewBinding = FragmentAdminListBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(viewBinding){
            if (Utils.getScreenDimension(requireContext(), true).second >= 600){
                btnAddAdmin.visibility = View.VISIBLE
                fbAddAdmin.visibility = View.GONE
                btnAddAdmin.setOnClickListener {
                    setAddAdminClick()
                }
            } else {
                fbAddAdmin.visibility = View.VISIBLE
                btnAddAdmin.visibility = View.GONE
                fbAddAdmin.setOnClickListener {
                    setAddAdminClick()
                }
            }
        }
        initRecyclerView()
        initObserver()
    }

    private fun initRecyclerView(){
        viewBinding.rvAdminList.run {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = adminListAdapter.apply {
                addItemDecoration(SpacingItemDecoration(20,10,20,10))
            }
        }
    }

    private fun setAddAdminClick(){
        val dialogFragment = AddAdminDialog().apply { setResponseCommunicator(object: IResponseCommunicator {
            override fun onResponse(dialogResponse: DialogResponse, data: Bundle?) {
                if (dialogResponse == DialogResponse.YES){
                    data?.getInt(KEY_DATA)?.let {
                        viewModel.editUserRole(0, it, ChangeRoleRequest(listOf(USER_ROLE, ADMIN_ROLE)))
                    }
                }
                dismiss()
            }
        }) }
        childFragmentManager.beginTransaction().add(dialogFragment, null).commit()
    }

    private fun initObserver() {
        viewModel.users.observe(viewLifecycleOwner, {
            retryCallbackList.removeAll { callback -> callback.first == 0 }
            when(it.status){
                ResponseWrapper.Status.LOADING -> {

                }
                ResponseWrapper.Status.SUCCESS -> {
                    it.body?.data?.let { users -> adminListAdapter.setItemList(users)}
                    loadingCommunicator.stopLoading()
                }
                ResponseWrapper.Status.ERROR -> {
                    retryCallbackList.add(Pair(0, object : IRetryCallBack{
                        override fun retry() {
                            viewModel.getUsers(ADMIN_ROLE)
                        }
                    }))
                    loadingCommunicator.onError(it.body?.errors.toString(), retryCallbackList)
                }
            }
        })
        viewModel.editUser.observe(viewLifecycleOwner, { event ->
            event.getContentIfNotHandled()?.let {
                when(it.second.status){
                    ResponseWrapper.Status.LOADING -> {

                    }
                    ResponseWrapper.Status.SUCCESS -> {
                        it.second.body?.data?.let { user ->
                            if (user.roles?.contains(ADMIN_ROLE).orDefaultBool(false)){
                                Toast.makeText(context, "ADMIN ADDED SUCCESSFULLY", Toast.LENGTH_SHORT).show()
                                adminListAdapter.addItemAt(0, user)
                            } else {
                                Toast.makeText(context, "ADMIN DELETED SUCCESSFULLY", Toast.LENGTH_SHORT).show()
                                adminListAdapter.removeAt(it.first)
                            }
                            viewModel.updateUserList(adminListAdapter.getUserList())
                        }
                    }
                    ResponseWrapper.Status.ERROR -> {
                        Toast.makeText(context, "ERROR: "+it.second.body?.errors.toString(), Toast.LENGTH_LONG).show()
                    }
                }
            }
        })
    }

    private fun onDeleteAdminClicked(userId: Int, position: Int){
        val dialogFragment = ConfirmationDialogFragment.newInstance(resources.getString(R.string.delete_confirmation)).apply { setResponseCommunicator(object: IResponseCommunicator{
            override fun onResponse(dialogResponse:DialogResponse, data: Bundle?) {
                if (dialogResponse == DialogResponse.YES)
                    viewModel.editUserRole(position, userId, ChangeRoleRequest(listOf(USER_ROLE)))
                dismiss()
            }
        }) }
        childFragmentManager.beginTransaction().add(dialogFragment, null).commit()
    }

    override fun getUsers(role: String, email: String) {
        viewModel.getUsers(role, email)
    }

    override fun addObserverOnUserLiveData(lifecycleOwner: LifecycleOwner, callback: Observer<ResponseWrapper<BaseApiResponse<List<User>>>>) {
        viewModel.searchUser.observe(lifecycleOwner, callback)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _viewBinding = null
    }
}