package com.example.xyrestoadminapp.admin.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ChangeRoleRequest(
    val roles: List<String>
): Parcelable