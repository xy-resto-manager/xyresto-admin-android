package com.example.xyrestoadminapp.admin.network

import com.example.xyrestoadminapp.admin.model.ChangeRoleRequest
import com.example.xyrestoadminapp.base.model.BaseApiResponse
import com.example.xyrestoadminapp.base.model.User
import retrofit2.Response
import retrofit2.http.*

interface IAdminApiService {
    @Headers("Content-Type:application/json")
    @GET("admin/user/users")
    suspend fun getAllUsers(@Query("role") role: String? = null): Response<BaseApiResponse<List<User>>>

    @POST("admin/user/users/{userId}/roles")
    suspend fun editUserRole(@Path("userId") userId: Int, @Body changeRoleRequest: ChangeRoleRequest): Response<BaseApiResponse<User>>
}