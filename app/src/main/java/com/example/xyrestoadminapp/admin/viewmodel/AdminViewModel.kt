package com.example.xyrestoadminapp.admin.viewmodel

import androidx.lifecycle.*
import com.example.xyrestoadminapp.admin.model.ChangeRoleRequest
import com.example.xyrestoadminapp.admin.repository.AdminRepository
import com.example.xyrestoadminapp.base.constant.Constant.ADMIN_ROLE
import com.example.xyrestoadminapp.base.custome_class.CustomeLiveData
import com.example.xyrestoadminapp.base.model.BaseApiResponse
import com.example.xyrestoadminapp.base.model.Event
import com.example.xyrestoadminapp.base.model.User
import com.example.xyrestoadminapp.base.utils.ApiCallHelper.safeApiCall
import com.example.xyrestoadminapp.base.utils.ResponseWrapper
import com.example.xyrestoadminapp.base.utils.Utils.orDefaultBool
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class AdminViewModel(private val repository: AdminRepository): ViewModel() {
    private val _users = CustomeLiveData<ResponseWrapper<BaseApiResponse<List<User>>>>()
    val users: LiveData<ResponseWrapper<BaseApiResponse<List<User>>>> get() = _users
    private val _searchUser = MutableLiveData<ResponseWrapper<BaseApiResponse<List<User>>>>()
    val searchUser: LiveData<ResponseWrapper<BaseApiResponse<List<User>>>> get() = Transformations.switchMap(_searchUser, ::filterUserByEmail)
    private val _editUser = MutableLiveData<Event<Pair<Int, ResponseWrapper<BaseApiResponse<User>>>>>()
    val editUser: LiveData<Event<Pair<Int, ResponseWrapper<BaseApiResponse<User>>>>> get() = _editUser
    private var emailQuery: String? = null
    private var debouncePeriod: Long = 800
    private var searchJob: Job? = null

    init {
        getUsers(ADMIN_ROLE)
    }

    fun getUsers(role: String, email: String? = null){
        email?.let { emailQuery = it }
        searchJob?.cancel()
        searchJob = viewModelScope.launch(Dispatchers.IO) {
            if (role == ADMIN_ROLE){
                _users.postValue(ResponseWrapper.loading())
                _users.postValue(safeApiCall { repository.getAllUsers(role) })
            } else {
                delay(debouncePeriod)
                _searchUser.postValue(ResponseWrapper.loading())
                _searchUser.postValue(safeApiCall { repository.getAllUsers(role) })
            }
        }
    }

    fun editUserRole(position: Int, userId: Int, changeRoleRequest: ChangeRoleRequest){
        viewModelScope.launch(Dispatchers.IO) {
            _editUser.postValue(Event(Pair(-1, ResponseWrapper.loading())))
            _editUser.postValue(Event(Pair(position, safeApiCall { repository.editUserRole(userId, changeRoleRequest) })))
        }
    }

    fun updateUserList(userList: List<User>){
        _users.value?.body?.let {
            _users.setValueWithoutNotify(
                ResponseWrapper(
                    ResponseWrapper.Status.SUCCESS,
                    BaseApiResponse(
                        it.code,
                        userList,
                        it.status,
                        it.errors,
                        null
                    ),
                    _users.value?.message
                )
            )
        }
    }

    private fun filterUserByEmail(response: ResponseWrapper<BaseApiResponse<List<User>>>): LiveData<ResponseWrapper<BaseApiResponse<List<User>>>>{
        val liveData = MutableLiveData<ResponseWrapper<BaseApiResponse<List<User>>>>()
        when(response.status){
            ResponseWrapper.Status.SUCCESS -> {
                response.body?.data?.let { users ->
                    val userList = users.filter {
                        it.email?.contains(emailQuery.orEmpty()).orDefaultBool(false) && !it.roles?.contains(ADMIN_ROLE).orDefaultBool(false)
                    }
                    liveData.value = ResponseWrapper.success(
                        BaseApiResponse(
                            code = response.body.code,
                            status = response.body.status,
                            data = userList
                        )
                    )
                }
            }
            else -> {
                liveData.value = response
            }
        }
        return liveData
    }

    override fun onCleared() {
        super.onCleared()
        searchJob?.cancel()
    }
}