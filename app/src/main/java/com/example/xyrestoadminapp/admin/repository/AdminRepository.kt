package com.example.xyrestoadminapp.admin.repository

import com.example.xyrestoadminapp.admin.model.ChangeRoleRequest
import com.example.xyrestoadminapp.admin.network.IAdminApiService

class AdminRepository(private val iAdminApiService: IAdminApiService) {
    suspend fun getAllUsers(role: String) = iAdminApiService.getAllUsers(role)
    suspend fun editUserRole(userId: Int, changeRoleRequest: ChangeRoleRequest) = iAdminApiService.editUserRole(userId, changeRoleRequest)
}