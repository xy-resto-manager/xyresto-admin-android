package com.example.xyrestoadminapp.admin.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.xyrestoadminapp.base.adapter.BaseRvAdapter
import com.example.xyrestoadminapp.base.model.User
import com.example.xyrestoadminapp.databinding.DropdownListItemBinding

class UserListAdapter(private val onItemClicked: (user: User) -> Unit): BaseRvAdapter<User>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseRvViewHolder<User> {
        val viewBinding = DropdownListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return UserListViewHolder(viewBinding)
    }

    override fun onBindViewHolder(holder: BaseRvViewHolder<User>, position: Int) {
        mutableItems[position].let(holder::bind)
    }

    inner class UserListViewHolder(private val viewBinding: DropdownListItemBinding): BaseRvViewHolder<User>(viewBinding.root){
        override fun bind(data: User, position: Int?) {
            viewBinding.tvItem.text = data.email.toString()
            viewBinding.root.setOnClickListener { onItemClicked(data) }
        }
    }
}