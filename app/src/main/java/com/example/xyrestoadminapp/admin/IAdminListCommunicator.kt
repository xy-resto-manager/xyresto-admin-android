package com.example.xyrestoadminapp.admin

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.example.xyrestoadminapp.base.model.BaseApiResponse
import com.example.xyrestoadminapp.base.model.User
import com.example.xyrestoadminapp.base.utils.ResponseWrapper

interface IAdminListCommunicator {
    fun getUsers(role: String, email: String)
    fun addObserverOnUserLiveData(lifecycleOwner: LifecycleOwner, callback: Observer<ResponseWrapper<BaseApiResponse<List<User>>>>)
}