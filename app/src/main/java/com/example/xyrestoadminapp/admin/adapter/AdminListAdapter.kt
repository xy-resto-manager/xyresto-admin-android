package com.example.xyrestoadminapp.admin.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.xyrestoadminapp.base.adapter.BaseRvAdapter
import com.example.xyrestoadminapp.base.model.User
import com.example.xyrestoadminapp.databinding.AdminListItemBinding

class AdminListAdapter(private val onDeleteAdminClicked: (id: Int, position: Int) -> Unit): BaseRvAdapter<User>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseRvViewHolder<User> {
        val viewBinding = AdminListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AdminListViewHolder(viewBinding)
    }

    override fun onBindViewHolder(holder: BaseRvViewHolder<User>, position: Int) {
        holder.bind(mutableItems[position % mutableItemCount], position)
    }

    fun getUserList(): List<User> = mutableItems

    inner class AdminListViewHolder(private val viewBinding: AdminListItemBinding): BaseRvViewHolder<User>(viewBinding.root){
        override fun bind(data: User, position: Int?) {
            with(viewBinding){
                tvUsername.text = data.fullName
                tvEmail.text = data.email
                ivDeleteAdmin.setOnClickListener {
                    data.id?.let { onDeleteAdminClicked(it, position!!) }
                }
            }
        }
    }
}