package com.example.xyrestoadminapp.report.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.xyrestoadminapp.report.repository.ReportRepository

class ReportViewModelFactory(private val repository: ReportRepository): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ReportViewModel(repository) as T
    }
}