package com.example.xyrestoadminapp.report.model

import android.os.Parcelable
import com.example.xyrestoadminapp.reservation.model.Reservation
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ReportResponse(
    @SerializedName("total")
    val total: Int,
    @SerializedName("reservations")
    val reservations: List<Reservation>
): Parcelable