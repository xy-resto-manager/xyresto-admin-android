package com.example.xyrestoadminapp.report

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.xyrestoadminapp.R
import com.example.xyrestoadminapp.base.model.DateHolder
import com.example.xyrestoadminapp.base.storage.network.ApiClient
import com.example.xyrestoadminapp.base.utils.ResponseWrapper
import com.example.xyrestoadminapp.base.utils.Utils
import com.example.xyrestoadminapp.base.utils.Utils.formatRp
import com.example.xyrestoadminapp.base.utils.Utils.orDefaultInt
import com.example.xyrestoadminapp.base.widget.decoration.SpacingItemDecoration
import com.example.xyrestoadminapp.databinding.FragmentReportBinding
import com.example.xyrestoadminapp.home.IRetryCallBack
import com.example.xyrestoadminapp.home.LoadingCommunicator
import com.example.xyrestoadminapp.home.LoadingMode
import com.example.xyrestoadminapp.report.network.IReportApiService
import com.example.xyrestoadminapp.report.repository.ReportRepository
import com.example.xyrestoadminapp.report.viewmodel.ReportViewModel
import com.example.xyrestoadminapp.report.viewmodel.ReportViewModelFactory
import com.example.xyrestoadminapp.reservation.adapter.ReservationRvAdapter
import com.example.xyrestoadminapp.reservation.model.Reservation

private const val ARG_SELECTED_DATE = "selected_date"

class ReportFragment : Fragment() {

    private var _viewBinding: FragmentReportBinding? = null
    private val viewBinding: FragmentReportBinding get() = _viewBinding!!
    private val viewModel: ReportViewModel by viewModels {
        ReportViewModelFactory(ReportRepository(ApiClient.getRetrofitInstance(requireContext()).create(IReportApiService::class.java)))
    }
    private val reservationAdapter by lazy { ReservationRvAdapter(::onReservationInfoClicked) }
    private var selectedDate: DateHolder = DateHolder()
    private lateinit var loadingCommunicator: LoadingCommunicator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initDate()
        savedInstanceState?.let {
            it.getParcelable<DateHolder>(ARG_SELECTED_DATE)?.let { date ->
                selectedDate = date
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        loadingCommunicator = activity as LoadingCommunicator
        loadingCommunicator.startLoading(1, LoadingMode.PROGRESS_BAR_WITH_BACKGROUND)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _viewBinding = FragmentReportBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView(){
        with(viewBinding){
            etDatePicker.setText(Utils.getCurrentDate())
            etDatePicker.setOnClickListener {
                if (cvContainer.isVisible){
                    cvContainer.visibility = View.GONE
                } else {
                    cvContainer.visibility = View.VISIBLE
                }
            }
            calendarView.setOnDateChangeListener { view, year, month, dayOfMonth ->
                selectedDate.year = year
                selectedDate.month = month + 1
                selectedDate.dayOfMonth = dayOfMonth
                etDatePicker.setText(Utils.formatDate(year, month, dayOfMonth))
                cvContainer.visibility = View.GONE
            }
            rvReservationList.run {
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                adapter = reservationAdapter.apply {
                    addItemDecoration(SpacingItemDecoration(20, 10, 20))
                }
            }
            btnGenerate.setOnClickListener {
                loadingCommunicator.startLoading(1, LoadingMode.ONLY_PROGRESS_BAR)
                viewModel.getReport(getTimeStampSelectedDate())
            }
        }
        initObserver()
    }

    private fun initObserver(){
        viewModel.reportResult.observe(viewLifecycleOwner, {
            when(it.status){
                ResponseWrapper.Status.LOADING -> {

                }
                ResponseWrapper.Status.SUCCESS -> {
                    it.body?.data?.let { data ->
                        if (data.reservations.size.orDefaultInt(0) > 0){
                            reservationAdapter.setItemList(data.reservations)
                            viewBinding.tvEmptyOrder.visibility = View.GONE
                        } else {
                            viewBinding.tvEmptyOrder.visibility = View.VISIBLE
                        }
                        viewBinding.tvTotalEarning.text = resources.getString(R.string.total_earning, data.total.formatRp())
                    }
                    loadingCommunicator.stopLoading()
                }
                ResponseWrapper.Status.ERROR -> {
                    loadingCommunicator.onError(it.body?.errors.toString(), listOf(Pair(0, object : IRetryCallBack{
                        override fun retry() {
                            viewModel.getReport(getTimeStampSelectedDate())
                        }
                    })))
                }
            }
        })
    }

    private fun onReservationInfoClicked(data: Reservation){
        data.id?.let {
            findNavController().navigate(ReportFragmentDirections.reportDetailFragment(it))
        }
    }

    private fun initDate(){
        val dates = Utils.getSeparateDate()
        selectedDate.dayOfMonth = dates[0]
        selectedDate.month = dates[1]
        selectedDate.year = dates[2]
        selectedDate.hourOfDay = dates[3]
        selectedDate.minute = dates[4]
    }

    private fun getTimeStampSelectedDate(): Long{
        val strDate = selectedDate.dayOfMonth.toString()+"-"+selectedDate.month+"-"+selectedDate.year+" "+selectedDate.hourOfDay+":"+selectedDate.minute
        return Utils.getTimeStampFormat(strDate, "dd-MM-yyyy kk:mm")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(ARG_SELECTED_DATE, selectedDate)
    }
}