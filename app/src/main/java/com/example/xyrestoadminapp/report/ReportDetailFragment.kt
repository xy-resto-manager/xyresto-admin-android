package com.example.xyrestoadminapp.report

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.xyrestoadminapp.R
import com.example.xyrestoadminapp.base.storage.network.ApiClient
import com.example.xyrestoadminapp.base.utils.ResponseWrapper
import com.example.xyrestoadminapp.base.utils.Utils
import com.example.xyrestoadminapp.base.utils.Utils.formatRp
import com.example.xyrestoadminapp.base.utils.Utils.orDefaultStr
import com.example.xyrestoadminapp.base.widget.decoration.SpacingItemDecoration
import com.example.xyrestoadminapp.databinding.FragmentReservationDetailBinding
import com.example.xyrestoadminapp.report.network.IReportApiService
import com.example.xyrestoadminapp.report.repository.ReportRepository
import com.example.xyrestoadminapp.report.viewmodel.ReportViewModel
import com.example.xyrestoadminapp.report.viewmodel.ReportViewModelFactory
import com.example.xyrestoadminapp.reservation.adapter.ReservationDetailAdapter

class ReportDetailFragment: Fragment() {

    private var _viewBinding: FragmentReservationDetailBinding? = null
    private val viewBinding: FragmentReservationDetailBinding get() = _viewBinding!!
    private val orderAdapter: ReservationDetailAdapter by lazy { ReservationDetailAdapter() }
    private val viewModel: ReportViewModel by viewModels {
        ReportViewModelFactory(
            ReportRepository(
                ApiClient.getRetrofitInstance(requireContext()).create(
                    IReportApiService::class.java))
        )
    }
    private val args: ReportDetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _viewBinding = FragmentReservationDetailBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        viewModel.getReservationDetail(args.reservationId)
    }

    private fun initView(){
        with(viewBinding){
            val layoutParam = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT)
            layoutParam.setMargins(20, 20, 20, 20)
            root.layoutParams = layoutParam
            btnApprove.visibility = View.GONE
            btnDecline.visibility = View.GONE
        }
        initRecyclerView()
        initObserver()
    }

    private fun initRecyclerView(){
        viewBinding.rvProductList.run{
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = orderAdapter.apply {
                addItemDecoration(SpacingItemDecoration(leftMargin = 10, topMargin = 10, rightMargin = 10, bottomMargin = 10))
            }
        }
    }

    private fun initObserver(){
        viewModel.reservationDetail.observe(viewLifecycleOwner, {
            when(it.status){
                ResponseWrapper.Status.LOADING -> {

                }
                ResponseWrapper.Status.SUCCESS -> {
                    it.body?.data?.let { data ->
                        data.orders?.let { orders ->
                            orderAdapter.setItemList(orders)
                        }
                        with(viewBinding){
                            tvReservationId.text = data.id.toString()
                            tvDate.text = root.context.getString(R.string.reservation_date, Utils.timeStampToDate(data.timestamp!!))
                            tvTime.text = root.context.getString(R.string.reservation_time, Utils.rangeTime(data.timestamp))
                            tvTableNumberValue.text = Utils.makeBold(data.tableId.toString(), 0, data.tableId.toString().length)
                            tvOrderedBy.text = Utils.makeBold(
                                resources.getString(
                                    R.string.ordered_by, data.fullName.orDefaultStr("-"),
                                    data.email.orDefaultStr("")
                                ),
                                13,
                                13+data.fullName.orDefaultStr("-").length
                            )
                            tvTotalOrder.text = Utils.makeBold(resources.getString(R.string.total_order, data.totalCost.formatRp()), 14, 14+data.totalCost.formatRp().length)
                        }
                    }
                }
                ResponseWrapper.Status.ERROR -> {
                    Toast.makeText(context, "ERROR: "+it.body?.errors.toString(), Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _viewBinding = null
    }
}