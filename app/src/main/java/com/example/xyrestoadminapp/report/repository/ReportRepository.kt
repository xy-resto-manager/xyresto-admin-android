package com.example.xyrestoadminapp.report.repository

import com.example.xyrestoadminapp.report.network.IReportApiService

class ReportRepository(private val iReportApiService: IReportApiService) {
    suspend fun getReport(timestamp: Long?) = iReportApiService.getReport(timestamp)
    suspend fun getReservationDetail(id: Int) = iReportApiService.getReservationDetail(id)
}