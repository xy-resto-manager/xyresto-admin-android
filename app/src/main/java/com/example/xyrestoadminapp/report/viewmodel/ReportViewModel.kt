package com.example.xyrestoadminapp.report.viewmodel

import androidx.lifecycle.*
import com.example.xyrestoadminapp.base.model.BaseApiResponse
import com.example.xyrestoadminapp.base.utils.ApiCallHelper.safeApiCall
import com.example.xyrestoadminapp.base.utils.ResponseWrapper
import com.example.xyrestoadminapp.report.model.ReportResponse
import com.example.xyrestoadminapp.report.repository.ReportRepository
import com.example.xyrestoadminapp.reservation.model.Reservation
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ReportViewModel(private val repository: ReportRepository): ViewModel() {
    private val _reportResult = MutableLiveData<ResponseWrapper<BaseApiResponse<ReportResponse>>>()
    val reportResult: LiveData<ResponseWrapper<BaseApiResponse<ReportResponse>>> get() = _reportResult
    private val _reservationDetail = MutableLiveData<ResponseWrapper<BaseApiResponse<Reservation>>>()
    val reservationDetail: LiveData<ResponseWrapper<BaseApiResponse<Reservation>>> = Transformations.switchMap(_reservationDetail, ::transformData)

    init {
        getReport(null)
    }

    fun getReport(timestamp: Long?){
        viewModelScope.launch(Dispatchers.IO) {
            _reportResult.postValue(ResponseWrapper.loading())
            _reportResult.postValue(safeApiCall { repository.getReport(timestamp) } )
        }
    }

    fun getReservationDetail(id: Int){
        viewModelScope.launch(Dispatchers.IO) {
            _reservationDetail.postValue(ResponseWrapper.loading())
            val result = safeApiCall { repository.getReservationDetail(id) }
            _reservationDetail.postValue(result)
        }
    }

    private fun transformData(reservation: ResponseWrapper<BaseApiResponse<Reservation>>) = calculateTotalCost(reservation)
    private fun calculateTotalCost(reservation: ResponseWrapper<BaseApiResponse<Reservation>>):LiveData<ResponseWrapper<BaseApiResponse<Reservation>>> {
        val liveData = MutableLiveData<ResponseWrapper<BaseApiResponse<Reservation>>>()
        var totalCost = 0
        reservation.body?.data?.orders?.let { orders ->
            for (order in orders){
                totalCost += order.product?.price?.times(order.quantity!!) ?: 0
            }
        }
        reservation.body?.data?.totalCost = totalCost
        liveData.value = reservation
        return liveData
    }
}