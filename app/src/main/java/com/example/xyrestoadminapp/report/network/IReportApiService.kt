package com.example.xyrestoadminapp.report.network

import com.example.xyrestoadminapp.base.model.BaseApiResponse
import com.example.xyrestoadminapp.report.model.ReportResponse
import com.example.xyrestoadminapp.reservation.model.Reservation
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface IReportApiService {
    @GET("admin/report/reports")
    suspend fun getReport(@Query("timestamp") timestamp: Long?): Response<BaseApiResponse<ReportResponse>>

    @GET("reservation/reservations/{reservationId}")
    suspend fun getReservationDetail(@Path("reservationId") reservationId: Int): Response<BaseApiResponse<Reservation>>
}