package com.example.xyrestoadminapp.order.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.xyrestoadminapp.base.model.BaseApiResponse
import com.example.xyrestoadminapp.base.utils.ApiCallHelper
import com.example.xyrestoadminapp.base.utils.ResponseWrapper
import com.example.xyrestoadminapp.order.repository.OrderRepository
import com.example.xyrestoadminapp.reservation.model.Reservation
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class OrderListViewModel(private val repository: OrderRepository): ViewModel() {
    private val _orders = MutableLiveData<ResponseWrapper<BaseApiResponse<List<Reservation>>>>()
    val orders: LiveData<ResponseWrapper<BaseApiResponse<List<Reservation>>>> get() = _orders

    init {
        getOrder(1)
    }

    fun getOrder(page: Int){
        viewModelScope.launch(Dispatchers.IO) {
            _orders.postValue(appendOrderList(ResponseWrapper.loading()))
            val result = ApiCallHelper.safeApiCall { repository.getOrder(page) }
            _orders.postValue(appendOrderList(result))
        }
    }

    private fun appendOrderList(response: ResponseWrapper<BaseApiResponse<List<Reservation>>>): ResponseWrapper<BaseApiResponse<List<Reservation>>>{
        _orders.value?.body?.data?.let { prevData ->
            response.body?.let {
                if (response.status == ResponseWrapper.Status.SUCCESS)
                    return ResponseWrapper(
                        response.status,
                        BaseApiResponse(
                            it.code,
                            prevData.toMutableList().apply { it.data?.let { data -> addAll(data) } },
                            it.status,
                            it.errors,
                            it.pagination
                        ),
                        response.message
                    )
            }
            return ResponseWrapper(
                response.status,
                BaseApiResponse(
                    response.body?.code,
                    prevData,
                    response.body?.status,
                    response.body?.errors,
                    _orders.value?.body?.pagination
                ),
                response.message
            )
        }
        return response
    }
}