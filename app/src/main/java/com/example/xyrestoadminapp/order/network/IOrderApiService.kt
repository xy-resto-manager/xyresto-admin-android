package com.example.xyrestoadminapp.order.network

import com.example.xyrestoadminapp.base.model.BaseApiResponse
import com.example.xyrestoadminapp.reservation.model.Reservation
import com.example.xyrestoadminapp.reservation.model.ReservationStatus
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface IOrderApiService {
    @GET("admin/reservation/reservations")
    suspend fun getOrder(@Query("status") status: ReservationStatus = ReservationStatus.DINING, @Query("page") page: Int): Response<BaseApiResponse<List<Reservation>>>
}