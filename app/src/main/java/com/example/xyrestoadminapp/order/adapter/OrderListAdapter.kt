package com.example.xyrestoadminapp.order.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.xyrestoadminapp.R
import com.example.xyrestoadminapp.base.adapter.FooterAdapter
import com.example.xyrestoadminapp.base.adapter.LoadingHolder
import com.example.xyrestoadminapp.base.utils.Utils
import com.example.xyrestoadminapp.databinding.OrdersItemBinding
import com.example.xyrestoadminapp.reservation.model.Reservation

class OrderListAdapter(private val onOrderInfoClicked: (Reservation) -> Unit): FooterAdapter<Reservation>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseRvViewHolder<Reservation> {
        when(viewType){
            VIEW_TYPE_CONTENT -> {
                val viewBinding = OrdersItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                return OrderViewHolder(viewBinding)
            }
            else -> {
                LayoutInflater.from(parent.context).inflate(R.layout.loading_item, parent, false)
                    .let {
                        return LoadingHolder(it)
                    }
            }
        }
    }

    override fun onBindViewHolder(holder: BaseRvViewHolder<Reservation>, position: Int) {
        mutableItems[position % mutableItemCount].let(holder::bind)
    }

    override fun getItemViewType(position: Int): Int {
        return if (isFooterVisible && position == mutableItemCount-1){
            VIEW_TYPE_FOOTER
        } else {
            VIEW_TYPE_CONTENT
        }
    }

    inner class OrderViewHolder(
        private val viewBinding: OrdersItemBinding
    ): BaseRvViewHolder<Reservation>(viewBinding.root){
        override fun bind(data: Reservation, position: Int?) {
            with(viewBinding) {
                data.timestamp?.let {
                    tvDate.text = root.context.getString(R.string.reservation_date, Utils.timeStampToDate(it))
                    tvTime.text = root.context.getString(R.string.reservation_time, Utils.rangeTime(data.timestamp))
                } ?: run {
                    tvDate.text = root.context.getString(R.string.reservation_date, "-")
                    tvTime.text = root.context.getString(R.string.reservation_time, "-")
                }
                tvTableNumberValue.text = data.tableId.toString()
                ivInfo.setOnClickListener { onOrderInfoClicked(data) }
            }
        }

    }
}