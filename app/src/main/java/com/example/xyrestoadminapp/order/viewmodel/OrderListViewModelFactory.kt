package com.example.xyrestoadminapp.order.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.xyrestoadminapp.order.repository.OrderRepository

class OrderListViewModelFactory(private val repository: OrderRepository): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return OrderListViewModel(repository) as T
    }
}