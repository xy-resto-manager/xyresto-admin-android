package com.example.xyrestoadminapp.order

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.xyrestoadminapp.base.listener.PaginationListener
import com.example.xyrestoadminapp.base.model.PaginationState
import com.example.xyrestoadminapp.base.storage.network.ApiClient
import com.example.xyrestoadminapp.base.utils.ResponseWrapper
import com.example.xyrestoadminapp.base.utils.Utils
import com.example.xyrestoadminapp.base.utils.Utils.orDefaultInt
import com.example.xyrestoadminapp.base.widget.decoration.SpacingItemDecoration
import com.example.xyrestoadminapp.databinding.FragmentOrderListBinding
import com.example.xyrestoadminapp.home.IRetryCallBack
import com.example.xyrestoadminapp.home.LoadingCommunicator
import com.example.xyrestoadminapp.home.LoadingMode
import com.example.xyrestoadminapp.order.adapter.OrderListAdapter
import com.example.xyrestoadminapp.order.network.IOrderApiService
import com.example.xyrestoadminapp.order.repository.OrderRepository
import com.example.xyrestoadminapp.order.viewmodel.OrderListViewModel
import com.example.xyrestoadminapp.order.viewmodel.OrderListViewModelFactory
import com.example.xyrestoadminapp.reservation.model.Reservation

private const val PAGINATION_STATE = "pagination_state"

class OrderListFragment : Fragment() {

    private var _viewBinding: FragmentOrderListBinding? = null
    private val viewBinding: FragmentOrderListBinding get() = _viewBinding!!
    private val orderListAdapter: OrderListAdapter by lazy {
        OrderListAdapter(::onOrderInfoClicked)
    }
    private val viewModel: OrderListViewModel by viewModels {
        OrderListViewModelFactory(OrderRepository(ApiClient.getRetrofitInstance(requireContext()).create(IOrderApiService::class.java)))
    }
    private val paginationState = PaginationState()
    private lateinit var loadingCommunicator: LoadingCommunicator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.getParcelable<PaginationState>(PAGINATION_STATE)?.let {
            paginationState.totalPage = it.totalPage
            paginationState.currentPage = it.currentPage
            paginationState.isLastPage = it.isLastPage
            paginationState.isLoading = it.isLoading
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        loadingCommunicator = activity as LoadingCommunicator
        loadingCommunicator.startLoading(1, LoadingMode.PROGRESS_BAR_WITH_BACKGROUND)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _viewBinding = FragmentOrderListBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        setPaginationOnOrderList()
        initObserver()
    }

    private fun initRecyclerView(){
        viewBinding.rvOrderList.run {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = orderListAdapter.apply {
                addItemDecoration(SpacingItemDecoration(leftMargin = 15, topMargin = 15, rightMargin = 15, bottomMargin = 15))
            }
        }
    }

    private fun setPaginationOnOrderList(){
        viewBinding.rvOrderList.run {
            layoutManager?.let {
                addOnScrollListener(object: PaginationListener(it, 10){
                    override fun loadMoreItems() {
                        paginationState.isLoading = true
                        viewModel.getOrder(page = paginationState.currentPage+1)
                    }

                    override fun isLastPage(): Boolean {
                        return paginationState.isLastPage
                    }

                    override fun isLoading(): Boolean {
                        return paginationState.isLoading
                    }
                })
            }
        }
    }

    private fun initObserver(){
        viewModel.orders.observe(viewLifecycleOwner, {
            when(it.status){
                ResponseWrapper.Status.LOADING -> {

                }
                ResponseWrapper.Status.SUCCESS -> {
                    paginationState.run {
                        it.body?.pagination?.let { paging ->
                            currentPage = paging.currentPage
                            totalPage = Utils.ceilInt(
                                paging.totalItems.orDefaultInt(1),
                                paging.itemsPerPage.orDefaultInt(1)
                            )
                        }
                        when {
                            it.body?.data?.size.orDefaultInt(0) <= 0 -> {
                                viewBinding.tvEmptyOrder.visibility = View.VISIBLE
                            }
                            currentPage < totalPage -> {
                                viewBinding.tvEmptyOrder.visibility = View.GONE
                                it.body?.data?.let { orderList -> orderListAdapter.setItemList(orderList, false) }
                                orderListAdapter.addFooter(Reservation(), false)
                                orderListAdapter.notifyDataSetChanged()
                            }
                            else -> {
                                viewBinding.tvEmptyOrder.visibility = View.GONE
                                isLastPage = true
                                orderListAdapter.removeFooter(false)
                                it.body?.data?.let { orderList -> orderListAdapter.setItemList(orderList) }
                            }
                        }
                    }
                    loadingCommunicator.stopLoading()
                }
                ResponseWrapper.Status.ERROR -> {
                    loadingCommunicator.onError(it.body?.errors.toString(), listOf(Pair(0, object : IRetryCallBack{
                        override fun retry() {
                            viewModel.getOrder(paginationState.currentPage+1)
                        }
                    })))
                }
            }
            paginationState.isLoading = false
        })
    }

    private fun onOrderInfoClicked(data: Reservation){
        data.id?.let {
            findNavController().navigate(OrderListFragmentDirections.orderDetailFragment(it))
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(PAGINATION_STATE, paginationState)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _viewBinding = null
    }
}