package com.example.xyrestoadminapp.order.repository

import com.example.xyrestoadminapp.order.network.IOrderApiService

class OrderRepository(private val iOrderApiService: IOrderApiService) {
    suspend fun getOrder(page: Int) = iOrderApiService.getOrder(page=page)
}