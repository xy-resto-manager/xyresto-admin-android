package com.example.xyrestoadminapp.menu.form

interface ICategoryCommunicator {
    fun createCategory(name: String)
}