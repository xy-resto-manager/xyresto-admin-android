package com.example.xyrestoadminapp.menu.form.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.xyrestoadminapp.menu.form.repository.MenuFormRepository

class MenuFormViewModelFactory(private val repository: MenuFormRepository): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = MenuFormViewModel(repository) as T
}