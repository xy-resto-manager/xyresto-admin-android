package com.example.xyrestoadminapp.menu.dashboard

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.xyrestoadminapp.R
import com.example.xyrestoadminapp.base.constant.Constant.PAGINATION_ITEM_COUNT
import com.example.xyrestoadminapp.base.constant.Constant.PRODUCT_IMAGE_VERSION
import com.example.xyrestoadminapp.base.fragment.ConfirmationDialogFragment
import com.example.xyrestoadminapp.base.fragment.DialogResponse
import com.example.xyrestoadminapp.base.fragment.IResponseCommunicator
import com.example.xyrestoadminapp.base.listener.PaginationListener
import com.example.xyrestoadminapp.base.model.Category
import com.example.xyrestoadminapp.base.model.Pagination
import com.example.xyrestoadminapp.base.model.PaginationState
import com.example.xyrestoadminapp.base.model.Product
import com.example.xyrestoadminapp.base.storage.network.ApiClient
import com.example.xyrestoadminapp.base.storage.session.IntegerDataSharedPreference
import com.example.xyrestoadminapp.base.utils.ResponseWrapper
import com.example.xyrestoadminapp.base.utils.Utils
import com.example.xyrestoadminapp.base.utils.Utils.ceilInt
import com.example.xyrestoadminapp.base.utils.Utils.getScreenDimension
import com.example.xyrestoadminapp.base.utils.Utils.orDefaultInt
import com.example.xyrestoadminapp.base.widget.decoration.SpacingItemDecoration
import com.example.xyrestoadminapp.databinding.FragmentMenuDashboardBinding
import com.example.xyrestoadminapp.home.IRetryCallBack
import com.example.xyrestoadminapp.home.LoadingCommunicator
import com.example.xyrestoadminapp.home.LoadingMode
import com.example.xyrestoadminapp.menu.dashboard.adapter.MenuCategoriesAdapter
import com.example.xyrestoadminapp.menu.dashboard.adapter.ProductsAdapter
import com.example.xyrestoadminapp.menu.dashboard.network.IMenuDashboardApiService
import com.example.xyrestoadminapp.menu.dashboard.repository.MenuDashboardRepository
import com.example.xyrestoadminapp.menu.dashboard.viewmodel.DashboardViewModelFactory
import com.example.xyrestoadminapp.menu.dashboard.viewmodel.MenuDashboardViewModel
import com.example.xyrestoadminapp.menu.form.CreateCategoryDialogFragment
import com.example.xyrestoadminapp.menu.form.ICategoryCommunicator
import com.example.xyrestoadminapp.menu.form.model.CategoryRequest

class MenuDashboardFragment : Fragment(), ICategoryCommunicator {

    companion object{
        const val SELECTED_CATEGORY = "selected_category"
        const val DELETED_CATEGORY_POSITION = "deletedCategoryPosition"
        private const val SELECTED_CATEGORY_POSITION = "selected_category_position"
        private const val CATEGORY_EDITING_STATUS = "categoryEditingStatus"
    }

    private var _viewBinding: FragmentMenuDashboardBinding? = null
    private val viewBinding: FragmentMenuDashboardBinding get() = _viewBinding!!
    private val args: MenuDashboardFragmentArgs by navArgs()
    private val viewModel: MenuDashboardViewModel by viewModels { DashboardViewModelFactory(
        MenuDashboardRepository(
            ApiClient.getRetrofitInstance(requireContext()).create(IMenuDashboardApiService::class.java)
        )
    ) }
    private val menuCategoryAdapter by lazy {
        MenuCategoriesAdapter(::onCategoryClicked, ::onNewCategoryCreated, ::onCategoryDeleted, ::openDialogFragment, selectedCategoryPosition)
    }
    private val imageVersionSharedPreference by lazy { IntegerDataSharedPreference(requireContext()) }
    private var imageVersion: Int = 0
    private val productAdapter by lazy { ProductsAdapter(::onProductEditClicked, ::onProductDeleteClicked, imageVersion) }
    private var paginationState = PaginationState()
    private var selectedCategoryPosition = 0
    private var selectedCategory: Category? = null
    private var isCategoryEdited = true
    private lateinit var loadingCommunicator: LoadingCommunicator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.let {
            selectedCategoryPosition = it.getInt(SELECTED_CATEGORY_POSITION)
            isCategoryEdited = it.getBoolean(CATEGORY_EDITING_STATUS)
        }
        imageVersion = if (args.isProductUpdated)
            imageVersionSharedPreference.incrementIntegerValue(PRODUCT_IMAGE_VERSION)
        else
            imageVersionSharedPreference.getData(PRODUCT_IMAGE_VERSION)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        loadingCommunicator = activity as LoadingCommunicator
        loadingCommunicator.startLoading(2, LoadingMode.PROGRESS_BAR_WITH_BACKGROUND)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _viewBinding = FragmentMenuDashboardBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initRecyclerView()
        initObserver()
        setPaginationOnProductList()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(SELECTED_CATEGORY_POSITION, selectedCategoryPosition)
        outState.putBoolean(CATEGORY_EDITING_STATUS, isCategoryEdited)
    }

    private fun initView(){
        with(viewBinding){
            btnEditCategory.setOnClickListener {
                menuCategoryAdapter.setEditCategoryVisibility(isCategoryEdited)
                isCategoryEdited = !isCategoryEdited
            }
            menuCategoryAdapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver(){
                override fun onChanged() {
                    if (isCategoryEdited){
                        rvMenuCategory.scrollToPosition(menuCategoryAdapter.itemCount-1)
                    }
                }
            })
            btnNewMenu.setOnClickListener {
                findNavController().navigate(MenuDashboardFragmentDirections.menuFormFragmentCreate())
            }
        }
    }

    private fun initRecyclerView(){
        with(viewBinding){
            rvMenuCategory.run {
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                adapter = menuCategoryAdapter
            }
            rvMenuList.run {
                val screenDimens = getScreenDimension(context)
                layoutManager = when {
                    screenDimens.first >= 720 -> GridLayoutManager(context, 3)
                    screenDimens.first >= 600 -> GridLayoutManager(context, 2)
                    else -> GridLayoutManager(context, 1)
                }
                if (screenDimens.first < 720){
                    addOnScrollListener(object : RecyclerView.OnScrollListener(){
                        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                            cvBtnWrapper?.run{
                                if (dy > 0){
                                    visibility = View.GONE
                                    return
                                }
                                if (dy < 0)
                                    visibility = View.VISIBLE
                            }

                        }
                    })
                }
                val margin = (5*resources.displayMetrics.density).toInt()
                adapter = productAdapter.apply { addItemDecoration(
                    SpacingItemDecoration(margin, margin, margin, margin )
                ) }
            }
        }
    }

    private fun setPaginationOnProductList(){
        with(viewBinding.rvMenuList){
            layoutManager?.let {
                addOnScrollListener(object: PaginationListener(it, PAGINATION_ITEM_COUNT){
                    override fun loadMoreItems() {
                        paginationState.isLoading = true
                        callProductListApi()
                    }

                    override fun isLastPage(): Boolean {
                        return paginationState.isLastPage
                    }

                    override fun isLoading(): Boolean {
                        return paginationState.isLoading
                    }

                })
            }
        }
    }

    private fun resetPaginationState(){
        paginationState.totalPage = 1
        paginationState.currentPage = 0
        paginationState.isLastPage = false
        paginationState.isLoading = false
    }

    private fun initObserver(){
        viewModel.menuCategories.observe(viewLifecycleOwner, {
            when(it.status){
                ResponseWrapper.Status.LOADING -> {

                }
                ResponseWrapper.Status.SUCCESS -> {
                    it.body?.data?.let { categoryList ->
                        menuCategoryAdapter.setItemList(categoryList.toMutableList().apply {
                            add(Category(-1, "Uncategorized"))
                            if (!isCategoryEdited) {
                                add(Category())
                                menuCategoryAdapter.setEditCategoryVisibility(
                                    !isCategoryEdited,
                                    false
                                )
                            }
                        })
                    }
                    loadingCommunicator.updateLoadingProgress()
                }
                ResponseWrapper.Status.ERROR -> {
                    loadingCommunicator.onError(it.body?.errors.toString(), listOf(Pair(0, object : IRetryCallBack{
                        override fun retry() {
                            viewModel.getCategories()
                        }
                    })))
                }
            }
        })
        viewModel.selectedCategory.observe(viewLifecycleOwner, { category ->
            selectedCategory = category
            viewBinding.tvMenuCategory.visibility = View.VISIBLE
            val str = resources.getString(R.string.menu_category, category.name)
            viewBinding.tvMenuCategory.text = Utils.makeBold(str, 0, str.length)
        })
        viewModel.products.observe(viewLifecycleOwner, {
            when(it.status){
                ResponseWrapper.Status.LOADING -> {

                }
                ResponseWrapper.Status.SUCCESS -> {
                    it.body?.pagination?.let { paging ->
                        paginationState.totalItem = paging.totalItems.orDefaultInt(0)
                        paginationState.currentPage = paging.currentPage
                        paginationState.totalPage = ceilInt(
                            paging.totalItems.orDefaultInt(1),
                            paging.itemsPerPage.orDefaultInt(1)
                        )
                    }
                    if (paginationState.currentPage < paginationState.totalPage) {
                        it.body?.data?.let { productList -> productAdapter.setItemList(productList, false) }
                        productAdapter.addFooter(Product(), false)
                        productAdapter.notifyDataSetChanged()
                    } else {
                        paginationState.isLastPage = true
                        productAdapter.removeFooter(false)
                        it.body?.data?.let { productList -> productAdapter.setItemList(productList) }
                    }
                    loadingCommunicator.updateLoadingProgress()
                }
                ResponseWrapper.Status.ERROR -> {
                    loadingCommunicator.onError(it.body?.errors.toString(), listOf(Pair(1, object : IRetryCallBack{
                        override fun retry() {
                            callProductListApi()
                        }
                    })))
                }
            }
            paginationState.isLoading = false
        })
        viewModel.deleteProduct.observe(viewLifecycleOwner, { event ->
            event.getContentIfNotHandled()?.let {
                when(it.first.status){
                    ResponseWrapper.Status.LOADING -> {

                    }
                    ResponseWrapper.Status.SUCCESS -> {
                        paginationState.totalItem--
                        productAdapter.removeAt(it.second)
                        productAdapter.setDeletedItemCount()
                        updatePaginationState()
                        it.first.body?.data?.let{ id ->
                            viewModel.deleteProductFromProductList(id, Pagination(
                                currentPage = paginationState.currentPage, totalItems = paginationState.totalItem, itemsPerPage = PAGINATION_ITEM_COUNT)
                            )
                        }
                        Toast.makeText(context, resources.getString(R.string.delete_menu_success, "Product"), Toast.LENGTH_LONG).show()
                    }
                    ResponseWrapper.Status.ERROR -> {
                        Toast.makeText(context, "ERROR: "+it.first.body?.errors, Toast.LENGTH_LONG).show()
                    }
                }
                loadingCommunicator.stopLoading()
            }
        })
        viewModel.deleteCategory.observe(viewLifecycleOwner, { event ->
            event.getContentIfNotHandled()?.let {
                when(it.first.status){
                    ResponseWrapper.Status.LOADING -> {

                    }
                    ResponseWrapper.Status.SUCCESS -> {
                        with(menuCategoryAdapter){
                            if (itemCount > 0){
                                setActivePosition(0, false)
                                viewModel.deleteCategoryFromList(getItems()[it.second])
                                selectedCategoryPosition = -1
                                onCategoryClicked(getItems(0)[0], 0)
                            } else {
                                viewBinding.tvMenuCategory.text = resources.getString(R.string.menu_category, "-")
                                productAdapter.clearData()
                            }
                        }
                        Toast.makeText(context, resources.getString(R.string.delete_menu_success, "Category"), Toast.LENGTH_LONG).show()
                    }
                    ResponseWrapper.Status.ERROR -> {
                        Toast.makeText(context, "ERROR: "+it.first.body?.errors, Toast.LENGTH_LONG).show()
                    }
                }
                loadingCommunicator.stopLoading()
            }
        })
        viewModel.categoryResponse.observe(viewLifecycleOwner, { event ->
            event.getContentIfNotHandled()?.let {
                when (it.status) {
                    ResponseWrapper.Status.LOADING -> {

                    }
                    ResponseWrapper.Status.SUCCESS -> {
                        it.body?.data?.let {
                            with(menuCategoryAdapter){
                                selectedCategoryPosition = -1
                                if (itemCount+1 >= 3) {
                                    setActivePosition(itemCount - 2, false)
                                    onCategoryClicked(it, itemCount - 3)
                                }
                                else {
                                    setActivePosition(0, false)
                                    onCategoryClicked(it, 0)
                                }
                                viewModel.addCategoryIntoList(it)
                            }
                        }
                    }
                    ResponseWrapper.Status.ERROR -> {
                        Toast.makeText(context, "ERROR: "+it.body?.errors, Toast.LENGTH_LONG).show()
                    }
                }
                loadingCommunicator.stopLoading()
            }
        })
    }

    private fun onCategoryClicked(data: Category, position: Int){
        resetPaginationState()
        viewBinding.cvBtnWrapper?.visibility = View.VISIBLE
        if (position != selectedCategoryPosition){
            loadingCommunicator.startLoading(1, LoadingMode.ONLY_PROGRESS_BAR)
            selectedCategoryPosition = position
            viewModel.setSelectedCategory(data)
            productAdapter.clearData()
            viewModel.getProducts(page = 1, sortByCategory=data.id, true)
        }
    }

    private fun callProductListApi(){
        selectedCategory?.id?.let { categoryId ->
            if (paginationState.currentPage == paginationState.totalPage ||
                (productAdapter.itemCount-1) % PAGINATION_ITEM_COUNT != 0){
                viewModel.getProducts(page = paginationState.currentPage, sortByCategory = categoryId)
            } else {
                viewModel.getProducts(page = paginationState.currentPage+1, sortByCategory = categoryId)
            }
        }
    }

    private fun onCategoryDeleted(category: Category, position: Int){
        val message = resources.getString(R.string.delete_menu_confirmation, category.name)
        val spanStart = resources.getInteger(R.integer.del_menu_confirm_start)
        childFragmentManager.beginTransaction().add(ConfirmationDialogFragment.newInstance(message).apply {
            setResponseCommunicator(object : IResponseCommunicator{
                override fun onResponse(
                    dialogResponse: DialogResponse,
                    data: Bundle?
                ) {
                    if (dialogResponse == DialogResponse.YES){
                        loadingCommunicator.startLoading(1, LoadingMode.ONLY_PROGRESS_BAR)
                        viewModel.deleteCategory(category.id, position)
                    }
                    dismiss()
                }
            })
            setSpannableStringBuilder(
                Utils.makeBoldAllAndColorPart(
                    message,
                    spanStart - 2 ,
                    spanStart + category.name?.length.orDefaultInt(0),
                    ContextCompat.getColor(this@MenuDashboardFragment.requireContext(), R.color.colorYellowDark_E4C104)
                )
            )
        }, null).commit()
    }

    private fun onNewCategoryCreated(name: String){
        viewModel.createCategory(CategoryRequest(name))
    }

    private fun openDialogFragment(){
        childFragmentManager.beginTransaction().add(CreateCategoryDialogFragment(), null).commit()
    }

    private fun onProductEditClicked(data: Product){
        findNavController().navigate(MenuDashboardFragmentDirections.menuFormFragmentEdit(true, data))
    }

    private fun onProductDeleteClicked(product: Product, position: Int){
        product.id?.let { id ->
            val message = resources.getString(R.string.delete_menu_confirmation, product.name)
            val spanStart = resources.getInteger(R.integer.del_menu_confirm_start)
            childFragmentManager.beginTransaction().add(ConfirmationDialogFragment.newInstance(message).apply {
                setResponseCommunicator(object : IResponseCommunicator {
                    override fun onResponse(
                        dialogResponse: DialogResponse,
                        data: Bundle?
                    ) {
                        if (dialogResponse == DialogResponse.YES){
                            loadingCommunicator.startLoading(1, LoadingMode.ONLY_PROGRESS_BAR)
                            viewModel.deleteProduct(id, position)
                        }
                        dismiss()
                    }

                })
                setSpannableStringBuilder(
                    Utils.makeBoldAllAndColorPart(
                        message,
                        spanStart - 2 ,
                        spanStart + product.name?.length.orDefaultInt(0),
                        ContextCompat.getColor(this@MenuDashboardFragment.requireContext(), R.color.colorYellowDark_E4C104)
                    )
                )
            }, null).commit()
        }
    }

    private fun updatePaginationState(){
        paginationState.totalPage = ceilInt(paginationState.totalItem,PAGINATION_ITEM_COUNT)
        paginationState.currentPage = ceilInt(productAdapter.itemCount,PAGINATION_ITEM_COUNT)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _viewBinding = null
    }

    override fun createCategory(name: String) {
        loadingCommunicator.startLoading(1, LoadingMode.ONLY_PROGRESS_BAR)
        viewModel.createCategory(CategoryRequest(name))
    }
}