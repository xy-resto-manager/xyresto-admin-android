package com.example.xyrestoadminapp.menu.dashboard.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.example.xyrestoadminapp.R
import com.example.xyrestoadminapp.base.adapter.FooterAdapter
import com.example.xyrestoadminapp.base.model.Category
import com.example.xyrestoadminapp.databinding.MenuCategoryItemBinding
import com.example.xyrestoadminapp.databinding.NewCategoryFormBinding

class MenuCategoriesAdapter(
    private val onCategoryClicked: (data: Category, position: Int) -> Unit,
    private val onCategoryCreated: (String) -> Unit,
    private val onCategoryDeleted: (Category, Int) -> Unit,
    private val openDialogFragment: () -> Unit,
    selectedPosition: Int = 0
): FooterAdapter<Category>() {

    private var activePosition: Int = selectedPosition

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseRvViewHolder<Category> {
        return if (viewType == VIEW_TYPE_CONTENT){
            val viewBinding = MenuCategoryItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            MenuCategoriesViewHolder(viewBinding)
        } else {
            val viewBinding = NewCategoryFormBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            FooterViewHolder(viewBinding)
        }
    }

    override fun onBindViewHolder(holder: BaseRvViewHolder<Category>, position: Int) {
        holder.bind(mutableItems[position % mutableItemCount], position)
    }

    override fun getItemViewType(position: Int): Int {
        return if (isFooterVisible && position == mutableItemCount-1)
            VIEW_TYPE_FOOTER
        else
            VIEW_TYPE_CONTENT
    }

    fun setEditCategoryVisibility(isVisible: Boolean = false, isNotified: Boolean = true){
        if (isVisible){
            addFooter(Category(), false)
        } else {
            removeFooter(false)
        }
        if (isNotified)
            notifyDataSetChanged()
    }

    fun setActivePosition(position: Int, isNotified: Boolean = true){
        activePosition = position
        if (isNotified)
            notifyDataSetChanged()
    }

    fun getItems(position: Int? = null): List<Category>{
        position?.let {
            return listOf(mutableItems[it])
        } ?: run {
            return mutableItems
        }
    }

    inner class MenuCategoriesViewHolder(
        private val viewBinding: MenuCategoryItemBinding
    ): BaseRvViewHolder<Category>(viewBinding.root){
        override fun bind(data: Category, position: Int?) {
            with(viewBinding){
                btnCategory.run {
                    text = data.name
                    setOnClickListener {
                        activePosition = position!!
                        onCategoryClicked(data, position)
                        notifyDataSetChanged()
                    }
                    background = if (position == activePosition){
                        ContextCompat.getDrawable(context, R.color.color_black)
                    } else {
                        ContextCompat.getDrawable(context, R.color.colorGreyDark_242424)
                    }
                }
                if (isFooterVisible && position != mutableItemCount-2){
                    ivDelete.visibility = View.VISIBLE
                    ivDelete.setOnClickListener { onCategoryDeleted(data, position!!) }
                } else {
                    ivDelete.visibility = View.GONE
                    ivDelete.setOnClickListener(null)
                }
            }
        }
    }

    inner class FooterViewHolder(
        private val viewBinding: NewCategoryFormBinding
    ): BaseRvViewHolder<Category>(viewBinding.root){
        override fun bind(data: Category, position: Int?) {
            with(viewBinding){
                ivAdd.setOnClickListener {
                    etNewCategory?.let {
                        if (it.text.isEmpty()){
                            Toast.makeText(root.context, "EMPTY CATEGORY", Toast.LENGTH_SHORT).show()
                        } else {
                            onCategoryCreated(etNewCategory.text.toString())
                        }
                    } ?: run {
                        openDialogFragment()
                    }
                }
            }
        }
    }
}