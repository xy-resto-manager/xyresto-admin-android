package com.example.xyrestoadminapp.menu.form.network

import com.example.xyrestoadminapp.base.model.BaseApiResponse
import com.example.xyrestoadminapp.base.model.Category
import com.example.xyrestoadminapp.base.model.Product
import com.example.xyrestoadminapp.menu.form.model.CategoryRequest
import com.example.xyrestoadminapp.menu.form.model.ProductRequest
import retrofit2.Response
import retrofit2.http.*
import okhttp3.MultipartBody

interface IMenuFormApiService {
    @GET("product/categories")
    suspend fun getCategories(): Response<BaseApiResponse<List<Category>>>

    @Multipart
    @POST("admin/product/{id}/image")
    suspend fun uploadProductImage(
        @Path("id") id: Int,
        @Part file: MultipartBody.Part
    ): Response<BaseApiResponse<Any>>

    @POST("admin/product/products")
    suspend fun createProduct(@Body productRequest: ProductRequest): Response<BaseApiResponse<Product>>

    @PUT("admin/product/{id}")
    suspend fun editProduct(@Path("id") id: Int, @Body productRequest: ProductRequest): Response<BaseApiResponse<Product>>

    @POST("admin/product/categories")
    suspend fun createCategory(@Body categoryRequest: CategoryRequest): Response<BaseApiResponse<Category>>
}