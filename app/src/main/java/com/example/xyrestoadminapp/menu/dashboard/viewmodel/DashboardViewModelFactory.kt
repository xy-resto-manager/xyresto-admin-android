package com.example.xyrestoadminapp.menu.dashboard.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.xyrestoadminapp.menu.dashboard.repository.MenuDashboardRepository

class DashboardViewModelFactory(private val repository: MenuDashboardRepository): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = MenuDashboardViewModel(repository) as T
}