package com.example.xyrestoadminapp.menu.form.repository

import com.example.xyrestoadminapp.menu.form.model.CategoryRequest
import com.example.xyrestoadminapp.menu.form.model.ProductRequest
import com.example.xyrestoadminapp.menu.form.network.IMenuFormApiService
import okhttp3.MultipartBody

class MenuFormRepository(private val iMenuFormApiService: IMenuFormApiService) {
    suspend fun getCategories() = iMenuFormApiService.getCategories()
    suspend fun createProduct(productRequest: ProductRequest) = iMenuFormApiService.createProduct(productRequest)
    suspend fun editProduct(id: Int, productRequest: ProductRequest) = iMenuFormApiService.editProduct(id, productRequest)
    suspend fun uploadImage(id: Int, file: MultipartBody.Part) = iMenuFormApiService.uploadProductImage(id, file)
    suspend fun createCategory(categoryRequest: CategoryRequest) = iMenuFormApiService.createCategory(categoryRequest)
}