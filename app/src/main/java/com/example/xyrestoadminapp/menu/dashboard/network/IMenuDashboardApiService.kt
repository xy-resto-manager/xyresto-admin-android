package com.example.xyrestoadminapp.menu.dashboard.network

import com.example.xyrestoadminapp.base.model.BaseApiResponse
import com.example.xyrestoadminapp.base.model.Category
import com.example.xyrestoadminapp.base.model.Product
import com.example.xyrestoadminapp.menu.form.model.CategoryRequest
import retrofit2.Response
import retrofit2.http.*

interface IMenuDashboardApiService {
    @GET("product/categories")
    suspend fun getCategories(): Response<BaseApiResponse<List<Category>>>

    @GET("product/products")
    suspend fun getProducts(@Query("page") page: Int, @Query("sort") sort: String): Response<BaseApiResponse<List<Product>>>

    @DELETE("admin/product/{id}")
    suspend fun deleteProduct(@Path("id") id: Int): Response<BaseApiResponse<Any>>

    @Headers("Content-Type:application/json")
    @DELETE("admin/product/categories/{id}")
    suspend fun deleteCategory(@Path("id") id: Int): Response<BaseApiResponse<Any>>

    @POST("admin/product/categories")
    suspend fun createCategory(@Body categoryRequest: CategoryRequest): Response<BaseApiResponse<Category>>
}