package com.example.xyrestoadminapp.menu.dashboard.repository

import com.example.xyrestoadminapp.menu.dashboard.network.IMenuDashboardApiService
import com.example.xyrestoadminapp.menu.form.model.CategoryRequest

class MenuDashboardRepository(private val iMenuDashboardApiService: IMenuDashboardApiService) {
    suspend fun getCategories() = iMenuDashboardApiService.getCategories()
    suspend fun getProducts(page: Int, sortByCategory: String) = iMenuDashboardApiService.getProducts(page, sortByCategory)
    suspend fun deleteProduct(id: Int) = iMenuDashboardApiService.deleteProduct(id)
    suspend fun deleteCategory(id: Int) = iMenuDashboardApiService.deleteCategory(id)
    suspend fun createCategory(categoryRequest: CategoryRequest) = iMenuDashboardApiService.createCategory(categoryRequest)
}