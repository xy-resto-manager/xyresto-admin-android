package com.example.xyrestoadminapp.menu.form.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProductRequest(
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("price")
    val price: Int? = null,
    @SerializedName("traits")
    val traits: List<String>? = null,
    @SerializedName("description")
    val description: String? = null,
    @SerializedName("category")
    val category: Int? = null,
    @SerializedName("recommended")
    val recommended: Boolean = false
): Parcelable