package com.example.xyrestoadminapp.menu.dashboard.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners
import com.bumptech.glide.signature.ObjectKey
import com.example.xyrestoadminapp.R
import com.example.xyrestoadminapp.base.adapter.FooterAdapter
import com.example.xyrestoadminapp.base.adapter.LoadingHolder
import com.example.xyrestoadminapp.base.constant.Constant.SERVER_URL
import com.example.xyrestoadminapp.base.model.Product
import com.example.xyrestoadminapp.databinding.MenuItemBinding

class ProductsAdapter(
    private val onProductEditClicked: (data: Product) -> Unit,
    private val onProductDeleteClicked: (data: Product, position: Int) -> Unit,
    private val imageCacheVersion: Int = 0
): FooterAdapter<Product>() {

    private var itemDeletedCount = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseRvViewHolder<Product> {
        when(viewType){
            VIEW_TYPE_CONTENT -> {
                val binding = MenuItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                return ProductsViewHolder(binding)
            }
            else -> {
                LayoutInflater.from(parent.context).inflate(R.layout.loading_item, parent, false)
                    .let {
                        return LoadingHolder(it)
                    }
            }
        }
    }

    override fun onBindViewHolder(holder: BaseRvViewHolder<Product>, position: Int) {
        holder.bind(mutableItems[position % mutableItemCount], position)
    }

    override fun getItemViewType(position: Int): Int {
        return if (isFooterVisible && position == mutableItemCount -1) {
            VIEW_TYPE_FOOTER
        } else {
            VIEW_TYPE_CONTENT
        }
    }

    fun setDeletedItemCount(){
        itemDeletedCount++
    }

    inner class ProductsViewHolder(private val viewBinding: MenuItemBinding): BaseRvViewHolder<Product>(viewBinding.root){
        override fun bind(data: Product, position: Int?) {
            with(viewBinding){
                Glide.with(root.context)
                    .load(SERVER_URL+data.imageUrl)
                    .signature(ObjectKey(imageCacheVersion))
                    .transform(CenterCrop(), GranularRoundedCorners(10.0F, 10.0F, 0.0F, 0.0F))
                    .placeholder(R.drawable.ic_food)
                    .into(ivProduct)
                tvProductTitle.text = data.name
                ivBtnEdit.setOnClickListener { onProductEditClicked(data) }
                ivBtnDelete.setOnClickListener { position?.let { onProductDeleteClicked(data, it-itemDeletedCount) } }
            }
        }

    }
}