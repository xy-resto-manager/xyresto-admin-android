package com.example.xyrestoadminapp.menu.form.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.xyrestoadminapp.base.model.BaseApiResponse
import com.example.xyrestoadminapp.base.model.Category
import com.example.xyrestoadminapp.base.model.Event
import com.example.xyrestoadminapp.base.model.Product
import com.example.xyrestoadminapp.base.utils.ApiCallHelper.safeApiCall
import com.example.xyrestoadminapp.base.utils.ResponseWrapper
import com.example.xyrestoadminapp.menu.form.model.CategoryRequest
import com.example.xyrestoadminapp.menu.form.model.ProductRequest
import com.example.xyrestoadminapp.menu.form.repository.MenuFormRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.MultipartBody

class MenuFormViewModel(private val repository: MenuFormRepository): ViewModel() {
    private val _categories = MutableLiveData<ResponseWrapper<BaseApiResponse<List<Category>>>>()
    val categories: LiveData<ResponseWrapper<BaseApiResponse<List<Category>>>> get() = _categories
    private val _product = MutableLiveData<Pair<Boolean, ResponseWrapper<BaseApiResponse<Product>>>>()
    val product: LiveData<Pair<Boolean, ResponseWrapper<BaseApiResponse<Product>>>> get() = _product
    private val _productImage = MutableLiveData<ResponseWrapper<BaseApiResponse<Any>>>()
    val productImage: LiveData<ResponseWrapper<BaseApiResponse<Any>>> get() = _productImage
    private val _categoryResponse = MutableLiveData<Event<ResponseWrapper<BaseApiResponse<Category>>>>()
    val categoryResponse: LiveData<Event<ResponseWrapper<BaseApiResponse<Category>>>> get() = _categoryResponse

    init {
        getCategories()
    }

    fun getCategories(){
        viewModelScope.launch(Dispatchers.IO) {
            val result = safeApiCall { repository.getCategories() }
            _categories.postValue(result)
        }
    }

    fun editProduct(id: Int, productRequest: ProductRequest){
        viewModelScope.launch(Dispatchers.IO) {
            _product.postValue(Pair(true, ResponseWrapper.loading()))
            val result = safeApiCall { repository.editProduct(id, productRequest) }
            _product.postValue(Pair(true, result))
        }
    }

    fun createProduct(productRequest: ProductRequest){
        viewModelScope.launch(Dispatchers.IO) {
            _product.postValue(Pair(false, ResponseWrapper.loading()))
            val result = safeApiCall { repository.createProduct(productRequest) }
            _product.postValue(Pair(false, result))
        }
    }

    fun uploadImage(id: Int, file: MultipartBody.Part){
        viewModelScope.launch(Dispatchers.IO) {
            val result = safeApiCall { repository.uploadImage(id, file) }
            _productImage.postValue(result)
        }
    }

    fun createCategory(categoryRequest: CategoryRequest){
        viewModelScope.launch(Dispatchers.IO) {
            _categoryResponse.postValue(Event(ResponseWrapper.loading()))
            val result = safeApiCall { repository.createCategory(categoryRequest) }
            _categoryResponse.postValue(Event(result))
        }
    }
}