package com.example.xyrestoadminapp.menu.form

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.xyrestoadminapp.R
import com.example.xyrestoadminapp.base.fragment.BaseDialogFragment
import com.example.xyrestoadminapp.base.utils.Utils
import com.example.xyrestoadminapp.base.utils.Utils.getScreenDimension
import com.example.xyrestoadminapp.databinding.FragmentDialogCreateCategoryBinding

class CreateCategoryDialogFragment: BaseDialogFragment() {

    private var _viewBinding: FragmentDialogCreateCategoryBinding? = null
    private val viewBinding: FragmentDialogCreateCategoryBinding get() = _viewBinding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _viewBinding = FragmentDialogCreateCategoryBinding.inflate(inflater, container, false)
        return  viewBinding.root
    }

    override fun initView() {
        with(viewBinding){
            tvMessage.text = Utils.makeBold(resources.getString(R.string.create_new_category), 0, resources.getString(R.string.create_new_category).length)
            tvCategoryTitle.text = Utils.makeBold(resources.getString(R.string.category), 0, resources.getString(R.string.category).length)
            btnSave.setOnClickListener {
                if (etCategoryName.text.toString().isNotEmpty()){
                    (parentFragment as ICategoryCommunicator).createCategory(etCategoryName.text.toString())
                    dismiss()
                }
            }
            btnCancel.setOnClickListener {
                dismiss()
            }
        }
    }

    override fun getDimension(): Pair<Int, Int> {
        val dimen = getScreenDimension(requireContext())
        val density = resources.displayMetrics.density
        return if (dimen.first < 600){
            Pair(ConstraintLayout.LayoutParams.MATCH_PARENT, (0.8*dimen.first).toInt())
        } else {
            Pair((600*density).toInt(), (400*density).toInt())
        }
    }
}