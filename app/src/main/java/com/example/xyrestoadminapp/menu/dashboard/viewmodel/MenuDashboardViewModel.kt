package com.example.xyrestoadminapp.menu.dashboard.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.xyrestoadminapp.base.constant.Constant.PAGINATION_ITEM_COUNT
import com.example.xyrestoadminapp.base.custome_class.CustomeLiveData
import com.example.xyrestoadminapp.base.model.*
import com.example.xyrestoadminapp.base.utils.ApiCallHelper.safeApiCall
import com.example.xyrestoadminapp.base.utils.ResponseWrapper
import com.example.xyrestoadminapp.base.utils.Utils.removeRange
import com.example.xyrestoadminapp.menu.dashboard.repository.MenuDashboardRepository
import com.example.xyrestoadminapp.menu.form.model.CategoryRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MenuDashboardViewModel(private val repository: MenuDashboardRepository): ViewModel() {
    private val _menuCategories = MutableLiveData<ResponseWrapper<BaseApiResponse<List<Category>>>>()
    val menuCategories: LiveData<ResponseWrapper<BaseApiResponse<List<Category>>>> get() = _menuCategories
    private val _products = CustomeLiveData<ResponseWrapper<BaseApiResponse<List<Product>>>>()
    val products: LiveData<ResponseWrapper<BaseApiResponse<List<Product>>>> get() = _products
    private val _selectedCategory = MutableLiveData<Category>()
    val selectedCategory: LiveData<Category> get() = _selectedCategory
    private val _deleteProduct = MutableLiveData<Event<Pair<ResponseWrapper<BaseApiResponse<Int>>,Int>>>()
    val deleteProduct: LiveData<Event<Pair<ResponseWrapper<BaseApiResponse<Int>>,Int>>> get() = _deleteProduct
    private val _deleteCategory = MutableLiveData<Event<Pair<ResponseWrapper<BaseApiResponse<Any>>, Int>>>()
    val deleteCategory: LiveData<Event<Pair<ResponseWrapper<BaseApiResponse<Any>>, Int>>> get() = _deleteCategory
    private val _categoryResponse = MutableLiveData<Event<ResponseWrapper<BaseApiResponse<Category>>>>()
    val categoryResponse: LiveData<Event<ResponseWrapper<BaseApiResponse<Category>>>> get() = _categoryResponse

    init {
        getCategories()
    }

    fun getCategories(){
        viewModelScope.launch(Dispatchers.IO) {
            _menuCategories.postValue(ResponseWrapper.loading())
            val result = safeApiCall { repository.getCategories() }
            if (result.status == ResponseWrapper.Status.SUCCESS){
                _selectedCategory.postValue(result.body?.data?.get(0))
                result.body?.data?.get(0)?.id?.let { getProducts(page = 1, it) }
            }
            _menuCategories.postValue(result)
        }
    }

    fun addCategoryIntoList(category: Category){
        _menuCategories.value = ResponseWrapper.success(
            BaseApiResponse(
                code = 200,
                status = "OK",
                data = _menuCategories.value?.body?.data?.toMutableList()?.apply { add(category) }
            )
        )
    }

    fun deleteCategoryFromList(category: Category){
        _menuCategories.value = ResponseWrapper.success(
            BaseApiResponse(
                code = 200,
                status = "OK",
                data = _menuCategories.value?.body?.data?.toMutableList()?.apply { remove(category) }
            )
        )
    }

    fun getProducts(page: Int, sortByCategory: Int, isNewCategory: Boolean = false){
        viewModelScope.launch(Dispatchers.IO) {
            val result = safeApiCall {
                if (sortByCategory == -1)
                    repository.getProducts(page, "null")
                else
                    repository.getProducts(page, sortByCategory.toString())
            }
            if (result.status == ResponseWrapper.Status.SUCCESS){
                _products.value?.body?.data?.let { prevProductList ->
                    val restCount = prevProductList.size % PAGINATION_ITEM_COUNT
                    if (isNewCategory){
                        _products.postValue(result)
                    } else if (prevProductList.isNotEmpty() && restCount != 0){
                        _products.postValue(
                            ResponseWrapper(
                                status = result.status,
                                body = BaseApiResponse(
                                    code = result.body?.code,
                                    data = prevProductList.toMutableList().apply {
                                        if (prevProductList.size < PAGINATION_ITEM_COUNT)
                                            removeRange(0, restCount)
                                        else
                                            removeRange(prevProductList.size-restCount-1, restCount)
                                        result.body?.data?.let { newData -> addAll(newData) }
                                    },
                                    status = result.body?.status,
                                    errors = result.body?.errors,
                                    pagination = result.body?.pagination
                                ),
                                message = result.message
                            )
                        )
                    } else {
                        _products.postValue(concatProductList(result))
                    }
                } ?: run {
                    _products.postValue(concatProductList(result))
                }
            }
        }
    }

    fun setSelectedCategory(category: Category){
        _selectedCategory.value = category
    }

    fun deleteProduct(productId: Int, position: Int){
        viewModelScope.launch(Dispatchers.IO) {
            _deleteProduct.postValue(Event(Pair(ResponseWrapper.loading(), -1)))
            val result = safeApiCall { repository.deleteProduct(productId) }
            _deleteProduct.postValue(
                Event(
                    Pair(
                        ResponseWrapper(
                            status = result.status,
                            body = BaseApiResponse(
                                result.body?.code,
                                if (result.status == ResponseWrapper.Status.SUCCESS) productId else null,
                                result.body?.status,
                                result.body?.errors
                            ),
                            message = result.message
                        ),
                        position
                    )
                )
            )
        }
    }

    fun deleteCategory(id: Int, position: Int){
        viewModelScope.launch(Dispatchers.IO) {
            _deleteCategory.postValue(Event(Pair(ResponseWrapper.loading(), -1)))
            val result = safeApiCall { repository.deleteCategory(id) }
            _deleteCategory.postValue(Event(Pair(result, position)))
        }
    }

    fun createCategory(categoryRequest: CategoryRequest){
        viewModelScope.launch(Dispatchers.IO) {
            _categoryResponse.postValue(Event(ResponseWrapper.loading()))
            val result = safeApiCall { repository.createCategory(categoryRequest) }
            _categoryResponse.postValue(Event(result))
        }
    }

    private fun concatProductList(response: ResponseWrapper<BaseApiResponse<List<Product>>>): ResponseWrapper<BaseApiResponse<List<Product>>> {
        _products.value?.body?.data?.let { prevProduct ->
            response.body?.let {
                if (response.status == ResponseWrapper.Status.SUCCESS)
                    return ResponseWrapper(
                        response.status,
                        BaseApiResponse(
                            it.code,
                            prevProduct.toMutableList().apply { it.data?.let { data -> addAll(data) } },
                            it.status,
                            it.errors,
                            it.pagination
                        ),
                        response.message
                    )
            }
            return ResponseWrapper(
                response.status,
                BaseApiResponse(
                    response.body?.code,
                    prevProduct,
                    response.body?.status,
                    response.body?.errors,
                    _products.value?.body?.pagination
                ),
                response.message
            )
        }
        return response
    }

    fun deleteProductFromProductList(productId: Int, pagination: Pagination){
        _products.value?.let {
            _products.setValueWithoutNotify(ResponseWrapper(
                it.status,
                BaseApiResponse(
                    code = it.body?.code,
                    data = it.body?.data?.toMutableList()?.apply { find { product -> product.id == productId }?.let { product ->  remove(product) } },
                    status = it.body?.status,
                    errors = it.body?.errors,
                    pagination = pagination
                ),
                it.message
            ))
        }
    }
}