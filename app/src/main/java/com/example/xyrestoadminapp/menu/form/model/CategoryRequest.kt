package com.example.xyrestoadminapp.menu.form.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CategoryRequest(
    @SerializedName("name")
    val name: String
): Parcelable
