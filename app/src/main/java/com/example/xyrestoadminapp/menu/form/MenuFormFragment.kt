package com.example.xyrestoadminapp.menu.form

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners
import com.example.xyrestoadminapp.R
import com.example.xyrestoadminapp.base.constant.Constant
import com.example.xyrestoadminapp.base.constant.Constant.SERVER_URL
import com.example.xyrestoadminapp.base.constant.Constant.TRAITS_SAVORY
import com.example.xyrestoadminapp.base.constant.Constant.TRAITS_SPICY
import com.example.xyrestoadminapp.base.constant.Constant.TRAITS_SWEETY
import com.example.xyrestoadminapp.base.fragment.FragmentWithPermissionChecker
import com.example.xyrestoadminapp.base.model.Category
import com.example.xyrestoadminapp.base.storage.network.ApiClient
import com.example.xyrestoadminapp.base.utils.FileUtils
import com.example.xyrestoadminapp.base.utils.ResponseWrapper
import com.example.xyrestoadminapp.base.utils.Utils.getScreenDimension
import com.example.xyrestoadminapp.base.widget.decoration.SpacingItemDecoration
import com.example.xyrestoadminapp.databinding.FragmentMenuFormBinding
import com.example.xyrestoadminapp.home.IRetryCallBack
import com.example.xyrestoadminapp.home.LoadingCommunicator
import com.example.xyrestoadminapp.home.LoadingMode
import com.example.xyrestoadminapp.menu.form.adapter.MenuFormAdapter
import com.example.xyrestoadminapp.menu.form.model.CategoryRequest
import com.example.xyrestoadminapp.menu.form.model.ProductRequest
import com.example.xyrestoadminapp.menu.form.network.IMenuFormApiService
import com.example.xyrestoadminapp.menu.form.repository.MenuFormRepository
import com.example.xyrestoadminapp.menu.form.viewmodel.MenuFormViewModel
import com.example.xyrestoadminapp.menu.form.viewmodel.MenuFormViewModelFactory
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File


private const val PICK_PHOTO = 10

class MenuFormFragment : FragmentWithPermissionChecker(), ICategoryCommunicator {

    private var _viewBinding: FragmentMenuFormBinding? = null
    private val viewBinding: FragmentMenuFormBinding get() = _viewBinding!!
    private val args: MenuFormFragmentArgs by navArgs()
    private var selectedCategory: Category? = null
    private val categoriesAdapter by lazy { MenuFormAdapter(::onCategoriesItemClicked) }
    private val viewModel: MenuFormViewModel by viewModels { MenuFormViewModelFactory(
        MenuFormRepository(
            ApiClient.getRetrofitInstance(requireContext(), 300000, 300000).create(IMenuFormApiService::class.java)
        )
    ) }
    private var imageUri: Uri? = null
    private var newProductId: Int? = null
    private var isNewCategoryCreated = false
    private var isNewProductCreated = false
    private val iLoadingCommunicator: LoadingCommunicator by lazy { activity as LoadingCommunicator }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.let {
            newProductId = it.getInt(KEY_NEW_PRODUCT_ID, -1)
            imageUri = it.getParcelable(KEY_IMAGE_URL)
            selectedCategory = it.getParcelable(KEY_SELECTED_CATEGORY)
            isNewCategoryCreated = it.getBoolean(KEY_IS_NEW_CATEGORY_CREATED)
            isNewProductCreated = it.getBoolean(KEY_IS_NEW_PRODUCT_CREATED)
        } ?: run {
            selectedCategory = args.product?.category
        }
        args.product?.id?.let { newProductId = it }
        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (isNewCategoryCreated || isNewProductCreated){
                    navToMenuDashboard()
                } else {
                    findNavController().popBackStack()
                }
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this,onBackPressedCallback)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _viewBinding = FragmentMenuFormBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initObserver()
    }

    private fun initView(){
        with(viewBinding){
            val density = resources.displayMetrics.density
            val widthDp = getScreenDimension(root.context, true).first
            if( widthDp <= 600){
                val width = (widthDp*density).toInt() - (20*density).toInt()
                ivUploadImg.layoutParams = ConstraintLayout.LayoutParams(width, width).apply {
                    startToStart = R.id.cl_container
                    endToEnd = R.id.cl_container
                    topToTop = R.id.cl_container
                    topMargin = (20 * density).toInt()
                }
            }
            etSelectCategory.setOnClickListener {
                if (cvDropdownContainer.isVisible){
                    cvDropdownContainer.visibility = View.GONE
                } else {
                    cvDropdownContainer.visibility = View.VISIBLE
                }
            }
            ivUploadImg.setOnClickListener {
                if (checkPermission()){
                    openFileManager()
                }
            }
            btnCancel.setOnClickListener {
                if (isNewCategoryCreated || isNewProductCreated){
                    navToMenuDashboard()
                } else {
                    findNavController().popBackStack()
                }
            }
            btnSave.setOnClickListener {
                val name = etName.text.toString()
                val price = etPrice.text.toString()
                if (name.isNotEmpty() && price.isNotEmpty()){
                    val description = etDescription.text.toString()
                    val traits = getSelectedTraits()
                    val productRequest = ProductRequest(
                        name = name,
                        price = price.toInt(),
                        category = selectedCategory?.id,
                        description = description,
                        traits = traits,
                        recommended = cbRecommended.isChecked
                    )
                    iLoadingCommunicator.startLoading(2, LoadingMode.ONLY_PROGRESS_BAR)
                    if (args.isEditing){
                        args.product?.id?.let { viewModel.editProduct(it, productRequest) }
                    } else {
                        viewModel.createProduct(productRequest)
                    }
                } else {
                    Toast.makeText(root.context, "Field Name and Price must be filled", Toast.LENGTH_SHORT).show()
                }
            }
            btnNewCategory.setOnClickListener {
                childFragmentManager.beginTransaction().add(CreateCategoryDialogFragment(), null).commit()
            }
            if (args.isEditing){
                etName.setText(args.product?.name)
                etPrice.setText(args.product?.price.toString())
                args.product?.traits?.let { setSelectedTraits(it) }
                etDescription.setText(args.product?.description)
                args.product?.recommended?.let { cbRecommended.isChecked = it }
                Glide.with(root.context)
                    .load(SERVER_URL + args.product?.imageUrl)
                    .transform(CenterCrop(), GranularRoundedCorners(10.0F, 10.0F, 10.0F, 10.0F))
                    .placeholder(R.drawable.ic_food)
                    .into(ivUploadImg)
            }
        }
        initRecyclerView()
        setImageToImageView()
    }

    private fun initRecyclerView(){
        viewBinding.rvCategoryList.run {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = categoriesAdapter.apply {
                addItemDecoration(SpacingItemDecoration(leftMargin = 30, rightMargin = 10))
            }
        }
    }

    private fun initObserver(){
        iLoadingCommunicator.startLoading(1, LoadingMode.PROGRESS_BAR_WITH_BACKGROUND)
        viewModel.categories.observe(viewLifecycleOwner, {
            when (it.status) {
                ResponseWrapper.Status.SUCCESS -> {
                    it.body?.data?.let { categories ->
                        categoriesAdapter.setItemList(categories.toMutableList().apply { add(Category(-1, "Uncategorized")) })
                        selectedCategory?.let { category ->
                            viewBinding.etSelectCategory.setText(category.name)
                        } ?: run {
                            viewBinding.etSelectCategory.setText(resources.getString(R.string.select_category))
                        }
                    }
                    iLoadingCommunicator.stopLoading()
                }

                ResponseWrapper.Status.ERROR -> {
                    iLoadingCommunicator.onError(it.body?.errors.toString(), listOf(Pair(0, object : IRetryCallBack{
                        override fun retry() {
                            viewModel.getCategories()
                        }
                    })))
                }
            }
        })
        viewModel.product.observe(viewLifecycleOwner, {
            when (it.second.status) {
                ResponseWrapper.Status.LOADING -> {

                }
                ResponseWrapper.Status.SUCCESS -> {
                    isNewProductCreated = true
                    imageUri?.let { uri ->
                        iLoadingCommunicator.updateLoadingProgress()
                        it.second.body?.data?.id?.let { productId ->
                            newProductId = productId
                            uploadProductImage(uri, productId)
                        }
                    } ?: run {
                        iLoadingCommunicator.stopLoading()
                        if (it.first) {
                            Toast.makeText(context, "UPDATE PRODUCT SUCCESSFULLY", Toast.LENGTH_SHORT).show()
                        } else {
                            Toast.makeText(context, "CREATE PRODUCT SUCCESSFULLY", Toast.LENGTH_SHORT).show()
                        }
                        navToMenuDashboard()
                    }
                }
                ResponseWrapper.Status.ERROR -> {
                    iLoadingCommunicator.stopLoading()
                    Toast.makeText(context, "ERROR: "+it.second.body?.errors, Toast.LENGTH_SHORT).show()
                }
            }
        })
        viewModel.productImage.observe(viewLifecycleOwner, {
            when (it.status) {
                ResponseWrapper.Status.LOADING -> {

                }
                ResponseWrapper.Status.SUCCESS -> {
                    iLoadingCommunicator.stopLoading()
                    navToMenuDashboard(true)
                }
                ResponseWrapper.Status.ERROR -> {
                    iLoadingCommunicator.onError(it.body?.errors.toString(), listOf(Pair(1, object : IRetryCallBack{
                        override fun retry() {
                            if (newProductId != null && newProductId != -1)
                                uploadProductImage(imageUri!!, newProductId!!)
                        }
                    })))
                }
            }
        })
        viewModel.categoryResponse.observe(viewLifecycleOwner, { event ->
            event.getContentIfNotHandled()?.let {
                when (it.status) {
                    ResponseWrapper.Status.LOADING -> {

                    }
                    ResponseWrapper.Status.SUCCESS -> {
                        it.body?.data?.let { category -> categoriesAdapter.addItemAt(categoriesAdapter.itemCount-1, category) }
                        viewBinding.cvDropdownContainer.visibility = View.GONE
                        isNewCategoryCreated = true
                    }
                    ResponseWrapper.Status.ERROR -> {
                        Toast.makeText(context, "ERROR: "+it.body?.errors, Toast.LENGTH_LONG).show()
                    }
                }
                iLoadingCommunicator.stopLoading()
            }
        })
    }

    private fun openFileManager(){
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "image/*"
        startActivityForResult(intent, PICK_PHOTO)
    }

    private fun uploadProductImage(uri: Uri, productId: Int){
        FileUtils.getPath(uri, requireContext())?.let { path ->
            val file = File(path)
            val requestFile = file.asRequestBody(("image/jpeg").toMediaTypeOrNull())
            val body = MultipartBody.Part.createFormData(
                "file",
                file.name,
                requestFile
            )
            viewModel.uploadImage(productId, body)
        }
    }

    private fun onCategoriesItemClicked(category: Category){
        with(viewBinding){
            cvDropdownContainer.visibility = View.GONE
            etSelectCategory.setText(category.name)
            selectedCategory = if (category.id == -1)
                null
            else
                category
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when(requestCode){
            Constant.PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    openFileManager()
                } else {
                    Toast.makeText(requireContext(), "CANNOT OPEN FILE MANAGER", Toast.LENGTH_LONG).show()
                }
            }
            else -> {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_PHOTO && resultCode == Activity.RESULT_OK){
            if (data != null){
                imageUri = data.data
                setImageToImageView()
            }
        }
    }

    private fun getSelectedTraits(): List<String>{
        val traits = mutableListOf<String>()
        with(viewBinding){
            if (cbTraitsSpicy.isChecked){ traits.add(TRAITS_SPICY) }
            if (cbTraitsSweety.isChecked){ traits.add(TRAITS_SWEETY) }
            if (cbTraitsSavory.isChecked){ traits.add(TRAITS_SAVORY) }
        }
        return traits
    }

    private fun setSelectedTraits(traits: List<String>){
        with(viewBinding){
            if (traits.contains(TRAITS_SPICY)) { cbTraitsSpicy.isChecked = true }
            if (traits.contains(TRAITS_SWEETY)) { cbTraitsSweety.isChecked = true }
            if (traits.contains(TRAITS_SAVORY)) { cbTraitsSavory.isChecked = true }
        }
    }

    private fun setImageToImageView(){
        imageUri?.let {
            Glide.with(viewBinding.root.context)
                .load(it)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .error(R.drawable.ic_food)
                .transform(CenterCrop(), GranularRoundedCorners(10.0F, 10.0F, 10.0F, 10.0F))
                .into(viewBinding.ivUploadImg)
        }
    }

    private fun navToMenuDashboard(isProductImageUpdated: Boolean = false){
        findNavController().navigate(
            MenuFormFragmentDirections.menuDashboard().setIsProductUpdated(isProductImageUpdated),
            NavOptions.Builder().setPopUpTo(R.id.nav_menu, true).build()
        )
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        newProductId?.let { outState.putInt(KEY_NEW_PRODUCT_ID, it) }
        outState.putParcelable(KEY_IMAGE_URL, imageUri)
        outState.putParcelable(KEY_SELECTED_CATEGORY, selectedCategory)
        outState.putBoolean(KEY_IS_NEW_CATEGORY_CREATED, isNewCategoryCreated)
        outState.putBoolean(KEY_IS_NEW_PRODUCT_CREATED, isNewProductCreated)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _viewBinding = null
    }

    override fun createCategory(name: String) {
        iLoadingCommunicator.startLoading(1, LoadingMode.ONLY_PROGRESS_BAR)
        viewModel.createCategory(CategoryRequest(name))
    }

    companion object{
        private const val KEY_NEW_PRODUCT_ID = "newProductId"
        private const val KEY_IMAGE_URL = "keyImageUrl"
        private const val KEY_SELECTED_CATEGORY = "selectedCategory"
        private const val KEY_IS_NEW_CATEGORY_CREATED = "isNewCategoryCreated"
        private const val KEY_IS_NEW_PRODUCT_CREATED = "isNewProductCreated"
    }
}