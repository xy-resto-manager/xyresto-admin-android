package com.example.xyrestoadminapp.menu.form.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.xyrestoadminapp.base.adapter.BaseRvAdapter
import com.example.xyrestoadminapp.base.model.Category
import com.example.xyrestoadminapp.databinding.CategoryListItemBinding

class MenuFormAdapter(private val onCategoryItemClicked: (Category) -> Unit): BaseRvAdapter<Category>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseRvViewHolder<Category> {
        val viewBinding = CategoryListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MenuFormViewHolder(viewBinding)
    }

    override fun onBindViewHolder(holder: BaseRvViewHolder<Category>, position: Int) {
        mutableItems[position % mutableItemCount].let(holder::bind)
    }

    inner class MenuFormViewHolder(private val viewBinding: CategoryListItemBinding): BaseRvViewHolder<Category>(viewBinding.root){
        override fun bind(data: Category, position: Int?) {
            viewBinding.tvCategoryName.text = data.name
            viewBinding.root.setOnClickListener { onCategoryItemClicked(data) }
        }

    }
}