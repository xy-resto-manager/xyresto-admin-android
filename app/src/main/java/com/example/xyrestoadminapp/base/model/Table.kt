package com.example.xyrestoadminapp.base.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Table(
    @SerializedName("tableId")
    val tableId: Int = 0,
    @SerializedName("x")
    var x: Int = 0,
    @SerializedName("y")
    var y: Int = 0,
    var booked: Boolean = false
): Parcelable
