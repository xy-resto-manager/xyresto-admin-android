package com.example.xyrestoadminapp.base.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.DialogFragment

abstract class BaseDialogFragment: DialogFragment() {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    protected abstract fun initView()

    protected abstract fun getDimension(): Pair<Int, Int>

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            getDimension().first,
            getDimension().second
        )
    }
}