package com.example.xyrestoadminapp.base.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Category(
    @field:SerializedName("id")
    val id: Int = 0,
    @field:SerializedName("name")
    val name: String? = null
): Parcelable
