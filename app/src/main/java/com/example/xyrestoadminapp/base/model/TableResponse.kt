package com.example.xyrestoadminapp.base.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TableResponse(
    @SerializedName("table")
    val table: Table? = null,
    @SerializedName("booked")
    val booked: Boolean? = null
): Parcelable
