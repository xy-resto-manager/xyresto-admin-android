package com.example.xyrestoadminapp.base.storage.network

import android.content.Context
import android.content.Intent
import com.example.xyrestoadminapp.base.constant.Constant.USER_TOKEN
import com.example.xyrestoadminapp.base.storage.session.SessionManager
import com.example.xyrestoadminapp.login.LoginActivity
import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor(private val context: Context) : Interceptor {
    private val sessionManager = SessionManager(context)

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val requestBuilder = chain.request().newBuilder()

        if (request.url.encodedPath.contains("admin") ||
            request.url.encodedPath.contains("reservation") ||
            request.url.encodedPath.contains("_logout")){
            sessionManager.getData(USER_TOKEN)?.let {
                requestBuilder.addHeader("Authorization", "Bearer $it")
            }
            val response = chain.proceed(requestBuilder.build())
            if (response.code == 403){
                context.startActivity(Intent(context, LoginActivity::class.java))
            }
            return response
        }
        return chain.proceed(request)
    }

}