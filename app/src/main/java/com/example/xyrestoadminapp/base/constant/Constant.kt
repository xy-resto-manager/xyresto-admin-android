package com.example.xyrestoadminapp.base.constant

object Constant {
    const val SERVER_URL = "http://xyresto-admin.maximuse.xyz:8080"
    const val BASE_URL = "$SERVER_URL/api/"

    const val PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123

    // User Role
    const val ADMIN_ROLE = "ADMIN"
    const val USER_ROLE = "USER"

    //Reservation Status
    const val STATUS_PENDING = "PENDING"
    const val STATUS_APPROVED = "APPROVED"
    const val STATUS_DECLINED = "DECLINED"

    //pagination
    const val PAGINATION_ITEM_COUNT = 10

    //Traits
    const val TRAITS_SPICY = "SPICY"
    const val TRAITS_SWEETY = "SWEETY"
    const val TRAITS_SAVORY = "SAVORY"

    //Shared Preferences KEY
    const val USER_TOKEN = "user_token"
    const val PRODUCT_KEY = "product"
    const val PRODUCT_IMAGE_VERSION = "productImageVersion"
}