package com.example.xyrestoadminapp.base.storage.session

import android.content.Context
import com.example.xyrestoadminapp.base.constant.Constant.USER_TOKEN

class SessionManager(context: Context): BaseSharedPreference<String?>(context) {

    override fun saveData(data: String?, key: String) {
        val editor = prefs.edit()
        editor.putString(USER_TOKEN, data)
        editor.apply()
    }

    override fun getData(key: String): String? {
        return prefs.getString(USER_TOKEN, null)
    }

}