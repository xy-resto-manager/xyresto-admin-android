package com.example.xyrestoadminapp.base.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Product(
    @field:SerializedName("id")
    val id: Int? = null,
    @field:SerializedName("name")
    val name: String? = null,
    @field:SerializedName("price")
    val price: Int = 0,
    @field:SerializedName("traits")
    val traits: List<String>? = null,
    @field:SerializedName("description")
    val description: String? = null,
    @field:SerializedName("imageUrl")
    val imageUrl: String? = null,
    @field:SerializedName("category")
    val category: Category? = null,
    @field:SerializedName("recommended")
    val recommended: Boolean = false,
    @field:SerializedName("purchaseCount")
    val purchaseCount: Long = 0,
): Parcelable
