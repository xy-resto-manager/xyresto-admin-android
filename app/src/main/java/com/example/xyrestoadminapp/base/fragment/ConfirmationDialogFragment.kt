package com.example.xyrestoadminapp.base.fragment

import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import com.example.xyrestoadminapp.R
import com.example.xyrestoadminapp.base.utils.Utils
import com.example.xyrestoadminapp.databinding.ConfirmationDialogFragmentBinding

class ConfirmationDialogFragment: BaseDialogFragment() {

    private lateinit var iResponseCommunicator: IResponseCommunicator
    private var _viewBinding: ConfirmationDialogFragmentBinding? = null
    private val viewBinding: ConfirmationDialogFragmentBinding get() = _viewBinding!!
    private var message: String = ""
    private var spannable: SpannableStringBuilder? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        message = arguments?.getString(KEY_MESSAGE).orEmpty()
        retainInstance = true
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _viewBinding = ConfirmationDialogFragmentBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun initView() {
        with(viewBinding){
            tvMessage.text = if (spannable == null) Utils.makeBold(message, 0, message.length) else spannable
            btnYes.setOnClickListener {
                if (this@ConfirmationDialogFragment::iResponseCommunicator.isInitialized){
                    iResponseCommunicator.onResponse(DialogResponse.YES, null)

                }
            }
            btnNo.setOnClickListener {
                if (this@ConfirmationDialogFragment::iResponseCommunicator.isInitialized){
                    iResponseCommunicator.onResponse(DialogResponse.NO, null)
                }
            }
        }
    }

    override fun getDimension(): Pair<Int, Int> {
        return Pair(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT)
    }

    fun setResponseCommunicator(iCommunicator: IResponseCommunicator){
        iResponseCommunicator = iCommunicator
    }

    fun setSpannableStringBuilder(spannable: SpannableStringBuilder){
        this.spannable = spannable
    }

    companion object{
        const val KEY_DATA = "key_data"
        const val KEY_MESSAGE = "key_message"

        fun newInstance(message: String): ConfirmationDialogFragment{
            return ConfirmationDialogFragment().apply {
                arguments = Bundle().apply { putString(KEY_MESSAGE, message) }
            }
        }
    }
}

interface IResponseCommunicator{
    fun onResponse(dialogResponse: DialogResponse, data: Bundle?)
}

enum class DialogResponse{
    YES, NO
}