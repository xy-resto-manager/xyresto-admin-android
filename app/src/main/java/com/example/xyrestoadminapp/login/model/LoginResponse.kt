package com.example.xyrestoadminapp.login.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LoginResponse(
    @field:SerializedName("token")
    val token: String? = null,
    @field:SerializedName("fullName")
    val fullName: String? = null,
    @field:SerializedName("email")
    val email: String? = null,
    @field:SerializedName("roles")
    val roles: List<String>? = null
): Parcelable
