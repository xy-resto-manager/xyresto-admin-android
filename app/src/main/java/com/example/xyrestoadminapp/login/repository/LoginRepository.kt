package com.example.xyrestoadminapp.login.repository

import android.content.Context
import com.example.xyrestoadminapp.base.storage.network.ApiClient
import com.example.xyrestoadminapp.login.model.LoginRequest
import com.example.xyrestoadminapp.login.network.ILoginApiService

class LoginRepository(context: Context){
    private val iLoginApiService = ApiClient.getRetrofitInstance(context).create(ILoginApiService::class.java)
    suspend fun callLoginApi(loginRequest: LoginRequest) = iLoginApiService.login(loginRequest=loginRequest)
}