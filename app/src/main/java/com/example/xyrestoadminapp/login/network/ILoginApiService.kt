package com.example.xyrestoadminapp.login.network

import com.example.xyrestoadminapp.base.model.BaseApiResponse
import com.example.xyrestoadminapp.login.model.LoginRequest
import com.example.xyrestoadminapp.login.model.LoginResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface ILoginApiService {
    @POST("user/_login")
    suspend fun login(
        @Body loginRequest: LoginRequest? = null
    ): Response<BaseApiResponse<LoginResponse>>
}