package com.example.xyrestoadminapp.login.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.xyrestoadminapp.base.model.BaseApiResponse
import com.example.xyrestoadminapp.base.utils.ApiCallHelper
import com.example.xyrestoadminapp.base.utils.ResponseWrapper
import com.example.xyrestoadminapp.login.model.LoginRequest
import com.example.xyrestoadminapp.login.model.LoginResponse
import com.example.xyrestoadminapp.login.repository.LoginRepository
import kotlinx.coroutines.launch

class LoginViewModel(private val loginRepository: LoginRepository): ViewModel(){
    private val _loginResponse = MutableLiveData<ResponseWrapper<BaseApiResponse<LoginResponse>>>()
    val loginResponse: LiveData<ResponseWrapper<BaseApiResponse<LoginResponse>>>
        get() = _loginResponse

    fun callLoginApi(loginRequest: LoginRequest) {
        viewModelScope.launch {
            _loginResponse.postValue(ResponseWrapper.loading())
            val result = ApiCallHelper.safeApiCall { loginRepository.callLoginApi(loginRequest) }
            _loginResponse.postValue(result)
        }
    }
}