package com.example.xyrestoadminapp.login

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.method.PasswordTransformationMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.Toast
import com.example.xyrestoadminapp.R
import com.example.xyrestoadminapp.base.constant.Constant.ADMIN_ROLE
import com.example.xyrestoadminapp.base.constant.Constant.USER_TOKEN
import com.example.xyrestoadminapp.base.storage.session.SessionManager
import com.example.xyrestoadminapp.base.utils.ResponseWrapper
import com.example.xyrestoadminapp.base.utils.Utils
import com.example.xyrestoadminapp.databinding.ActivityLoginBinding
import com.example.xyrestoadminapp.home.HomeActivity
import com.example.xyrestoadminapp.login.model.LoginRequest
import com.example.xyrestoadminapp.login.repository.LoginRepository
import com.example.xyrestoadminapp.login.viewmodel.LoginViewModel
import com.example.xyrestoadminapp.register.view.RegisterActivity

class LoginActivity : AppCompatActivity() {
    private lateinit var viewBinding: ActivityLoginBinding
    private val loginViewModel: LoginViewModel by lazy { LoginViewModel(LoginRepository(this)) }

    companion object{
        const val EMAIL = "email"
        private const val ONLY_ADMIN_ALLOWED = "Only Admin Allowed, Try To Call Admin For Permission"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)
        initView()
        initObserver()
    }

    private fun initView(){
        with(viewBinding){
            layoutFieldEmail.tvLabel.text = resources.getString(R.string.label_email)
            layoutFieldEmail.etInput.setText(intent.getStringExtra(EMAIL))
            layoutFieldPassword.tvLabel.text = resources.getString(R.string.label_password)
            layoutFieldPassword.etInput.transformationMethod = PasswordTransformationMethod.getInstance()
            tvForgotPassword.setOnClickListener {
                // redirect to change password
            }
            btnLogin.setOnClickListener {
                callLoginApi()
            }
            val spannableString = SpannableString(resources.getString(R.string.to_register))
            val clickableSpan: ClickableSpan = object : ClickableSpan() {
                override fun onClick(widget: View) {
                    startActivity(Intent(this@LoginActivity, RegisterActivity::class.java))
                }

                override fun updateDrawState(ds: TextPaint) {
                    super.updateDrawState(ds)
                    ds.color = Utils.getThemeAttribute(R.attr.themeContentTextColorAccent, root.context)
                    ds.isUnderlineText = false
                    ds.isFakeBoldText = true
                }
            }
            spannableString.setSpan(clickableSpan,
                resources.getInteger(R.integer.register_span_start),
                resources.getInteger(R.integer.register_span_end),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            tvToRegister.run {
                text = spannableString
                highlightColor = Color.TRANSPARENT
                movementMethod = LinkMovementMethod.getInstance()
            }
        }
    }

    private fun initObserver(){
        loginViewModel.loginResponse.observe(this, {
            when(it.status){
                ResponseWrapper.Status.LOADING -> {
                    Log.d("LOADING", "YES")
                }
                ResponseWrapper.Status.SUCCESS -> {
                    Log.d("TOKEN", it.body?.data?.token.toString())
                    if (it.body?.data?.roles?.contains(ADMIN_ROLE) == true){
                        SessionManager(this).saveData(it.body.data.token.toString(), USER_TOKEN)
                        redirectToPreviousScreen()
                    } else {
                        Toast.makeText(this, ONLY_ADMIN_ALLOWED, Toast.LENGTH_SHORT).show()
                    }
                }
                ResponseWrapper.Status.ERROR -> {
                    Toast.makeText(this, it.body?.errors.toString(), Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun callLoginApi(){
        val email = viewBinding.layoutFieldEmail.etInput.text.toString().trim()
        val password = viewBinding.layoutFieldPassword.etInput.text.toString()
        val loginRequest = LoginRequest(email = email, password = password)
        if (email.isEmpty() || password.isEmpty()){
            Toast.makeText(this, resources.getString(R.string.empty_email_password), Toast.LENGTH_SHORT).show()
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            Toast.makeText(this, resources.getString(R.string.wrong_email_format), Toast.LENGTH_SHORT).show()
        } else {
            loginViewModel.callLoginApi(loginRequest)
        }
    }

    private fun redirectToPreviousScreen(){
        startActivity(Intent(this, HomeActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
    }

}