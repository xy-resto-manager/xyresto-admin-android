package com.example.xyrestoadminapp.home.api

class LogoutRepository(private val iLogoutApiService: ILogoutApiService) {
    suspend fun logout() = iLogoutApiService.logout()
}