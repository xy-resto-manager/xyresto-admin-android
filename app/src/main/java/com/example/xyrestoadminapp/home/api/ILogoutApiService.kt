package com.example.xyrestoadminapp.home.api

import com.example.xyrestoadminapp.base.model.BaseApiResponse
import retrofit2.Response
import retrofit2.http.POST

interface ILogoutApiService {
    @POST("user/_logout")
    suspend fun logout(): Response<BaseApiResponse<Any>>
}