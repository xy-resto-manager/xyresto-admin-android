package com.example.xyrestoadminapp.home

interface IRetryCallBack {
    fun retry()
}