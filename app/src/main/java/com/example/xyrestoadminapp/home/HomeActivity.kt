package com.example.xyrestoadminapp.home

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.xyrestoadminapp.R
import com.example.xyrestoadminapp.base.constant.Constant.USER_TOKEN
import com.example.xyrestoadminapp.base.fragment.ConfirmationDialogFragment
import com.example.xyrestoadminapp.base.fragment.DialogResponse
import com.example.xyrestoadminapp.base.fragment.IResponseCommunicator
import com.example.xyrestoadminapp.base.storage.network.ApiClient
import com.example.xyrestoadminapp.base.storage.session.SessionManager
import com.example.xyrestoadminapp.base.utils.ResponseWrapper
import com.example.xyrestoadminapp.databinding.ActivityHomeBinding
import com.example.xyrestoadminapp.home.api.ILogoutApiService
import com.example.xyrestoadminapp.home.api.LogoutRepository
import com.example.xyrestoadminapp.home.api.LogoutViewModel
import com.example.xyrestoadminapp.home.api.LogoutViewModelFactory
import com.example.xyrestoadminapp.login.LoginActivity
import kotlin.system.exitProcess

class HomeActivity : AppCompatActivity(), LoadingCommunicator {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var viewBinding: ActivityHomeBinding
    private val sessionManager by lazy { SessionManager(this) }
    private val viewModel: LogoutViewModel by viewModels {
        LogoutViewModelFactory(LogoutRepository(ApiClient.getRetrofitInstance(this).create(ILogoutApiService::class.java)))
    }
    private var maxLoadingTime = 0
    private var loadingProgress = 0
    private var loadingMode: LoadingMode = LoadingMode.PROGRESS_BAR_WITH_BACKGROUND

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)
        initNavDrawer()
        initErrorView()
        setLogoutButton()
        checkLoggedIn()
    }

    private fun initNavDrawer(){
        setSupportActionBar(viewBinding.layoutAppBar.toolbar)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_reservations, R.id.nav_menu, R.id.nav_orders, R.id.nav_table, R.id.nav_banner,
                R.id.nav_report, R.id.nav_admins, R.id.nav_edit_table
            ), viewBinding.drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        viewBinding.navView.setupWithNavController(navController)
    }

    private fun initErrorView(){
        with(viewBinding.layoutAppBar.layoutContentMain){
            btnExit.setOnClickListener {
                finish()
                exitProcess(0)
            }
        }
    }

    private fun setLogoutButton(){
        viewBinding.btnLogout.setOnClickListener {
            val dialogFragment = ConfirmationDialogFragment.newInstance(resources.getString(R.string.logout_message)).apply {
                setResponseCommunicator(object : IResponseCommunicator {
                    override fun onResponse(
                        dialogResponse: DialogResponse,
                        data: Bundle?
                    ) {
                        if (dialogResponse == DialogResponse.YES)
                            logout()
                         dismiss()
                    }
                })
            }
            supportFragmentManager.beginTransaction().add(dialogFragment, null).commit()
        }
    }

    private fun logout(){
        viewModel.logout().observe(this, {
            when(it.status){
                ResponseWrapper.Status.SUCCESS -> {
                    sessionManager.removeData(USER_TOKEN)
                    recreate()
                }
                ResponseWrapper.Status.ERROR -> {
                    Toast.makeText(this, "ERROR: LOGOUT FAILED", Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    private fun checkLoggedIn(){
        if (sessionManager.getData(USER_TOKEN) == null) {
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }

    override fun startLoading(maxIteration: Int, mode: LoadingMode) {
        loadingMode = mode
        maxLoadingTime = maxIteration
        loadingProgress = 0
        toggleErrorMessage(null, false)
        toggleLoadingVisibility(true)
    }

    override fun updateLoadingProgress() {
        loadingProgress++
        if (loadingProgress == maxLoadingTime){
            stopLoading()
        }
    }

    override fun getProgress(): Pair<Int,Int> {
        return Pair(loadingProgress, maxLoadingTime)
    }

    override fun stopLoading() {
        toggleLoadingVisibility(false)
    }

    override fun onError(message: String, callbacks: List<Pair<Int, IRetryCallBack>>) {
        toggleErrorMessage(message, true)
        with(viewBinding.layoutAppBar.layoutContentMain){
            btnRetry.setOnClickListener {
                groupError.visibility = View.GONE
                startLoading(callbacks.size, LoadingMode.PROGRESS_BAR_WITH_BACKGROUND)
                callbacks.forEach { callback ->
                    callback.second.retry()
                }
            }
        }
    }

    private fun toggleErrorMessage(message: String?, isVisible: Boolean){
        with(viewBinding.layoutAppBar.layoutContentMain){
            if (isVisible){
                loadingProgress = 0
                maxLoadingTime = -1
                toggleLoadingVisibility(isVisible = false, hideViewBackground = false)
                viewBackground.background = ContextCompat.getDrawable(root.context, android.R.color.white)
                message?.let { tvErrorMessage.text = it }
                groupError.visibility = View.VISIBLE
            } else {
                groupError.visibility = View.GONE
            }
        }
    }

    private fun toggleLoadingVisibility(isVisible: Boolean, hideViewBackground : Boolean = true){
        with(viewBinding.layoutAppBar.layoutContentMain){
            if (isVisible){
                if (loadingMode == LoadingMode.ONLY_PROGRESS_BAR)
                    viewBackground.background = ContextCompat.getDrawable(root.context, android.R.color.transparent)
                else
                    viewBackground.background = ContextCompat.getDrawable(root.context, android.R.color.white)
                viewBackground.visibility = View.VISIBLE
                progressBar.visibility = View.VISIBLE
            } else {
                if (hideViewBackground)
                    viewBackground.visibility = View.GONE
                progressBar.visibility = View.GONE
            }
        }
    }
}