package com.example.xyrestoadminapp.home

enum class LoadingMode{
    ONLY_PROGRESS_BAR,
    PROGRESS_BAR_WITH_BACKGROUND
}

interface LoadingCommunicator {
    fun startLoading(maxIteration: Int, mode: LoadingMode)
    fun updateLoadingProgress()
    fun getProgress(): Pair<Int, Int>
    fun stopLoading()
    fun onError(message: String, callbacks: List<Pair<Int,IRetryCallBack>>)
}