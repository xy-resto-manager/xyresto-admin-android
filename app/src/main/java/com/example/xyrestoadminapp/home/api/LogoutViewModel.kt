package com.example.xyrestoadminapp.home.api

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.xyrestoadminapp.base.model.BaseApiResponse
import com.example.xyrestoadminapp.base.utils.ApiCallHelper.safeApiCall
import com.example.xyrestoadminapp.base.utils.ResponseWrapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LogoutViewModel(private val repository: LogoutRepository): ViewModel() {
    fun logout(): LiveData<ResponseWrapper<BaseApiResponse<Any>>>{
        val liveData = MutableLiveData<ResponseWrapper<BaseApiResponse<Any>>>()
        viewModelScope.launch(Dispatchers.IO) {
            liveData.postValue(safeApiCall { repository.logout() })
        }
        return liveData
    }
}