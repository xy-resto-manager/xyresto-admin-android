package com.example.xyrestoadminapp.home.api

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class LogoutViewModelFactory(private val repository: LogoutRepository): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return LogoutViewModel(repository) as T
    }
}