package com.example.xyrestoadminapp.table

import android.content.ClipData
import android.content.ClipDescription
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.DragEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.example.xyrestoadminapp.R
import com.example.xyrestoadminapp.base.model.Table
import com.example.xyrestoadminapp.base.storage.network.ApiClient
import com.example.xyrestoadminapp.base.utils.ResponseWrapper
import com.example.xyrestoadminapp.base.utils.Utils
import com.example.xyrestoadminapp.base.utils.Utils.orDefaultBool
import com.example.xyrestoadminapp.base.utils.Utils.toInteger
import com.example.xyrestoadminapp.base.widget.DragShadowBuilder
import com.example.xyrestoadminapp.databinding.FragmentEditTableBinding
import com.example.xyrestoadminapp.home.IRetryCallBack
import com.example.xyrestoadminapp.home.LoadingCommunicator
import com.example.xyrestoadminapp.home.LoadingMode
import com.example.xyrestoadminapp.table.network.ITableApiService
import com.example.xyrestoadminapp.table.repository.TableRepository
import com.example.xyrestoadminapp.table.viewmodel.TableViewModel
import com.example.xyrestoadminapp.table.viewmodel.TableViewModelFactory

class EditTableFragment : BaseTableFragment() {

    private var _viewBinding: FragmentEditTableBinding? = null
    private val viewBinding: FragmentEditTableBinding get() = _viewBinding!!
    private val viewModel: TableViewModel by viewModels {
        TableViewModelFactory(
            TableRepository(
            ApiClient.getRetrofitInstance(requireContext()).create(ITableApiService::class.java)
        )
        )
    }
    private val tables: MutableList<Table> = mutableListOf()
    private var nextTableId: Int? = null
    private var gridLayoutWidth: Float = 0.0F
    private var gridLayoutHeight: Float = 0.0F
    private var isTableLayoutUpdated = false
    private lateinit var loadingCommunicator: LoadingCommunicator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.let {
            it.getParcelable<Table>(KEY_SELECTED_TABLE)?.let { table -> selectedTable = table }
            it.getParcelableArrayList<Table>(KEY_TABLES)?.let { tableList -> tables.addAll(tableList) }
            nextTableId = it.getInt(KEY_NEXT_TABLE_ID)
            isTableLayoutUpdated = it.getBoolean(KEY_IS_TABLE_UPDATED)
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, object : OnBackPressedCallback(true){
            override fun handleOnBackPressed() {
                if (isTableLayoutUpdated){
                    findNavController().navigate(
                        EditTableFragmentDirections.navTableFragment(),
                        NavOptions.Builder().setPopUpTo(R.id.nav_table, true).build()
                    )
                } else {
                    findNavController().popBackStack()
                }
            }
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        loadingCommunicator = activity as LoadingCommunicator
        loadingCommunicator.startLoading(1, LoadingMode.PROGRESS_BAR_WITH_BACKGROUND)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _viewBinding = FragmentEditTableBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initObserver()
    }

    private fun initView(){
        with(viewBinding){
            gridTableLayout.viewTreeObserver.addOnGlobalLayoutListener {
                gridLayoutWidth = gridTableLayout.width.toFloat()
                gridLayoutHeight = gridTableLayout.height.toFloat()
            }
            gridLayout = gridTableLayout
            val density = resources.displayMetrics.density
            val dimens = Utils.getScreenDimension(root.context, true)
            if (dimens.first < 600){
                val width = (dimens.first*density).toInt() - (10*density).toInt()
                gridTableLayout.layoutParams = ConstraintLayout.LayoutParams(width, width).apply {
                    startToStart = R.id.cl_container
                    endToEnd = R.id.cl_container
                    topToBottom = R.id.iv_show_hide
                    marginStart = (10*density).toInt()
                    marginEnd = (10*density).toInt()
                    topMargin = (10*density).toInt()
                }
            } else {
                val width = (dimens.first*density*0.65).toInt() - (10*density).toInt()
                val height = (dimens.second*density).toInt() - (resources.getDimension(R.dimen.app_bar_size)).toInt() - (10*density).toInt()
                val dimen = if (width > height) height else width
                gridTableLayout.layoutParams = ConstraintLayout.LayoutParams(dimen, dimen).apply {
                    startToStart = R.id.cl_container
                    endToStart = R.id.guideline
                    topToTop = R.id.cl_container
                    marginStart = (10*density).toInt()
                    marginEnd = (10*density).toInt()
                    topMargin = (5*density).toInt()
                    bottomMargin = (5*density).toInt()
                }
            }
            initGrid()
            btnDeleteTable.setOnClickListener {
                viewBinding.svOption?.visibility = View.GONE
                selectedTable?.let {
                    tables.remove(it)
                    (gridTableLayout[convertXY(it.x,it.y)] as Button).run {
                        text = ""
                        setOnClickListener(null)
                        setOnLongClickListener(null)
                        background = ContextCompat.getDrawable(context, android.R.color.white)
                    }
                    if (it.tableId < nextTableId!!){
                        updateNextTableId(it.tableId)
                    }
                    selectedTable = null
                }
            }
            ivShowHide?.setOnClickListener {
                if (svOption?.isVisible == true)
                    svOption.visibility = View.GONE
                else
                    svOption?.visibility = View.VISIBLE
            }
            btnApprove.setOnClickListener {
                viewBinding.svOption?.visibility = View.GONE
                tables.forEach { table ->
                    table.x++
                    table.y++
                }
                loadingCommunicator.startLoading(1, LoadingMode.ONLY_PROGRESS_BAR)
                viewModel.editTable(tables)
            }
            setDragListener()
            setDragStarting(btnNewTable, true)
            setTableNumber(0)
        }
    }

    private fun initObserver() {
        viewModel.tables.observe(viewLifecycleOwner, {
            when (it.status) {
                ResponseWrapper.Status.LOADING -> {

                }
                ResponseWrapper.Status.SUCCESS -> {
                    with(viewBinding.gridTableLayout) {
                        nextTableId = 1
                        if (tables.isEmpty()){
                            for ((i, data) in it.body?.data!!.withIndex()) {
                                tables.add(i, data.table!!.apply {
                                    if (x > 0 && y > 0){
                                        x--
                                        y--
                                    }
                                    booked = data.booked!!
                                })
                            }
                        }
                        for ((i, data) in tables.withIndex()) {
                            (this[convertXY(tables[i].x,tables[i].y)] as Button).run {
                                text = tables[i].tableId.toString()
                                if (data.booked.orDefaultBool()) {
                                    background = ContextCompat.getDrawable(
                                        context,
                                        R.drawable.button_background_grey
                                    )
                                } else {
                                    isEnabled = true
                                    background = ContextCompat.getDrawable(
                                        context,
                                        R.drawable.button_background_yellow_outlined
                                    )
                                    onTableClicked(this, tables[i])
                                }
                                setDragStarting(this, false, tables[i].booked)
                            }
                        }
                        findNextTableId()
                        updateNextTableId(nextTableId!!)
                        selectedTable?.let { table ->
                            (viewBinding.gridTableLayout[convertXY(table.x, table.y)] as Button).run {
                                setTableButton(this, table, true)
                            }
                        }
                    }
                    loadingCommunicator.stopLoading()
                }
                ResponseWrapper.Status.ERROR -> {
                    loadingCommunicator.onError(it.body?.errors.toString(), listOf(Pair(0, object : IRetryCallBack{
                        override fun retry() {
                            viewModel.callTablesApi()
                        }
                    })))
                }
            }
        })
        viewModel.editTables.observe(viewLifecycleOwner, { event ->
            event.getContentIfNotHandled()?.let {
                when(it.status){
                    ResponseWrapper.Status.LOADING -> {

                    }
                    ResponseWrapper.Status.SUCCESS -> {
                        Toast.makeText(context, "Edit Table Layout Successfully", Toast.LENGTH_LONG).show()
                        isTableLayoutUpdated = true
                        loadingCommunicator.stopLoading()
                    }
                    ResponseWrapper.Status.ERROR -> {
                        loadingCommunicator.onError(it.body?.errors.toString(), listOf(Pair(0, object : IRetryCallBack{
                            override fun retry() {
                                viewModel.editTable(tables)
                            }
                        })))
                    }
                }
            }
        })
    }

    private fun setDragListener(){
        viewBinding.gridTableLayout.setOnDragListener { v, event ->
            when (event.action) {
                DragEvent.ACTION_DRAG_STARTED -> {
                    event.clipDescription.hasMimeType(ClipDescription.MIMETYPE_TEXT_INTENT)
                }
                DragEvent.ACTION_DRAG_ENTERED -> {
                    true
                }

                DragEvent.ACTION_DRAG_LOCATION -> {
                    true
                }

                DragEvent.ACTION_DRAG_EXITED -> {
                    true
                }

                DragEvent.ACTION_DROP -> {
                    val item: ClipData.Item = event.clipData.getItemAt(0)
                    val dragIntent = item.intent
                    val tableId = dragIntent.getStringExtra(KEY_TABLE_ID)
                    val isNewTable = dragIntent.getBooleanExtra(KEY_IS_NEW_TABLE, false)
                    val isBooked = dragIntent.getBooleanExtra(KEY_IS_BOOKED, false)
                    val point = xyScreenToXyGrid(event.x, event.y)
                    if ((viewBinding.gridTableLayout[convertXY(point.first, point.second)] as Button).text == ""){
                        if (isNewTable){
                            tables.add(Table(tableId.toInteger(), point.first, point.second))
                            findNextTableId()
                            updateNextTableId(nextTableId!!)
                            (viewBinding.gridTableLayout[convertXY(point.first,point.second)] as Button).run {
                                text = tableId
                                background = ContextCompat.getDrawable(
                                    context,
                                    R.drawable.button_background_yellow_outlined
                                )
                                onTableClicked(this, tables[tables.indexOfFirst { it.tableId == tableId?.toInt() }])
                                setDragStarting(this, false)
                            }
                        } else {
                            val index = tables.indexOfFirst { it.tableId == tableId?.toInt() }
                            (viewBinding.gridTableLayout[convertXY(tables[index].x, tables[index].y)] as Button).run {
                                text = ""
                                setOnClickListener(null)
                                setOnLongClickListener(null)
                                background = ContextCompat.getDrawable(context, android.R.color.white)
                            }
                            tables[index] = Table(tableId.toInteger(), point.first, point.second, isBooked)
                            (viewBinding.gridTableLayout[convertXY(point.first,point.second)] as Button).run {
                                text = tableId
                                onTableClicked(this, tables[index])
                                setDragStarting(this, false, isBooked)
                                background = if (isBooked)
                                    ContextCompat.getDrawable(context, R.drawable.button_background_grey)
                                else
                                    ContextCompat.getDrawable(context, R.drawable.button_background_yellow_outlined)

                            }
                        }
                    }
                    true
                }

                DragEvent.ACTION_DRAG_ENDED -> {
                    if (!event.result){
                        Toast.makeText(context, "The drop didn't work.", Toast.LENGTH_SHORT).show()
                    }
                    true
                }
                else -> {
                    Log.e("DragDrop Example", "Unknown action type received by OnDragListener.")
                    false
                }
            }
        }
    }

    private fun setDragStarting(button: Button, isNewTable: Boolean, isBooked: Boolean = false){
        button.apply {
            setOnLongClickListener { v: View ->
                if (viewBinding.ivShowHide != null)
                    viewBinding.svOption?.visibility = View.GONE
                val item = ClipData.Item(
                    Intent()
                    .putExtra(KEY_TABLE_ID, text)
                    .putExtra(KEY_IS_NEW_TABLE, isNewTable)
                    .putExtra(KEY_IS_BOOKED, isBooked)
                )
                val dragData = ClipData(
                    KEY_ITEM,
                    arrayOf(ClipDescription.MIMETYPE_TEXT_INTENT),
                    item)
                val myShadow = DragShadowBuilder(this)
                v.startDrag(
                    dragData,   // the data to be dragged
                    myShadow,   // the drag shadow builder
                    null,       // no need to use local data
                    0           // flags (not currently used, set to 0)
                )
            }
        }
    }

    private fun onTableClicked(button: Button, table: Table){
        button.run {
            setOnClickListener {
                selectedTable?.let {
                    if (table.tableId != it.tableId){
                        (viewBinding.gridTableLayout[convertXY(it.x, it.y)] as Button).run {
                            tag = KEY_UNSELECTED
                            background = ContextCompat.getDrawable(
                                context,
                                R.drawable.button_background_yellow_outlined
                            )
                        }
                    }
                }
                if (tag == null || tag == KEY_UNSELECTED){
                    setTableButton(button, table, true)
                } else {
                    setTableButton(button, table, false)
                }
            }
        }
    }

    private fun setTableNumber(tableId: Int){
        viewBinding.tvTableNumber.text = resources.getString(R.string.table_number, tableId)
    }

    private fun setTableButton(button: Button, table: Table, isSelected: Boolean){
        button.run {
            if (isSelected){
                tag = KEY_SELECTED
                selectedTable = table
                background = ContextCompat.getDrawable(
                    context,
                    R.drawable.button_background_yellow
                )
                setTableNumber(table.tableId)
            } else {
                tag = KEY_UNSELECTED
                selectedTable = null
                background = ContextCompat.getDrawable(
                    context,
                    R.drawable.button_background_yellow_outlined
                )
                setTableNumber(0)
            }
        }
    }

    private fun updateNextTableId(id: Int){
        nextTableId = id
        viewBinding.btnNewTable.text = id.toString()
    }

    private fun findNextTableId(){
        nextTableId = 1
        tables.sortWith(compareBy { it.tableId })
        for (table in tables){
            if (nextTableId == null || nextTableId!! >= table.tableId){
                nextTableId = table.tableId.plus(1)
            }
        }
    }

    private fun xyScreenToXyGrid(width: Float, height: Float): Pair<Int, Int>{
        val itemWidth = gridLayoutWidth / viewBinding.gridTableLayout.columnCount.toFloat()
        val itemHeight = gridLayoutHeight / viewBinding.gridTableLayout.rowCount.toFloat()
        var colAxis: Int = (width/itemWidth).toInt() - 1
        var rowAxis: Int = (height/itemHeight).toInt() - 1
        if ((width/itemWidth) - colAxis > 0){
            colAxis++
        }
        if ((height/itemHeight) - rowAxis > 0){
            rowAxis++
        }
        return Pair(colAxis, rowAxis)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelableArrayList(KEY_TABLES, arrayListOf<Table>().apply { addAll(tables) })
        outState.putParcelable(KEY_SELECTED_TABLE, selectedTable)
        nextTableId?.let { outState.putInt(KEY_NEXT_TABLE_ID, it) }
        outState.putBoolean(KEY_IS_TABLE_UPDATED, isTableLayoutUpdated)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _viewBinding = null
    }

    companion object{
        private const val KEY_ITEM = "itemKey"
        private const val KEY_TABLE_ID = "tableId"
        private const val KEY_IS_NEW_TABLE = "isNewTable"
        private const val KEY_IS_BOOKED = "isBooked"
        private const val KEY_SELECTED = "selected"
        private const val KEY_UNSELECTED = "unselected"
        private const val KEY_TABLES = "tables"
        private const val KEY_SELECTED_TABLE = "selectedTable"
        private const val KEY_NEXT_TABLE_ID = "nextTableId"
        private const val KEY_IS_TABLE_UPDATED = "isTableUpdated"
    }
}