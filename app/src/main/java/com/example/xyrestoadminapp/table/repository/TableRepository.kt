package com.example.xyrestoadminapp.table.repository

import com.example.xyrestoadminapp.base.model.Table
import com.example.xyrestoadminapp.table.network.ITableApiService

class TableRepository(private val iTableApiService: ITableApiService) {
    suspend fun getTable() = iTableApiService.getTables()
    suspend fun editTable(tables: List<Table>) = iTableApiService.editTables(tables)
}