package com.example.xyrestoadminapp.table

import android.widget.Button
import androidx.gridlayout.widget.GridLayout
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.fragment.app.Fragment
import com.example.xyrestoadminapp.base.model.Table

open class BaseTableFragment: Fragment() {

    protected lateinit var gridLayout: GridLayout
    protected var selectedTable: Table? = null

    protected fun initGrid() {
        with(gridLayout){
            removeAllViews()
            for (x in 0 until rowCount){
                for (y in 0 until columnCount){
                    addView(Button(context).apply {
                        val param: GridLayout.LayoutParams = GridLayout.LayoutParams(
                            GridLayout.spec(GridLayout.UNDEFINED, 1f),
                            GridLayout.spec(GridLayout.UNDEFINED, 1f)
                        )
                        text = ""
                        param.setMargins(2,2,2,2)
                        layoutParams = param
                        setPadding(6,6,6,6)
                        background = ContextCompat.getDrawable(context, android.R.color.white)
                    })
                }
            }
        }
    }

    protected fun convertXY(horizontal: Int, vertical: Int): Int{
        if (vertical <= 4 && horizontal <= 4 && vertical >=0 && horizontal >= 0){
            if (vertical == 0){
                return horizontal
            }
            return (vertical*gridLayout.columnCount) + horizontal
        } else {
            return 0
        }
    }

    protected fun getSelectedTableView(): Button?{
        selectedTable?.let { return gridLayout[convertXY(it.x, it.y)] as Button }
        return null
    }
}