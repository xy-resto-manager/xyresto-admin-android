package com.example.xyrestoadminapp.table

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.xyrestoadminapp.R
import com.example.xyrestoadminapp.base.storage.network.ApiClient
import com.example.xyrestoadminapp.base.utils.ResponseWrapper
import com.example.xyrestoadminapp.base.utils.Utils.orDefaultBool
import com.example.xyrestoadminapp.databinding.FragmentTableBinding
import com.example.xyrestoadminapp.home.IRetryCallBack
import com.example.xyrestoadminapp.home.LoadingCommunicator
import com.example.xyrestoadminapp.home.LoadingMode
import com.example.xyrestoadminapp.table.network.ITableApiService
import com.example.xyrestoadminapp.table.repository.TableRepository
import com.example.xyrestoadminapp.table.viewmodel.TableViewModel
import com.example.xyrestoadminapp.table.viewmodel.TableViewModelFactory

class TableFragment : BaseTableFragment() {

    private var _viewBinding: FragmentTableBinding? = null
    private val viewBinding: FragmentTableBinding get() = _viewBinding!!
    private val viewModel: TableViewModel by viewModels {
        TableViewModelFactory(TableRepository(
            ApiClient.getRetrofitInstance(requireContext()).create(ITableApiService::class.java)
        ))
    }
    private lateinit var loadingCommunicator: LoadingCommunicator

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        loadingCommunicator = activity as LoadingCommunicator
        loadingCommunicator.startLoading(1, LoadingMode.PROGRESS_BAR_WITH_BACKGROUND)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _viewBinding = FragmentTableBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewBinding.btnEdit.setOnClickListener {
            findNavController().navigate(TableFragmentDirections.editTableFragment())
        }
        gridLayout = viewBinding.gridTableLayout
        initObserver()
    }

    private fun initObserver() {
        viewModel.tables.observe(viewLifecycleOwner, {
            when (it.status) {
                ResponseWrapper.Status.LOADING -> {

                }
                ResponseWrapper.Status.SUCCESS -> {
                    initGrid()
                    with(viewBinding.gridTableLayout) {
                        for (data in it.body?.data!!) {
                            (this[convertXY(data.table?.x!!-1,data.table.y-1)] as Button).run {
                                text = data.table.tableId.toString()
                                if (data.booked.orDefaultBool()) {
                                    isEnabled = false
                                    background = ContextCompat.getDrawable(
                                        context,
                                        R.drawable.button_background_grey
                                    )
                                } else {
                                    isEnabled = true
                                    background = ContextCompat.getDrawable(
                                        context,
                                        R.drawable.button_background_yellow_outlined
                                    )
                                }
                            }
                        }
                    }
                    loadingCommunicator.stopLoading()
                }
                ResponseWrapper.Status.ERROR -> {
                    loadingCommunicator.onError(it.body?.errors.toString(), listOf(Pair(0, object : IRetryCallBack{
                        override fun retry() {
                            viewModel.callTablesApi()
                        }
                    })))
                }
            }
        })
    }

}