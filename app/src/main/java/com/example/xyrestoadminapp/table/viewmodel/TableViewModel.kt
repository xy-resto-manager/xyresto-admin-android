package com.example.xyrestoadminapp.table.viewmodel

import androidx.lifecycle.*
import com.example.xyrestoadminapp.base.model.BaseApiResponse
import com.example.xyrestoadminapp.base.model.Event
import com.example.xyrestoadminapp.base.model.Table
import com.example.xyrestoadminapp.base.model.TableResponse
import com.example.xyrestoadminapp.base.utils.ApiCallHelper.safeApiCall
import com.example.xyrestoadminapp.base.utils.ResponseWrapper
import com.example.xyrestoadminapp.table.repository.TableRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TableViewModel(private val repository: TableRepository): ViewModel() {
    private val _tables = MutableLiveData<ResponseWrapper<BaseApiResponse<List<TableResponse>>>>()
    val tables: LiveData<ResponseWrapper<BaseApiResponse<List<TableResponse>>>> = _tables
    private val _editTables = MutableLiveData<Event<ResponseWrapper<BaseApiResponse<List<Table>>>>>()
    val editTables: LiveData<Event<ResponseWrapper<BaseApiResponse<List<Table>>>>> = _editTables

    init {
        callTablesApi()
    }

    fun callTablesApi() {
        viewModelScope.launch(Dispatchers.IO) {
            val tables = safeApiCall{ repository.getTable() }
            _tables.postValue(tables)
        }
    }

    fun editTable(tables: List<Table>){
        viewModelScope.launch(Dispatchers.IO) {
            _editTables.postValue(Event(ResponseWrapper.loading()))
            _editTables.postValue(Event(safeApiCall { repository.editTable(tables) }))
        }
    }
}