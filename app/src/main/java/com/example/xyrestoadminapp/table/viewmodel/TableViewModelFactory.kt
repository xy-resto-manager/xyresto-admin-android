package com.example.xyrestoadminapp.table.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.xyrestoadminapp.table.repository.TableRepository

class TableViewModelFactory(private val repository: TableRepository): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return TableViewModel(repository) as T
    }
}