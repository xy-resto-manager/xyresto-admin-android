package com.example.xyrestoadminapp.table.network

import com.example.xyrestoadminapp.base.model.BaseApiResponse
import com.example.xyrestoadminapp.base.model.Table
import com.example.xyrestoadminapp.base.model.TableResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PUT

interface ITableApiService {
    @GET("admin/layout/tables")
    suspend fun getTables(): Response<BaseApiResponse<List<TableResponse>>>

    @PUT("admin/layout/tables")
    suspend fun editTables(@Body tables: List<Table>): Response<BaseApiResponse<List<Table>>>
}