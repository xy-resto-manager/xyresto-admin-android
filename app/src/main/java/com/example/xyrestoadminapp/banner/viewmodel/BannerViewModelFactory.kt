package com.example.xyrestoadminapp.banner.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.xyrestoadminapp.banner.repository.BannerRepository

class BannerViewModelFactory(private val repository: BannerRepository): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return BannerViewModel(repository) as T
    }
}