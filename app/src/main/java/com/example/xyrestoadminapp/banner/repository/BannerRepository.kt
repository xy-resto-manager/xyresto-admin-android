package com.example.xyrestoadminapp.banner.repository

import com.example.xyrestoadminapp.banner.network.IBannerApiService
import okhttp3.MultipartBody

class BannerRepository(private val iBannerApiService: IBannerApiService) {
    suspend fun getBanner() = iBannerApiService.getBanner()
    suspend fun createBanner(file: MultipartBody.Part) = iBannerApiService.createBanner(file)
    suspend fun deleteBanner(bannerId: Int) = iBannerApiService.deleteBanner(bannerId)
}