package com.example.xyrestoadminapp.banner.network

import com.example.xyrestoadminapp.banner.model.Banner
import com.example.xyrestoadminapp.base.model.BaseApiResponse
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.*

interface IBannerApiService {
    @GET("banner/banners")
    suspend fun getBanner(): Response<BaseApiResponse<List<Banner>>>

    @Multipart
    @POST("admin/banner/banners")
    suspend fun createBanner(@Part file: MultipartBody.Part): Response<BaseApiResponse<Banner>>

    @DELETE("admin/banner/{bannerId}")
    suspend fun deleteBanner(@Path("bannerId") bannerId: Int): Response<BaseApiResponse<Any>>
}