package com.example.xyrestoadminapp.banner.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.xyrestoadminapp.banner.model.Banner
import com.example.xyrestoadminapp.banner.repository.BannerRepository
import com.example.xyrestoadminapp.base.custome_class.CustomeLiveData
import com.example.xyrestoadminapp.base.model.BaseApiResponse
import com.example.xyrestoadminapp.base.model.Event
import com.example.xyrestoadminapp.base.utils.ApiCallHelper.safeApiCall
import com.example.xyrestoadminapp.base.utils.ResponseWrapper
import com.example.xyrestoadminapp.base.utils.Utils.orDefaultInt
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.MultipartBody

class BannerViewModel(private val repository: BannerRepository): ViewModel() {
    private val _banners = CustomeLiveData<ResponseWrapper<BaseApiResponse<List<Banner>>>>()
    val banners: LiveData<ResponseWrapper<BaseApiResponse<List<Banner>>>> get() = _banners
    private val _addBanner = MutableLiveData<Event<ResponseWrapper<BaseApiResponse<Banner>>>>()
    val addBanner: LiveData<Event<ResponseWrapper<BaseApiResponse<Banner>>>> get() = _addBanner
    private val _deleteBanner = MutableLiveData<Event<ResponseWrapper<BaseApiResponse<Any>>>>()
    val deleteBanner: LiveData<Event<ResponseWrapper<BaseApiResponse<Any>>>> get() = _deleteBanner

    init {
        getBanner()
    }

    fun getBanner(){
        viewModelScope.launch(Dispatchers.IO) {
            _banners.postValue(ResponseWrapper.loading())
            _banners.postValue(safeApiCall { repository.getBanner() })
        }
    }

    fun createBanner(file: MultipartBody.Part){
        viewModelScope.launch(Dispatchers.IO) {
            _addBanner.postValue(Event(ResponseWrapper.loading()))
            _addBanner.postValue(Event(safeApiCall { repository.createBanner(file) }))
        }
    }

    fun deleteBanner(bannerId: Int){
        viewModelScope.launch(Dispatchers.IO) {
            _deleteBanner.postValue(Event(ResponseWrapper.loading()))
            _deleteBanner.postValue(Event(safeApiCall { repository.deleteBanner(bannerId) }))
        }
    }

    fun appendBannerList(newBanner: Banner){
        _banners.value?.let { response ->
            _banners.setValue( ResponseWrapper(
                response.status,
                BaseApiResponse(
                    response.body?.code,
                    response.body?.data?.toMutableList()?.apply { add(0, newBanner) },
                    response.body?.status,
                    response.body?.errors,
                    response.body?.pagination
                ),
                response.message
            ))
        } ?: run {
            _banners.setValue(ResponseWrapper.success(BaseApiResponse(data = listOf(newBanner))))
        }
    }

    fun deleteBannerFromBannerList(index: Int){
        _banners.value?.let { response ->
            if (index >= 0 && index < response.body?.data?.size.orDefaultInt(-1)){
                _banners.setValueWithoutNotify(ResponseWrapper(
                    response.status,
                    BaseApiResponse(
                        response.body?.code,
                        response.body?.data?.toMutableList()?.apply { removeAt(index) },
                        response.body?.status,
                        response.body?.errors,
                        response.body?.pagination
                    ),
                    response.message
                ))
            }
        }
    }
}