package com.example.xyrestoadminapp.banner.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners
import com.example.xyrestoadminapp.R
import com.example.xyrestoadminapp.banner.model.Banner
import com.example.xyrestoadminapp.base.adapter.BaseRvAdapter
import com.example.xyrestoadminapp.base.constant.Constant
import com.example.xyrestoadminapp.databinding.BannersItemBinding

class BannerListAdapter(
    currentPosition: Int = 0,
    private val onBannerClicked: (Banner, Int) -> Unit,
    private val dimens: Pair<Int, Int>,
    private val screenDensity: Float = 1.0F
): BaseRvAdapter<Banner>() {

    private var activePosition: Int = currentPosition

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseRvViewHolder<Banner> {
        val viewBinding = BannersItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return BannerListViewHolder(viewBinding)
    }

    override fun onBindViewHolder(holder: BaseRvViewHolder<Banner>, position: Int) {
        holder.itemView.layoutParams = holder.itemView.layoutParams.apply {
            if (dimens.first < dimens.second || dimens.second >= 600){
                //portrait or height >= 600dp
                width = (dimens.second*screenDensity*0.2).toInt()
                height = FrameLayout.LayoutParams.MATCH_PARENT
            } else {
                //landscape
                width = FrameLayout.LayoutParams.MATCH_PARENT
                height = (dimens.first*screenDensity*0.2).toInt()
            }

        }
        holder.bind(mutableItems[position % mutableItemCount], position)
    }

    fun setActivePosition(position: Int){
        activePosition = position
        notifyDataSetChanged()
    }

    fun getItem(position: Int): Banner?{
        return if (mutableItemCount > 0)
            mutableItems[position]
        else
            null
    }

    inner class BannerListViewHolder(private val viewBinding: BannersItemBinding): BaseRvViewHolder<Banner>(viewBinding.root){
        override fun bind(data: Banner, position: Int?) {
            with(viewBinding){
                root.setOnClickListener {
                    if (activePosition != position){
                        onBannerClicked(data, position!!)
                        activePosition = position
                        notifyDataSetChanged()
                    }
                }
                Glide.with(root.context)
                    .load(Constant.SERVER_URL + data.imageUrl)
                    .transform(CenterCrop(), GranularRoundedCorners(10.0F, 10.0F, 10.0F, 10.0F))
                    .placeholder(R.drawable.ic_food)
                    .error(R.drawable.not_found_text)
                    .into(ivBanner)
                if (position == activePosition){
                    root.background = ContextCompat.getDrawable(root.context, R.drawable.background_outlined_yellow_radius_all)
                } else {
                    root.background = ContextCompat.getDrawable(root.context, R.drawable.background_outlined_transparent)
                }
            }
        }

    }
}