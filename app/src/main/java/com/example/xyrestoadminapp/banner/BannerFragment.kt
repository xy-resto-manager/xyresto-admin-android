package com.example.xyrestoadminapp.banner

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.example.xyrestoadminapp.R
import com.example.xyrestoadminapp.banner.adapter.BannerListAdapter
import com.example.xyrestoadminapp.banner.model.Banner
import com.example.xyrestoadminapp.banner.network.IBannerApiService
import com.example.xyrestoadminapp.banner.repository.BannerRepository
import com.example.xyrestoadminapp.banner.viewmodel.BannerViewModel
import com.example.xyrestoadminapp.banner.viewmodel.BannerViewModelFactory
import com.example.xyrestoadminapp.base.constant.Constant
import com.example.xyrestoadminapp.base.fragment.FragmentWithPermissionChecker
import com.example.xyrestoadminapp.base.storage.network.ApiClient
import com.example.xyrestoadminapp.base.utils.FileUtils
import com.example.xyrestoadminapp.base.utils.ResponseWrapper
import com.example.xyrestoadminapp.base.utils.Utils.getScreenDimension
import com.example.xyrestoadminapp.base.widget.decoration.SpacingItemDecoration
import com.example.xyrestoadminapp.databinding.FragmentBannerBinding
import com.example.xyrestoadminapp.home.IRetryCallBack
import com.example.xyrestoadminapp.home.LoadingCommunicator
import com.example.xyrestoadminapp.home.LoadingMode
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File

private const val PICK_PHOTO = 10

class BannerFragment : FragmentWithPermissionChecker() {
    private var _viewBinding: FragmentBannerBinding? = null
    private val viewBinding: FragmentBannerBinding get() = _viewBinding!!
    private val viewModel: BannerViewModel by viewModels {
        BannerViewModelFactory(BannerRepository(
            ApiClient.getRetrofitInstance(requireContext()).create(IBannerApiService::class.java)
        ))
    }
    private var selectedBannerId: Int = 0
    private var selectedBannerPosition: Int = 0
    private lateinit var screenDimen: Pair<Int, Int>
    private val bannerListAdapter: BannerListAdapter by lazy {
        BannerListAdapter(selectedBannerPosition, ::onBannerClicked, screenDimen, resources.displayMetrics.density)
    }
    private lateinit var loadingCommunicator: LoadingCommunicator
    private val retryCallbackList = mutableListOf<Pair<Int, IRetryCallBack>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.let {
            selectedBannerId = it.getInt(SELECTED_BANNER)
            selectedBannerPosition = it.getInt(SELECTED_BANNER_POSITION)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        loadingCommunicator = activity as LoadingCommunicator
        loadingCommunicator.startLoading(1, LoadingMode.PROGRESS_BAR_WITH_BACKGROUND)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _viewBinding = FragmentBannerBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView(){
        with(viewBinding){
            val dimens = getScreenDimension(root.context, true)
            screenDimen = Pair(dimens.first, dimens.second-resources.getDimension(R.dimen.app_bar_size).toInt())
            btnDelete?.setOnClickListener { viewModel.deleteBanner(selectedBannerId) }
            fbDelete?.setOnClickListener { viewModel.deleteBanner(selectedBannerId) }
            ivAddBanner.setOnClickListener {
                if (checkPermission()){
                    openFileManager()
                }
            }
            rvBanners.run {
                adapter = bannerListAdapter
                addItemDecoration(SpacingItemDecoration(10, 10, 10, 10))
            }
        }
        initObserver()
    }

    private fun initObserver(){
        viewModel.banners.observe(viewLifecycleOwner, {
            retryCallbackList.removeAll { callback -> callback.first == 0 }
            when(it.status){
                ResponseWrapper.Status.LOADING -> {

                }
                ResponseWrapper.Status.SUCCESS -> {
                    it.body?.data?.let { data ->
                        bannerListAdapter.setItemList(data, false)
                        bannerListAdapter.setActivePosition(selectedBannerPosition)
                        onBannerClicked(data[selectedBannerPosition], selectedBannerPosition)
                    }
                    loadingCommunicator.stopLoading()
                }
                ResponseWrapper.Status.ERROR -> {
                    retryCallbackList.add(Pair(0, object : IRetryCallBack{
                        override fun retry() {
                            viewModel.getBanner()
                        }
                    }))
                    loadingCommunicator.onError(it.body?.errors.toString(), retryCallbackList)
                }
            }
        })
        viewModel.addBanner.observe(viewLifecycleOwner, { event ->
            event.getContentIfNotHandled()?.let {
                when(it.status){
                    ResponseWrapper.Status.LOADING -> {

                    }
                    ResponseWrapper.Status.SUCCESS -> {
                        it.body?.data?.let { banner ->
                            onBannerClicked(banner, 0)
                            viewModel.appendBannerList(banner)
                        }
                    }
                    ResponseWrapper.Status.ERROR -> {
                        Toast.makeText(context, "ERROR: "+it.body?.errors.toString(), Toast.LENGTH_LONG).show()
                    }
                }
                loadingCommunicator.stopLoading()
            }
        })
        viewModel.deleteBanner.observe(viewLifecycleOwner, { event ->
            event.getContentIfNotHandled()?.let {
                when(it.status){
                    ResponseWrapper.Status.LOADING -> {

                    }
                    ResponseWrapper.Status.SUCCESS -> {
                        viewModel.deleteBannerFromBannerList(selectedBannerPosition)
                        bannerListAdapter.removeAt(selectedBannerPosition, false)
                        bannerListAdapter.setActivePosition(0)
                        bannerListAdapter.getItem(0)?.let { banner ->
                            onBannerClicked(banner, 0)
                        } ?: run {
                            selectedBannerId = 0
                            onBannerClicked(null, 0)
                        }
                        Toast.makeText(context, "DELETE BANNER SUCCESSFULLY", Toast.LENGTH_LONG).show()
                    }
                    ResponseWrapper.Status.ERROR -> {
                        Toast.makeText(context, "ERROR: "+it.body?.errors.toString(), Toast.LENGTH_LONG).show()
                    }
                }
            }
        })
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(SELECTED_BANNER, selectedBannerId)
        outState.putInt(SELECTED_BANNER_POSITION, selectedBannerPosition)
    }

    private fun onBannerClicked(data: Banner?, position: Int){
        data?.id?.let { selectedBannerId = it }
        selectedBannerPosition = position
        data?.imageUrl?.let { setImageIntoImageView(it) }
    }

    private fun openFileManager(){
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "image/*"
        startActivityForResult(intent, PICK_PHOTO)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when(requestCode){
            Constant.PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    openFileManager()
                } else {
                    Toast.makeText(requireContext(), "CANNOT OPEN FILE MANAGER", Toast.LENGTH_LONG).show()
                }
            }
            else -> {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_PHOTO && resultCode == Activity.RESULT_OK){
            if (data != null){
                data.data?.let {
                    FileUtils.getPath(it, requireContext())?.let { path ->
                        val file = File(path)
                        val requestFile = file.asRequestBody(("image/jpeg").toMediaTypeOrNull())
                        val body = MultipartBody.Part.createFormData(
                            "file",
                            file.name,
                            requestFile
                        )
                        loadingCommunicator.startLoading(1, LoadingMode.PROGRESS_BAR_WITH_BACKGROUND)
                        viewModel.createBanner(body)
                    }
                }
            }
        }
    }

    private fun setImageIntoImageView(url: String){
        with(viewBinding){
            ivBannerBig.layout(0,0,0,0)
            Glide.with(root.context).load(Constant.SERVER_URL + url)
                .centerCrop()
                .placeholder(R.drawable.ic_food)
                .error(R.drawable.not_found)
                .into(viewBinding.ivBannerBig)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _viewBinding = null
    }

    companion object{
        private const val SELECTED_BANNER = "selected_banner_key"
        private const val SELECTED_BANNER_POSITION = "selectedBannerPosition"
    }
}