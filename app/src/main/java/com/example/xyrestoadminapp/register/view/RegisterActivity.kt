package com.example.xyrestoadminapp.register.view

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.method.PasswordTransformationMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.Toast
import com.example.xyrestoadminapp.R
import com.example.xyrestoadminapp.base.utils.ResponseWrapper
import com.example.xyrestoadminapp.base.utils.Utils
import com.example.xyrestoadminapp.base.utils.Utils.parseError
import com.example.xyrestoadminapp.databinding.ActivityRegisterBinding
import com.example.xyrestoadminapp.login.LoginActivity
import com.example.xyrestoadminapp.register.model.RegisterRequest
import com.example.xyrestoadminapp.register.repository.RegisterRepository
import com.example.xyrestoadminapp.register.viewmodel.RegisterViewModel
import com.google.gson.JsonArray

class RegisterActivity : AppCompatActivity() {

    private lateinit var viewBinding: ActivityRegisterBinding
    private val registerViewModel: RegisterViewModel by lazy { RegisterViewModel(RegisterRepository(this)) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)
        initView()
        initObserver()
    }

    private fun initView(){
        with(viewBinding){
            layoutFieldName.tvLabel.text = resources.getString(R.string.label_name)
            layoutFieldEmail.tvLabel.text = resources.getString(R.string.label_email)
            layoutFieldPassword.tvLabel.text = resources.getString(R.string.label_password)
            layoutFieldConfirmPassword.tvLabel.text = resources.getString(R.string.label_confirm_password)
            layoutFieldPassword.etInput.transformationMethod = PasswordTransformationMethod.getInstance()
            layoutFieldConfirmPassword.etInput.transformationMethod = PasswordTransformationMethod.getInstance()

            btnRegister.setOnClickListener {
                callRegisterApi()
            }

            val spannableString = SpannableString(resources.getString(R.string.to_login))
            val clickableSpan: ClickableSpan = object : ClickableSpan() {
                override fun onClick(widget: View) {
                    onBackPressed()
                }

                override fun updateDrawState(ds: TextPaint) {
                    super.updateDrawState(ds)
                    ds.color = Utils.getThemeAttribute(R.attr.themeContentTextColorAccent, root.context)
                    ds.isUnderlineText = false
                    ds.isFakeBoldText = true
                }
            }
            spannableString.setSpan(clickableSpan,
                resources.getInteger(R.integer.login_span_start),
                resources.getInteger(R.integer.login_span_end),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            tvToLogin.run {
                text = spannableString
                highlightColor = Color.TRANSPARENT
                movementMethod = LinkMovementMethod.getInstance()
            }
        }
    }

    private fun initObserver(){
        registerViewModel.registerResponse.observe(this, {
            when(it.status){
                ResponseWrapper.Status.LOADING -> {
                    Log.d("LOADING", "YES")
                }
                ResponseWrapper.Status.SUCCESS -> {
                    Log.d("DATA", it.body?.data.toString())
                    redirectToLogin(it.body?.data?.email.orEmpty())
                }
                ResponseWrapper.Status.ERROR -> {
                    val errorStr = (it.body?.errors as JsonArray).get(0).toString()
                    Toast.makeText(this, parseError(errorStr).message.toString(), Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun callRegisterApi(){
        val fullName = viewBinding.layoutFieldName.etInput.text.toString()
        val email = viewBinding.layoutFieldEmail.etInput.text.toString().trim()
        val password = viewBinding.layoutFieldPassword.etInput.text.toString()
        val confirmPassword = viewBinding.layoutFieldConfirmPassword.etInput.text.toString()
        val registerRequest = RegisterRequest(fullName=fullName, email = email, password = password)
        if (fullName.isEmpty() || email.isEmpty() || password.isEmpty()){
            Toast.makeText(this, resources.getString(R.string.all_field_must_be_filled), Toast.LENGTH_SHORT).show()
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            Toast.makeText(this, resources.getString(R.string.wrong_email_format), Toast.LENGTH_SHORT).show()
        } else if (password != confirmPassword){
            Toast.makeText(this, resources.getString(R.string.password_not_matched), Toast.LENGTH_SHORT).show()
        } else {
            registerViewModel.callRegisterApi(registerRequest)
        }
    }

    private fun redirectToLogin(email: String){
        val intent = Intent(this, LoginActivity::class.java)
        intent.putExtra(LoginActivity.EMAIL, email)
        startActivity(intent)
    }
}