package com.example.xyrestoadminapp.register.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.xyrestoadminapp.base.model.BaseApiResponse
import com.example.xyrestoadminapp.base.utils.ApiCallHelper.safeApiCall
import com.example.xyrestoadminapp.base.utils.ResponseWrapper
import com.example.xyrestoadminapp.register.model.RegisterRequest
import com.example.xyrestoadminapp.register.model.RegisterResponse
import com.example.xyrestoadminapp.register.repository.RegisterRepository
import kotlinx.coroutines.launch

class RegisterViewModel(private val registerRepository: RegisterRepository): ViewModel() {
    private val _registerResponse = MutableLiveData<ResponseWrapper<BaseApiResponse<RegisterResponse>>>()
    val registerResponse: LiveData<ResponseWrapper<BaseApiResponse<RegisterResponse>>>
        get() = _registerResponse

    fun callRegisterApi(registerRequest: RegisterRequest) {
        viewModelScope.launch {
            _registerResponse.postValue(ResponseWrapper.loading())
            val result = safeApiCall { registerRepository.callRegisterApi(registerRequest) }
            _registerResponse.postValue(result)
        }
    }
}