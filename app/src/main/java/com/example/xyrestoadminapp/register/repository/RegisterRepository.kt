package com.example.xyrestoadminapp.register.repository

import android.content.Context
import com.example.xyrestoadminapp.base.storage.network.ApiClient
import com.example.xyrestoadminapp.register.model.RegisterRequest
import com.example.xyrestoadminapp.register.network.IRegisterApiService

class RegisterRepository(context: Context) {
    private val iRegisterApiService: IRegisterApiService = ApiClient.getRetrofitInstance(context).create(
        IRegisterApiService::class.java)

    suspend fun callRegisterApi(registerRequest: RegisterRequest) = iRegisterApiService.register(registerRequest)
}