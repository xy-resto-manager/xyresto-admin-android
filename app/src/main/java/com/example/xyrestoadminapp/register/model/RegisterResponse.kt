package com.example.xyrestoadminapp.register.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RegisterResponse(
    @field:SerializedName("id")
    val id: Int? = null,
    @field:SerializedName("fullName")
    val fullName: String? = null,
    @field:SerializedName("email")
    val email: String? = null,
    @field:SerializedName("roles")
    val roles: List<String>? = null
): Parcelable
