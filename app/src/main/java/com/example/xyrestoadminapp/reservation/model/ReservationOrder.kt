package com.example.xyrestoadminapp.reservation.model

import android.os.Parcelable
import com.example.xyrestoadminapp.base.model.Product
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ReservationOrder(
    @SerializedName("product")
    val product: Product? = null,
    @SerializedName("quantity")
    val quantity: Int? = null,
    @SerializedName("served")
    val served: Boolean? = null
): Parcelable
