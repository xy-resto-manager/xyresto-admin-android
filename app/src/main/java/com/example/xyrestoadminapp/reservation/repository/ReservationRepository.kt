package com.example.xyrestoadminapp.reservation.repository

import com.example.xyrestoadminapp.reservation.model.EditReservationStatusRequest
import com.example.xyrestoadminapp.reservation.model.EditServingStatus
import com.example.xyrestoadminapp.reservation.model.ReservationRequest
import com.example.xyrestoadminapp.reservation.network.ReservationApiService

class ReservationRepository(private val reservationApiService: ReservationApiService) {
    suspend fun getReservations(status: String, page: Int) = reservationApiService.getReservation(status, page)
    suspend fun getReservationDetail(id: Int) = reservationApiService.getReservationDetail(id)
    suspend fun editReservationStatus(id: Int, reservationStatus: EditReservationStatusRequest) = reservationApiService.editReservationStatus(id, reservationStatus)
    suspend fun editServingStatus(reservationId: Int, servingStatus: EditServingStatus) = reservationApiService.editServingStatus(reservationId, servingStatus)
    suspend fun getTables(timestamp: Long) = reservationApiService.getTables(timestamp)
    suspend fun createReservation(reservationRequest: ReservationRequest) = reservationApiService.createReservation(reservationRequest)
}