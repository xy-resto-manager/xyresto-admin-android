package com.example.xyrestoadminapp.reservation.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners
import com.example.xyrestoadminapp.R
import com.example.xyrestoadminapp.base.adapter.BaseRvAdapter
import com.example.xyrestoadminapp.base.constant.Constant
import com.example.xyrestoadminapp.base.utils.Utils.orDefaultBool
import com.example.xyrestoadminapp.databinding.ReservationDetailListItemBinding
import com.example.xyrestoadminapp.reservation.model.ReservationOrder

class ReservationDetailAdapter(private val iChangeServingStatus: IChangeServingStatus? = null): BaseRvAdapter<ReservationOrder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseRvViewHolder<ReservationOrder> {
        val viewBinding = ReservationDetailListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ReservationDetailViewHolder(viewBinding)
    }

    override fun onBindViewHolder(holder: BaseRvViewHolder<ReservationOrder>, position: Int) {
        mutableItems[position % mutableItemCount].let(holder::bind)
    }

    inner class ReservationDetailViewHolder(
        private val viewBinding: ReservationDetailListItemBinding
    ): BaseRvViewHolder<ReservationOrder>(viewBinding.root){
        override fun bind(data: ReservationOrder, position: Int?) {
            with(viewBinding){
                tvQuantity.text = root.context.resources.getString(R.string.quantity_x, data.quantity)
                tvProductName.text = data.product?.name
                Glide.with(root.context)
                    .load(Constant.SERVER_URL +data.product?.imageUrl)
                    .transform(CenterCrop(), GranularRoundedCorners(10.0F, 10.0F, 10.0F, 10.0F))
                    .placeholder(R.drawable.ic_food)
                    .into(ivProduct)
                when {
                    iChangeServingStatus == null -> {
                        tvServingStatus.visibility = View.GONE
                    }
                    data.served.orDefaultBool(false) -> {
                        tvServingStatus.text = root.context.getString(R.string.served)
                        tvServingStatus.setBackgroundColor(ContextCompat.getColor(root.context, R.color.colorGreenDark_3E8606))
                        tvServingStatus.setOnClickListener(null)
                    }
                    else -> {
                        tvServingStatus.text = root.context.getString(R.string.mark_as_served)
                        tvServingStatus.setBackgroundColor(ContextCompat.getColor(root.context, R.color.colorYellowDark_E4C104))
                        tvServingStatus.setOnClickListener{ data.product?.id?.let { iChangeServingStatus.changeServingStatus(it) } }
                    }
                }
            }
        }

    }
}