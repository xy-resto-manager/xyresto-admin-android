package com.example.xyrestoadminapp.reservation.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.xyrestoadminapp.base.constant.Constant.STATUS_APPROVED
import com.example.xyrestoadminapp.base.constant.Constant.STATUS_DECLINED
import com.example.xyrestoadminapp.base.constant.Constant.STATUS_PENDING
import com.example.xyrestoadminapp.reservation.ReservationContentFragment

class ReservationViewPagerAdapter(fragment: Fragment): FragmentStateAdapter(fragment) {

    private val fragmentList = mutableListOf<Fragment>(
        ReservationContentFragment.newInstance(STATUS_PENDING),
        ReservationContentFragment.newInstance(STATUS_APPROVED),
        ReservationContentFragment.newInstance(STATUS_DECLINED)
    )
    private var fragmentIds = fragmentList.map { it.hashCode().toLong() }

    override fun getItemCount(): Int {
        return fragmentList.size
    }

    override fun createFragment(position: Int): Fragment {
        return fragmentList[position]
    }

    override fun getItemId(position: Int): Long {
        return fragmentList[position].hashCode().toLong()
    }

    override fun containsItem(itemId: Long): Boolean {
        return fragmentIds.contains(itemId)
    }

    fun changeFragment(position: Int, mFragment: Fragment){
        fragmentList.removeAt(position)
        fragmentList.add(position, mFragment)
        fragmentIds = fragmentList.map { it.hashCode().toLong() }
        notifyItemChanged(position)
    }

    fun getCurrentItem(position: Int): Fragment{
        return fragmentList[position]
    }
}