package com.example.xyrestoadminapp.reservation.network

import com.example.xyrestoadminapp.base.model.BaseApiResponse
import com.example.xyrestoadminapp.reservation.model.*
import retrofit2.Response
import retrofit2.http.*

interface ReservationApiService {
    @GET("admin/reservation/reservations")
    suspend fun getReservation(@Query("status") status: String, @Query("page") page: Int): Response<BaseApiResponse<List<Reservation>>>

    @GET("reservation/reservations/{reservationId}")
    suspend fun getReservationDetail(@Path("reservationId") reservationId: Int): Response<BaseApiResponse<Reservation>>

    @PUT("admin/reservation/reservations/{reservationId}")
    suspend fun editReservationStatus(@Path("reservationId") reservationId: Int, @Body reservationStatus: EditReservationStatusRequest): Response<BaseApiResponse<Any>>

    @PUT("admin/reservation/reservations/{reservationId}/orders")
    suspend fun editServingStatus(@Path("reservationId") reservationId: Int, @Body servingStatus: EditServingStatus): Response<BaseApiResponse<Any>>

    @GET("layout/tables")
    suspend fun getTables(@Query("timestamp") timestamp: Long): Response<BaseApiResponse<List<TableResponse>>>

    @POST("reservation/reservations")
    suspend fun createReservation(@Body reservationRequest: ReservationRequest): Response<BaseApiResponse<ReservationResponse>>
}