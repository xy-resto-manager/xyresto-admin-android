package com.example.xyrestoadminapp.reservation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.xyrestoadminapp.base.constant.Constant.STATUS_APPROVED
import com.example.xyrestoadminapp.base.constant.Constant.STATUS_DECLINED
import com.example.xyrestoadminapp.base.constant.Constant.STATUS_PENDING
import com.example.xyrestoadminapp.base.listener.PaginationListener
import com.example.xyrestoadminapp.base.model.PaginationState
import com.example.xyrestoadminapp.base.storage.network.ApiClient
import com.example.xyrestoadminapp.base.utils.ResponseWrapper
import com.example.xyrestoadminapp.base.utils.Utils
import com.example.xyrestoadminapp.base.utils.Utils.getScreenDimension
import com.example.xyrestoadminapp.base.utils.Utils.orDefaultInt
import com.example.xyrestoadminapp.base.widget.decoration.SpacingItemDecoration
import com.example.xyrestoadminapp.databinding.FragmentReservationContentBinding
import com.example.xyrestoadminapp.home.IRetryCallBack
import com.example.xyrestoadminapp.home.LoadingCommunicator
import com.example.xyrestoadminapp.home.LoadingMode
import com.example.xyrestoadminapp.reservation.adapter.IViewPagerManager
import com.example.xyrestoadminapp.reservation.adapter.ReservationRvAdapter
import com.example.xyrestoadminapp.reservation.model.Reservation
import com.example.xyrestoadminapp.reservation.network.ReservationApiService
import com.example.xyrestoadminapp.reservation.repository.ReservationRepository
import com.example.xyrestoadminapp.reservation.viewmodel.ReservationViewModel
import com.example.xyrestoadminapp.reservation.viewmodel.ReservationViewModelFactory

const val ARG_STATUS_RESERVATION = "reservation_status"
private const val PAGINATION_STATE = "pagination_state"

class ReservationContentFragment: Fragment(){

    private var _viewBinding: FragmentReservationContentBinding? = null
    private val viewBinding: FragmentReservationContentBinding get() = _viewBinding!!
    private var reservationStatus: String = STATUS_DECLINED
    private val reservationAdapter by lazy { ReservationRvAdapter(::onReservationInfoClicked) }
    private val viewModel: ReservationViewModel by viewModels { ReservationViewModelFactory(
        reservationStatus,
        ReservationRepository(
            ApiClient.getRetrofitInstance(requireContext()).create(ReservationApiService::class.java)
        )
    ) }
    private val paginationState = PaginationState()
    private lateinit var loadingCommunicator: LoadingCommunicator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.getString(ARG_STATUS_RESERVATION)?.let { reservationStatus = it }
        savedInstanceState?.getParcelable<PaginationState>(PAGINATION_STATE)?.let {
            paginationState.totalPage = it.totalPage
            paginationState.currentPage = it.currentPage
            paginationState.isLastPage = it.isLastPage
            paginationState.isLoading = it.isLoading
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        loadingCommunicator = activity as LoadingCommunicator
        loadingCommunicator.startLoading(1, LoadingMode.PROGRESS_BAR_WITH_BACKGROUND)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _viewBinding = FragmentReservationContentBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initObserver()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(PAGINATION_STATE, paginationState)
    }

    private fun initView(){
        initRecyclerView()
        setPaginationOnReservationList()
    }

    private fun initRecyclerView(){
        viewBinding.rvReservations.run {
            val width = getScreenDimension(context, true).first
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = reservationAdapter.apply {
                if (width < 600)
                    addItemDecoration(SpacingItemDecoration(leftMargin = 4, topMargin = 10, rightMargin = 4, bottomMargin = 10))
                else
                    addItemDecoration(SpacingItemDecoration(leftMargin = 10, topMargin = 15, rightMargin = 10, bottomMargin = 15))
            }
        }
    }

    private fun setPaginationOnReservationList(){
        with(viewBinding.rvReservations){
            layoutManager?.let {
                addOnScrollListener(object: PaginationListener(it, 10){
                    override fun loadMoreItems() {
                        paginationState.isLoading = true
                        viewModel.getReservation(status = reservationStatus, page = paginationState.currentPage+1)
                    }

                    override fun isLastPage(): Boolean {
                        return paginationState.isLastPage
                    }

                    override fun isLoading(): Boolean {
                        return paginationState.isLoading
                    }

                })
            }
        }
    }

    private fun initObserver(){
        viewModel.reservations.observe(viewLifecycleOwner, {
            when(it.status){
                ResponseWrapper.Status.LOADING -> {

                }
                ResponseWrapper.Status.SUCCESS -> {
                    paginationState.run {
                        it.body?.pagination?.let { paging ->
                            currentPage = paging.currentPage
                            totalPage = Utils.ceilInt(
                                paging.totalItems.orDefaultInt(1),
                                paging.itemsPerPage.orDefaultInt(1)
                            )
                        }
                        when {
                            it.body?.data?.size.orDefaultInt(0) <= 0 -> {
                                viewBinding.tvEmptyOrder.visibility = View.VISIBLE
                            }
                            currentPage < totalPage -> {
                                viewBinding.tvEmptyOrder.visibility = View.GONE
                                it.body?.data?.let { productList -> reservationAdapter.setItemList(productList, false) }
                                reservationAdapter.addFooter(Reservation(), false)
                                reservationAdapter.notifyDataSetChanged()
                            }
                            else -> {
                                viewBinding.tvEmptyOrder.visibility = View.GONE
                                isLastPage = true
                                reservationAdapter.removeFooter(false)
                                it.body?.data?.let { productList -> reservationAdapter.setItemList(productList) }
                            }
                        }
                    }
                    loadingCommunicator.stopLoading()
                }
                ResponseWrapper.Status.ERROR -> {
                    loadingCommunicator.onError(it.body?.errors.toString(), listOf(Pair(0, object : IRetryCallBack{
                        override fun retry() {
                            viewModel.getReservation(reservationStatus, paginationState.currentPage+1)
                        }
                    })))
                }
            }
            paginationState.isLoading = false
        })
    }

    private fun onReservationInfoClicked(data: Reservation){
        val iViewPagerManager: IViewPagerManager = parentFragment as ReservationsFragment
        when(reservationStatus){
            STATUS_PENDING -> {
                iViewPagerManager.changeFragment(0, ReservationDetailFragment.newInstance(data.id!!))
            }
            STATUS_APPROVED -> {
                iViewPagerManager.changeFragment(1, ReservationDetailFragment.newInstance(data.id!!))
            }
            STATUS_DECLINED -> {
                iViewPagerManager.changeFragment(2, ReservationDetailFragment.newInstance(data.id!!))
            }
        }
        iViewPagerManager.setReservationId(data.id!!)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _viewBinding = null
    }

    companion object{
        fun newInstance(status: String): ReservationContentFragment{
            return ReservationContentFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_STATUS_RESERVATION, status)
                }
            }
        }
    }
}