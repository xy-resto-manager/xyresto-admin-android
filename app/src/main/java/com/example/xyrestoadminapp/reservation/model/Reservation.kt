package com.example.xyrestoadminapp.reservation.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Reservation(
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("timestamp")
    val timestamp: Long? = null,
    @SerializedName("email")
    val email: String? = null,
    @SerializedName("fullName")
    val fullName: String? = null,
    @SerializedName("tableId")
    val tableId: Int? = null,
    @SerializedName("status")
    val status: String? = null,
    @SerializedName("orders")
    val orders: List<ReservationOrder>? = null,
    var totalCost: Int = 0
): Parcelable
