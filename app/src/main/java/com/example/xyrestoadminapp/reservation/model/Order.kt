package com.example.xyrestoadminapp.reservation.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Order(
    @SerializedName("productId")
    val productId: Int? = null,
    @SerializedName("quantity")
    val quantity: Int? = null
): Parcelable
