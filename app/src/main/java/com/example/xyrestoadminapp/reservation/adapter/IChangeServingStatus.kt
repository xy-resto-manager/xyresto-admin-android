package com.example.xyrestoadminapp.reservation.adapter

interface IChangeServingStatus {
    fun changeServingStatus(productId: Int)
}