package com.example.xyrestoadminapp.reservation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.xyrestoadminapp.R
import com.example.xyrestoadminapp.base.adapter.FooterAdapter
import com.example.xyrestoadminapp.base.adapter.LoadingHolder
import com.example.xyrestoadminapp.base.utils.Utils
import com.example.xyrestoadminapp.databinding.ReservationsItemBinding
import com.example.xyrestoadminapp.reservation.model.Reservation

class ReservationRvAdapter(
    private val onReservationInfoClicked: (Reservation) -> Unit
): FooterAdapter<Reservation>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseRvViewHolder<Reservation> {
        when(viewType){
            VIEW_TYPE_CONTENT -> {
                val viewBinding = ReservationsItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                return ReservationViewHolder(viewBinding)
            }
            else -> {
                LayoutInflater.from(parent.context).inflate(R.layout.loading_item, parent, false)
                    .let {
                        return LoadingHolder(it)
                    }
            }
        }
    }

    override fun onBindViewHolder(holder: BaseRvViewHolder<Reservation>, position: Int) {
        mutableItems[position % mutableItemCount].let(holder::bind)
    }

    override fun getItemViewType(position: Int): Int {
        return if (isFooterVisible && position == mutableItemCount-1){
            VIEW_TYPE_FOOTER
        } else {
            VIEW_TYPE_CONTENT
        }
    }

    inner class ReservationViewHolder(
        private val viewBinding: ReservationsItemBinding
    ): BaseRvViewHolder<Reservation>(viewBinding.root){
        override fun bind(data: Reservation, position: Int?) {
            with(viewBinding) {
                tvReservationId.text = data.id.toString()
                data.timestamp?.let {
                    tvDate.text = root.context.getString(R.string.reservation_date, Utils.timeStampToDate(it))
                    tvTime.text = root.context.getString(
                        R.string.reservation_time,
                        Utils.rangeTime(it)
                    )
                } ?: run {
                    tvDate.text = root.context.getString(R.string.reservation_date, "-")
                    tvTime.text = root.context.getString(R.string.reservation_time, "-")
                }
                tvTableNumberValue.text = data.tableId.toString()
                ivInfo.setOnClickListener { onReservationInfoClicked(data) }
            }
        }

    }
}