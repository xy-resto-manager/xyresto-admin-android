package com.example.xyrestoadminapp.reservation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import com.example.xyrestoadminapp.R
import com.example.xyrestoadminapp.base.model.DateHolder
import com.example.xyrestoadminapp.base.model.Table
import com.example.xyrestoadminapp.base.storage.network.ApiClient
import com.example.xyrestoadminapp.base.utils.ResponseWrapper
import com.example.xyrestoadminapp.base.utils.Utils
import com.example.xyrestoadminapp.base.utils.Utils.formatTime
import com.example.xyrestoadminapp.base.utils.Utils.orDefaultBool
import com.example.xyrestoadminapp.databinding.FragmentReservationFormBinding
import com.example.xyrestoadminapp.home.IRetryCallBack
import com.example.xyrestoadminapp.home.LoadingCommunicator
import com.example.xyrestoadminapp.home.LoadingMode
import com.example.xyrestoadminapp.reservation.model.ReservationRequest
import com.example.xyrestoadminapp.reservation.network.ReservationApiService
import com.example.xyrestoadminapp.reservation.repository.ReservationRepository
import com.example.xyrestoadminapp.reservation.viewmodel.ReservationFormViewModel
import com.example.xyrestoadminapp.reservation.viewmodel.ReservationFormViewModelFactory
import com.example.xyrestoadminapp.table.BaseTableFragment

private const val ARG_SELECTED_DATE = "selected_date"
private const val ARG_SELECTED_TABLE = "selected_table"

class ReservationFormFragment : BaseTableFragment() {

    private var _viewBinding: FragmentReservationFormBinding? = null
    private val viewBinding: FragmentReservationFormBinding get() = _viewBinding!!
    private val viewModel: ReservationFormViewModel by viewModels {
        ReservationFormViewModelFactory(
            ReservationRepository(
                ApiClient.getRetrofitInstance(requireContext()).create(
                    ReservationApiService::class.java
                )
            )
        )
    }
    private lateinit var selectedDate: DateHolder
    private val iLoadingCommunicator: LoadingCommunicator by lazy { activity as LoadingCommunicator }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.let {
            selectedTable = it.getParcelable(ARG_SELECTED_TABLE)
            it.getParcelable<DateHolder>(ARG_SELECTED_DATE)?.let { date ->
                selectedDate = date
            }
        } ?: run {
            initDate()
            viewModel.callTablesApi(getTimeStampSelectedDate())
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _viewBinding = FragmentReservationFormBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        gridLayout = viewBinding.gridTableLayout
        initView()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(ARG_SELECTED_DATE, selectedDate)
        selectedTable?.let { outState.putParcelable(ARG_SELECTED_TABLE, it) }
    }

    private fun initView(){
        with(viewBinding){
            ArrayAdapter.createFromResource(
                requireContext(),
                R.array.reservation_time,
                android.R.layout.simple_spinner_dropdown_item
            ).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinner.adapter = adapter
            }
            selectedTable?.let {
                tvTableNumber.text = resources.getString(R.string.table_number_str, it.tableId.toString())
            } ?: run {
                tvTableNumber.text = resources.getString(R.string.table_number_str, "-")
            }
            tvTableCapacity.text = resources.getQuantityString(R.plurals.table_capacity, 4, 4)
            selectedDate.run {
                etDatepicker.setText(Utils.formatDate(year, month-1, dayOfMonth))
                calendarView.setDate(getTimeStampSelectedDate(), true, true)
                val index = resources.getStringArray(R.array.reservation_time).indexOfFirst { it.split("-")[0].contains(hourOfDay.formatTime()) }
                spinner.setSelection(index)
            }
            etDatepicker.setOnClickListener {
                if (cvDatePicker.isVisible){
                    cvDatePicker.visibility = View.GONE
                } else {
                    cvDatePicker.visibility = View.VISIBLE
                }
            }
            spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (cvDatePicker.isVisible)
                        cvDatePicker.visibility = View.GONE
                    selectedDate.hourOfDay = getTimeFromArray(position).split("-")[0].split(":")[0].formatTime()
                    iLoadingCommunicator.startLoading(1, LoadingMode.ONLY_PROGRESS_BAR)
                    viewModel.callTablesApi(getTimeStampSelectedDate())
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                    //nothing to do
                }

            }
            calendarView.setOnDateChangeListener { view, year, month, dayOfMonth ->
                selectedDate.let {
                    it.year = year
                    it.month = month + 1
                    it.dayOfMonth = dayOfMonth
                }
                cvDatePicker.visibility = View.GONE
                etDatepicker.setText(Utils.formatDate(year, month, dayOfMonth))
                iLoadingCommunicator.startLoading(1, LoadingMode.ONLY_PROGRESS_BAR)
                viewModel.callTablesApi(getTimeStampSelectedDate())
            }
            btnReserve.setOnClickListener {
                selectedTable?.let { table ->
                    val reservationRequest = ReservationRequest(
                        timestamp = getTimeStampSelectedDate(),
                        tableId = table.tableId,
                        orders = null
                    )
                    iLoadingCommunicator.startLoading(1, LoadingMode.ONLY_PROGRESS_BAR)
                    viewModel.createReservation(reservationRequest)
                } ?: run {
                    Toast.makeText(
                        context,
                        resources.getString(R.string.you_must_select_table),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
        initObserver()
    }

    private fun initDate(){
        val dates = Utils.getSeparateDate()
        selectedDate = DateHolder().apply {
            dayOfMonth = dates[0]
            month = dates[1]
            year = dates[2]
            hourOfDay = getTimeFromArray(0).split("-")[0].split(":")[0].formatTime()
            minute = 0
        }
    }

    private fun getTimeStampSelectedDate(): Long{
        val strDate: String = selectedDate.run {
            dayOfMonth.toString()+"-"+month+"-"+year+" "+hourOfDay.formatTime()+":"+minute.formatTime()
        }
        return Utils.getTimeStampFormat(strDate, "dd-MM-yyyy kk:mm")
    }

    private fun initObserver(){
        viewModel.tables.observe(viewLifecycleOwner, {
            when (it.status) {
                ResponseWrapper.Status.LOADING -> {

                }
                ResponseWrapper.Status.SUCCESS -> {
                    initGrid()
                    with(viewBinding.gridTableLayout) {
                        for (data in it.body?.data!!) {
                            (this[convertXY(data.table?.x!!-1,data.table.y!!-1)] as Button).run {
                                text = data.table.tableId.toString()
                                if (data.booked.orDefaultBool()) {
                                    isEnabled = false
                                    changeButtonBackground(this, 2)
                                } else {
                                    isEnabled = true
                                    if (selectedTable?.tableId == data.table.tableId){
                                        changeButtonBackground(this, 1)
                                    } else {
                                        changeButtonBackground(this, 0)
                                    }
                                    setOnClickListener {
                                        if (selectedTable?.tableId != data.table.tableId) {
                                            getSelectedTableView()?.let { prevBtn ->
                                                changeButtonBackground(prevBtn, 0)
                                            }
                                            selectedTable = data.run { Table(table?.tableId!!, table.x!!-1, table.y!!-1, booked.orDefaultBool()) }
                                            changeButtonBackground(this, 1)
                                            viewBinding.tvTableNumber.text = resources.getString(
                                                R.string.table_number,
                                                selectedTable?.tableId
                                            )
                                        } else {
                                            changeButtonBackground(this, 0)
                                            viewBinding.tvTableNumber.text = resources.getString(
                                                R.string.table_number_str,
                                                "-"
                                            )
                                            selectedTable = null
                                        }
                                    }
                                }
                            }
                        }
                    }
                    iLoadingCommunicator.stopLoading()
                }
                ResponseWrapper.Status.ERROR -> {
                    iLoadingCommunicator.onError(it.body?.errors.toString(), listOf(Pair(0, object : IRetryCallBack{
                        override fun retry() {
                            viewModel.callTablesApi(getTimeStampSelectedDate())
                        }
                    })))
                }
            }
        })
        viewModel.reservationResponse.observe(viewLifecycleOwner, {
            when (it.status) {
                ResponseWrapper.Status.LOADING -> {

                }
                ResponseWrapper.Status.SUCCESS -> {
                    Toast.makeText(context, "CREATE RESERVATION SUCCESSFULLY", Toast.LENGTH_LONG)
                        .show()
                    requireActivity().onBackPressed()
                }
                ResponseWrapper.Status.ERROR -> {
                    Toast.makeText(
                        context,
                        "ERROR: " + it.body?.errors.toString(),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
            iLoadingCommunicator.stopLoading()
        })
    }

    private fun changeButtonBackground(button: Button, type: Int){
        with(button){
            when(type){
                0 -> {
                    background = ContextCompat.getDrawable(
                        context,
                        R.drawable.button_background_yellow_outlined
                    )
                }
                1 -> {
                    background = ContextCompat.getDrawable(
                        context,
                        R.drawable.button_background_yellow
                    )
                }
                else -> {
                    background = ContextCompat.getDrawable(
                        context,
                        R.drawable.button_background_grey
                    )
                }
            }
        }
    }

    private fun getTimeFromArray(position: Int): String{
        return resources.getStringArray(R.array.reservation_time)[position]
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _viewBinding = null
    }
}