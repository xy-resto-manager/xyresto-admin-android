package com.example.xyrestoadminapp.reservation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.xyrestoadminapp.base.model.BaseApiResponse
import com.example.xyrestoadminapp.base.utils.ApiCallHelper.safeApiCall
import com.example.xyrestoadminapp.base.utils.ResponseWrapper
import com.example.xyrestoadminapp.reservation.model.Reservation
import com.example.xyrestoadminapp.reservation.repository.ReservationRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ReservationViewModel(reservationStatus: String, private val repository: ReservationRepository): ViewModel() {
    private val _reservations = MutableLiveData<ResponseWrapper<BaseApiResponse<List<Reservation>>>>()
    val reservations: LiveData<ResponseWrapper<BaseApiResponse<List<Reservation>>>> get() = _reservations

    init {
        getReservation(reservationStatus, 1)
    }

    fun getReservation(status: String, page: Int){
        viewModelScope.launch(Dispatchers.IO) {
            _reservations.postValue(appendReservationList(ResponseWrapper.loading()))
            val result = safeApiCall { repository.getReservations(status, page) }
            _reservations.postValue(appendReservationList(result))
        }
    }

    private fun appendReservationList(response: ResponseWrapper<BaseApiResponse<List<Reservation>>>): ResponseWrapper<BaseApiResponse<List<Reservation>>>{
        _reservations.value?.body?.data?.let { prevData ->
            response.body?.let {
                if (response.status == ResponseWrapper.Status.SUCCESS)
                    return ResponseWrapper(
                        response.status,
                        BaseApiResponse(
                            it.code,
                            prevData.toMutableList().apply { it.data?.let { data -> addAll(data) } },
                            it.status,
                            it.errors,
                            it.pagination
                        ),
                        response.message
                    )
            }
            return ResponseWrapper(
                response.status,
                BaseApiResponse(
                    response.body?.code,
                    prevData,
                    response.body?.status,
                    response.body?.errors,
                    _reservations.value?.body?.pagination
                ),
                response.message
            )
        }
        return response
    }
}