package com.example.xyrestoadminapp.reservation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.example.xyrestoadminapp.R
import com.example.xyrestoadminapp.base.constant.Constant
import com.example.xyrestoadminapp.base.utils.Utils.getScreenDimension
import com.example.xyrestoadminapp.databinding.FragmentReservationsBinding
import com.example.xyrestoadminapp.reservation.adapter.IViewPagerManager
import com.example.xyrestoadminapp.reservation.adapter.ReservationViewPagerAdapter
import com.google.android.material.tabs.TabLayoutMediator

class ReservationsFragment : Fragment(), IViewPagerManager {
    private var _viewBinding: FragmentReservationsBinding? = null
    private val viewBinding: FragmentReservationsBinding get() = _viewBinding!!
    private var reservationId: Int? = null
    private var currentViewPagerPosition = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.let {
            reservationId = it.getInt(RESERVATION_ID, -1)
            currentViewPagerPosition = it.getInt(CURRENT_VIEWPAGER_POSITION)
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, object : OnBackPressedCallback(true){
            override fun handleOnBackPressed() {
                val currentItem = getCurrentItem()
                if (currentItem.second is ReservationDetailFragment){
                    when(currentItem.first){
                        0 -> {
                            changeFragment(0, ReservationContentFragment.newInstance(Constant.STATUS_PENDING))
                        }
                        1 -> {
                            changeFragment(1, ReservationContentFragment.newInstance(Constant.STATUS_APPROVED))
                        }
                        2 -> {
                            changeFragment(2, ReservationContentFragment.newInstance(Constant.STATUS_DECLINED))
                        }
                    }
                    reservationId = null
                } else {
                    isEnabled = false
                    activity?.onBackPressed()
                }
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _viewBinding = FragmentReservationsBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView(){
        if (getScreenDimension(requireContext(), true).second >= 600){
            viewBinding.btnNewReservation.visibility = View.VISIBLE
            viewBinding.fbAddReservation.visibility = View.GONE
            viewBinding.btnNewReservation.setOnClickListener {
                findNavController().navigate(ReservationsFragmentDirections.reservationFormFragment())
            }
        } else {
            viewBinding.fbAddReservation.visibility = View.VISIBLE
            viewBinding.btnNewReservation.visibility = View.GONE
            viewBinding.fbAddReservation.setOnClickListener {
                findNavController().navigate(ReservationsFragmentDirections.reservationFormFragment())
            }
        }
        initViewPager()
        initTab()
        setReservationDetailFragment(currentViewPagerPosition)
    }

    private fun initViewPager(){
        with(viewBinding){
            viewPager.adapter = ReservationViewPagerAdapter(this@ReservationsFragment)
        }
    }

    private fun initTab(){
        with(viewBinding){
            TabLayoutMediator(tabLayout, viewPager){ tab, position ->
                when(position){
                    0 -> {
                        tab.text = resources.getString(R.string.pending)
                    }
                    1 -> {
                        tab.text = resources.getString(R.string.approved)
                    }
                    2 -> {
                        tab.text = resources.getString(R.string.rejected)
                    }
                }
            }.attach()
            viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback(){
                override fun onPageSelected(position: Int) {
                    currentViewPagerPosition = position
                }
            })
        }
    }

    private fun setReservationDetailFragment(position: Int){
        if (reservationId != null && reservationId != -1)
            changeFragment(position, ReservationDetailFragment.newInstance(reservationId!!))
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        reservationId?.let { outState.putInt(RESERVATION_ID, it) }
        outState.putInt(CURRENT_VIEWPAGER_POSITION, currentViewPagerPosition)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _viewBinding = null
    }

    override fun changeFragment(position: Int, fragment: Fragment){
        val viewPagerAdapter = viewBinding.viewPager.adapter as ReservationViewPagerAdapter
        viewPagerAdapter.changeFragment(position, fragment)
    }

    override fun getCurrentItem(): Pair<Int, Fragment> {
        with(viewBinding.viewPager){
            return Pair(currentItem, (adapter as ReservationViewPagerAdapter).getCurrentItem(currentItem))
        }
    }

    override fun setReservationId(id: Int) {
        reservationId = id
    }

    companion object{
        private const val RESERVATION_ID = "reservationId"
        private const val CURRENT_VIEWPAGER_POSITION = "currentViewPagerPosition"
    }
}