package com.example.xyrestoadminapp.reservation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.xyrestoadminapp.R
import com.example.xyrestoadminapp.base.constant.Constant.STATUS_APPROVED
import com.example.xyrestoadminapp.base.constant.Constant.STATUS_DECLINED
import com.example.xyrestoadminapp.base.constant.Constant.STATUS_PENDING
import com.example.xyrestoadminapp.base.storage.network.ApiClient
import com.example.xyrestoadminapp.base.utils.ResponseWrapper
import com.example.xyrestoadminapp.base.utils.Utils
import com.example.xyrestoadminapp.base.utils.Utils.formatRp
import com.example.xyrestoadminapp.base.utils.Utils.orDefaultStr
import com.example.xyrestoadminapp.base.widget.decoration.SpacingItemDecoration
import com.example.xyrestoadminapp.databinding.FragmentReservationDetailBinding
import com.example.xyrestoadminapp.home.IRetryCallBack
import com.example.xyrestoadminapp.home.LoadingCommunicator
import com.example.xyrestoadminapp.home.LoadingMode
import com.example.xyrestoadminapp.reservation.adapter.IViewPagerManager
import com.example.xyrestoadminapp.reservation.adapter.ReservationDetailAdapter
import com.example.xyrestoadminapp.reservation.model.EditReservationStatusRequest
import com.example.xyrestoadminapp.reservation.model.ReservationStatus
import com.example.xyrestoadminapp.reservation.network.ReservationApiService
import com.example.xyrestoadminapp.reservation.repository.ReservationRepository
import com.example.xyrestoadminapp.reservation.viewmodel.ReservationDetailViewModel
import com.example.xyrestoadminapp.reservation.viewmodel.ReservationDetailViewModelFactory

private const val ARG_RESERVATION_ID = "reservation_id"

class ReservationDetailFragment : Fragment() {
    private var reservationId: Int? = null
    private var _viewBinding: FragmentReservationDetailBinding? = null
    private val viewBinding: FragmentReservationDetailBinding get() = _viewBinding!!
    private val orderAdapter: ReservationDetailAdapter by lazy { ReservationDetailAdapter() }
    private val viewModel: ReservationDetailViewModel by viewModels { ReservationDetailViewModelFactory(
        ReservationRepository(ApiClient.getRetrofitInstance(requireContext()).create(ReservationApiService::class.java))
    ) }
    private lateinit var loadingCommunicator: LoadingCommunicator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            reservationId = it.getInt(ARG_RESERVATION_ID)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        loadingCommunicator = activity as LoadingCommunicator
        loadingCommunicator.startLoading(1, LoadingMode.PROGRESS_BAR_WITH_BACKGROUND)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _viewBinding = FragmentReservationDetailBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        reservationId?.let {
            viewModel.getReservationDetail(it)
        }
    }

    private fun initView(){
        val density = resources.displayMetrics.density
        val dimens = Utils.getScreenDimension(requireContext(), true)
        with(viewBinding){
            if (dimens.first < 720){
                val rvHeight = if (dimens.second > 400) (0.5*dimens.second*density).toInt() else (200*density).toInt()
                rvProductList.layoutParams = ConstraintLayout.LayoutParams(0, rvHeight).apply {
                    startToStart = R.id.cl_container
                    endToEnd = R.id.cl_container
                    topToBottom = R.id.v_line_break_2
                }
            }
            root.layoutParams = FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT
            ).apply { setMargins(10, 20, 10, 20) }
            btnApprove.setOnClickListener {
                viewModel.editReservationStatus(reservationId!!, EditReservationStatusRequest(ReservationStatus.APPROVED))
            }
            btnDecline.setOnClickListener {
                viewModel.editReservationStatus(reservationId!!, EditReservationStatusRequest(ReservationStatus.DECLINED))
            }
        }
        initRecyclerView()
        initObserver()
    }

    private fun initRecyclerView(){
        viewBinding.rvProductList.run{
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = orderAdapter.apply {
                addItemDecoration(SpacingItemDecoration(leftMargin = 10, topMargin = 10, rightMargin = 10, bottomMargin = 10))
            }
        }
    }

    private fun initObserver(){
        viewModel.reservationDetail.observe(viewLifecycleOwner, {
            when(it.status){
                ResponseWrapper.Status.LOADING -> {

                }
                ResponseWrapper.Status.SUCCESS -> {
                    it.body?.data?.let { data ->
                        data.orders?.let { orders ->
                            orderAdapter.setItemList(orders)
                        }
                        with(viewBinding){
                            tvReservationId.text = data.id.toString()
                            tvDate.text = root.context.getString(R.string.reservation_date, Utils.timeStampToDate(data.timestamp!!))
                            tvTime.text = root.context.getString(R.string.reservation_time, Utils.rangeTime(data.timestamp))
                            tvTableNumberValue.text = Utils.makeBold(data.tableId.toString(), 0, data.tableId.toString().length)
                            tvOrderedBy.text = Utils.makeBold(
                                resources.getString(
                                    R.string.ordered_by, data.fullName.orDefaultStr("-"),
                                    data.email.orDefaultStr("")
                                ),
                                13,
                                13+data.fullName.orDefaultStr("-").length
                            )
                            tvTotalOrder.text = Utils.makeBold(resources.getString(R.string.total_order, data.totalCost.formatRp()), 14, 14+data.totalCost.formatRp().length)
                            when(data.status){
                                STATUS_APPROVED -> {
                                    btnApprove.text = resources.getString(R.string.dining)
                                    btnApprove.setOnClickListener {
                                        viewModel.editReservationStatus(reservationId!!, EditReservationStatusRequest(ReservationStatus.DINING))
                                    }
                                }
                                STATUS_DECLINED -> {
                                    btnDecline.isEnabled = false
                                    btnDecline.background = ContextCompat.getDrawable(root.context, R.drawable.background_rectangle_gray_outlined)
                                    btnDecline.setTextColor(ContextCompat.getColor(root.context,R.color.colorGreyDark_767676))
                                }
                            }
                            if (data.orders.isNullOrEmpty()){
                                tvEmptyOrder.visibility = View.VISIBLE
                            }
                        }
                    }
                    loadingCommunicator.stopLoading()
                }
                ResponseWrapper.Status.ERROR -> {
                    loadingCommunicator.onError(it.body?.errors.toString(), listOf(Pair(0, object : IRetryCallBack{
                        override fun retry() {
                            reservationId?.let { reservationId ->
                                viewModel.getReservationDetail(reservationId)
                            }
                        }
                    })))
                }
            }
        })
        viewModel.editReservationStatus.observe(viewLifecycleOwner, {
            when(it.second.status){
                ResponseWrapper.Status.LOADING -> {

                }
                ResponseWrapper.Status.SUCCESS -> {
                    Toast.makeText(context, "UPDATE SUCCESSFULLY", Toast.LENGTH_LONG).show()
                    val iViewPagerManager: IViewPagerManager = parentFragment as IViewPagerManager
                    when(it.first){
                        ReservationStatus.PENDING -> {
                            iViewPagerManager.changeFragment(0, ReservationContentFragment.newInstance(STATUS_PENDING))
                        }
                        ReservationStatus.APPROVED -> {
                            iViewPagerManager.changeFragment(1, ReservationContentFragment.newInstance(STATUS_APPROVED))
                        }
                        ReservationStatus.DECLINED -> {
                            iViewPagerManager.changeFragment(2, ReservationContentFragment.newInstance(STATUS_DECLINED))
                        }
                    }
                    requireActivity().onBackPressed()
                }
                ResponseWrapper.Status.ERROR -> {
                    Toast.makeText(context, "ERROR: "+it.second.body?.errors.toString(), Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    companion object {
        @JvmStatic
        fun newInstance(reservationId: Int) =
            ReservationDetailFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_RESERVATION_ID, reservationId)
                }
            }
    }
}