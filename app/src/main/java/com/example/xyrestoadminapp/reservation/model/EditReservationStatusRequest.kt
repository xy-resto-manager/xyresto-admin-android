package com.example.xyrestoadminapp.reservation.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EditReservationStatusRequest(
    @field:SerializedName("status")
    val reservationStatus: ReservationStatus
): Parcelable
