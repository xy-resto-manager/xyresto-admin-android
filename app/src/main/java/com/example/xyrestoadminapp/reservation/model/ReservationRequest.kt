package com.example.xyrestoadminapp.reservation.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ReservationRequest(
    @SerializedName("timestamp")
    val timestamp: Long? = null,
    @SerializedName("tableId")
    val tableId: Int? = null,
    @SerializedName("orders")
    val orders: List<Order>? = null
): Parcelable
