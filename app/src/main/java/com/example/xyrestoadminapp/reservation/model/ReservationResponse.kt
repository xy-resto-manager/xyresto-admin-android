package com.example.xyrestoadminapp.reservation.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ReservationResponse(
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("timestamp")
    val timestamp: Long? = null,
    @SerializedName("tableId")
    val tableId: Int? = null,
    @SerializedName("status")
    val status: String? = null,
    @SerializedName("email")
    val email: String? = null,
    @SerializedName("orders")
    val orders: List<ReservationItem>? = null
): Parcelable