package com.example.xyrestoadminapp.reservation.adapter

import androidx.fragment.app.Fragment

interface IViewPagerManager{
    fun changeFragment(position: Int, fragment: Fragment)
    fun getCurrentItem(): Pair<Int, Fragment>
    fun setReservationId(id: Int)
}
