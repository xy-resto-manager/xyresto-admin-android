package com.example.xyrestoadminapp.reservation.viewmodel

import androidx.lifecycle.*
import com.example.xyrestoadminapp.base.model.BaseApiResponse
import com.example.xyrestoadminapp.base.utils.ApiCallHelper.safeApiCall
import com.example.xyrestoadminapp.base.utils.ResponseWrapper
import com.example.xyrestoadminapp.reservation.model.ReservationRequest
import com.example.xyrestoadminapp.reservation.model.ReservationResponse
import com.example.xyrestoadminapp.reservation.model.TableResponse
import com.example.xyrestoadminapp.reservation.repository.ReservationRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class ReservationFormViewModel(private val repository: ReservationRepository): ViewModel() {
    private val _tables = MutableLiveData<ResponseWrapper<BaseApiResponse<List<TableResponse>>>>()
    val tables: LiveData<ResponseWrapper<BaseApiResponse<List<TableResponse>>>> get() =  _tables
    private val _reservationResponse = MutableLiveData<ResponseWrapper<BaseApiResponse<ReservationResponse>>>()
    val reservationResponse: LiveData<ResponseWrapper<BaseApiResponse<ReservationResponse>>> get() = _reservationResponse
    private var debouncePeriod: Long = 1000
    private var searchJob: Job? = null

    fun callTablesApi(timestamp: Long) {
        searchJob?.cancel()
        searchJob = viewModelScope.launch(Dispatchers.IO) {
            delay(debouncePeriod)
            _tables.postValue(ResponseWrapper.loading())
            val tables = safeApiCall{ repository.getTables(timestamp) }
            _tables.postValue(tables)
        }
    }

    fun createReservation(reservationRequest: ReservationRequest){
        viewModelScope.launch(Dispatchers.IO) {
            _reservationResponse.postValue(ResponseWrapper.loading())
            val result = safeApiCall { repository.createReservation(reservationRequest) }
            _reservationResponse.postValue(result)
        }
    }

    override fun onCleared() {
        super.onCleared()
        searchJob?.cancel()
    }
}