package com.example.xyrestoadminapp.reservation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.xyrestoadminapp.reservation.repository.ReservationRepository

class ReservationDetailViewModelFactory(private val repository: ReservationRepository): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ReservationDetailViewModel(repository) as T
    }
}