package com.example.xyrestoadminapp.reservation.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EditServingStatus(
    @SerializedName("productIds")
    val productIds: List<Int>
): Parcelable
