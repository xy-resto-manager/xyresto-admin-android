package com.example.xyrestoadminapp.reservation.viewmodel

import androidx.lifecycle.*
import com.example.xyrestoadminapp.base.model.BaseApiResponse
import com.example.xyrestoadminapp.base.utils.ApiCallHelper.safeApiCall
import com.example.xyrestoadminapp.base.utils.ResponseWrapper
import com.example.xyrestoadminapp.base.utils.Utils.orDefaultInt
import com.example.xyrestoadminapp.reservation.model.*
import com.example.xyrestoadminapp.reservation.repository.ReservationRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ReservationDetailViewModel(private val repository: ReservationRepository): ViewModel() {
    private val _reservationDetail = MutableLiveData<ResponseWrapper<BaseApiResponse<Reservation>>>()
    val reservationDetail: LiveData<ResponseWrapper<BaseApiResponse<Reservation>>> = Transformations.switchMap(_reservationDetail, ::transformData)
    private val _editReservationStatus = MutableLiveData<Pair<ReservationStatus, ResponseWrapper<BaseApiResponse<Any>>>>()
    val editReservationStatus: LiveData<Pair<ReservationStatus, ResponseWrapper<BaseApiResponse<Any>>>> = _editReservationStatus
    private lateinit var reservationStatus: ReservationStatus

    fun getReservationDetail(id: Int){
        viewModelScope.launch(Dispatchers.IO) {
            _reservationDetail.postValue(ResponseWrapper.loading())
            val result = safeApiCall { repository.getReservationDetail(id) }
            _reservationDetail.postValue(result)
        }
    }

    fun editReservationStatus(id: Int, mReservationStatus: EditReservationStatusRequest){
        reservationStatus = mReservationStatus.reservationStatus
        viewModelScope.launch(Dispatchers.IO) {
            _editReservationStatus.postValue(Pair(reservationStatus, ResponseWrapper.loading()))
            val result = safeApiCall { repository.editReservationStatus(id, mReservationStatus) }
            _editReservationStatus.postValue(Pair(reservationStatus, result))
        }
    }

    fun editServingStatus(reservationId: Int, editServingStatus: EditServingStatus){
        viewModelScope.launch(Dispatchers.IO) {
            val result = safeApiCall { repository.editServingStatus(reservationId, editServingStatus) }
            if (result.status == ResponseWrapper.Status.SUCCESS){
                _reservationDetail.value?.body?.let {
                    _reservationDetail.postValue(
                        ResponseWrapper(
                            result.status,
                            BaseApiResponse(
                                code = result.body?.code,
                                data = Reservation(
                                    it.data?.id,
                                    it.data?.timestamp,
                                    it.data?.email,
                                    it.data?.fullName,
                                    it.data?.tableId,
                                    it.data?.status,
                                    updateServingStatus(editServingStatus.productIds[0],it.data?.orders),
                                    it.data?.totalCost.orDefaultInt(0)
                                ),
                                status = result.body?.status
                            ),
                            result.message
                        )
                    )
                }
            }
        }
    }

    private fun updateServingStatus(productId: Int, orderList: List<ReservationOrder>?): List<ReservationOrder>{
        return mutableListOf<ReservationOrder>().apply {
            orderList?.forEach {
                if (it.product?.id == productId){
                    add(
                        ReservationOrder(
                            it.product,
                            it.quantity,
                            true
                        )
                    )
                } else {
                    add(it)
                }
            }
        }
    }

    private fun transformData(reservation: ResponseWrapper<BaseApiResponse<Reservation>>) = calculateTotalCost(reservation)
    private fun calculateTotalCost(reservation: ResponseWrapper<BaseApiResponse<Reservation>>):LiveData<ResponseWrapper<BaseApiResponse<Reservation>>> {
        val liveData = MutableLiveData<ResponseWrapper<BaseApiResponse<Reservation>>>()
        var totalCost = 0
        reservation.body?.data?.orders?.let { orders ->
            for (order in orders){
                totalCost += order.product?.price?.times(order.quantity!!) ?: 0
            }
        }
        reservation.body?.data?.totalCost = totalCost
        liveData.value = reservation
        return liveData
    }
}