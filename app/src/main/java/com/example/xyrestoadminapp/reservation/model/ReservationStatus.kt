package com.example.xyrestoadminapp.reservation.model

enum class ReservationStatus {
    PENDING, APPROVED, DECLINED, DINING, FINISHED,CANCELED
}