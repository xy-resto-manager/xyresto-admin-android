package com.example.xyrestoadminapp.reservation.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Table(
    @SerializedName("tableId")
    val tableId: Int? = null,
    @SerializedName("x")
    val x: Int? = null,
    @SerializedName("y")
    val y: Int? = null
): Parcelable
